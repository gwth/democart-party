import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import { loadEnv } from "vite";
// https://vitejs.dev/config/
export default ({ mode }) => {
  process.env = Object.assign(process.env, loadEnv(mode, process.cwd(), ""));
  console.log("process.env", process.env.VITE_API_URL, process.env.VITE_APP_URL, process.env.VITE_PORT, process.env.VITE_API_URL_3);
  return defineConfig({
    plugins: [react()],
    server: {
      host: process.env.VITE_APP_URL,
      port: Number(process.env.VITE_PORT),
      open: false,
      proxy: {
        "/api-3": {
          target: process.env.VITE_API_URL_3,
          changeOrigin: true,
          secure: false,
          rewrite: (path) => path.replace(/^\/api-3/, "/api"),
          configure: (proxy, _options) => {
            proxy.on("error", (err, _req, _res) => {
              console.log("proxy error", err);
            });
            proxy.on("proxyReq", (proxyReq, req, _res) => {
              console.log("Sending Request to the Target: API 3 ", req.method, req.url);
            });
            proxy.on("proxyRes", (proxyRes, req, _res) => {
              console.log("Received Response from the Target: API 3", proxyRes.statusCode, req.url);
            });
          },
        },
        "/api": {
          target: process.env.VITE_API_URL,
          changeOrigin: true,
          secure: false,
          ws: true,
          configure: (proxy, _options) => {
            proxy.on("error", (err, _req, _res) => {
              console.log("proxy error", err);
            });
            proxy.on("proxyReq", (proxyReq, req, _res) => {
              console.log("Sending Request to the Target:", req.method, req.url);
            });
            proxy.on("proxyRes", (proxyRes, req, _res) => {
              console.log("Received Response from the Target:", proxyRes.statusCode, req.url);
            });
          },
        },
      },
    },
    build: {
      // outDir: "build",
      // sourcemap: true,
      assetsDir: "assets",
      emptyOutDir: true,
    },
  });
};
