export class UserData {
    id: number;
    prefixTh: string;
    nameTh: string;
    lastNameTh: string;
    prefixEng: string;
    nameEng: string;
    lastNameEng: string;
    idCard: string;
    birthDate: Date;
    religion: string;
    address: string;
    address_number: string;
    village_no: string | null;
    alley: string;
    soi: string | null;
    road: string | null;
    tombon: string;
    amphoe: string;
    province: string;
    registDate: Date;
    email: string
}

export class registerLog {
    id: number
    name_th: string
    surname_th: string
    name_en: string
    agent_id: number
    surname_en: string
    birth_date: string
    created_at: string
    status: number
    id_card: number
    phone: number
    type_prefix_th: any
    province: any
    tumbon: any
    aumper: any
    address_number: string
    alley: string
    soi: string
    road: string
    accept_type_id: number
    payment: number
    photo_bank: string
    photo_id_card: string
    photo_me: string
    email: string
}

export interface IRegisterlog {
    prefix: string;
    prefixTh: string;
    prefixOther: string;
    firstName: string;
    lastName: string;
    firstNameEn: string | null;
    lastNameEn: string | null;
    prefixEn: string | null;
    idCard: string;
    birthDate: Date;
    houseNumber: string;
    building: string | null;
    moo: string | null;
    soi: string | null;
    road: string | null;
    tombon: string | undefined;
    amphoe: string| undefined;
    province: string| undefined;
    telephone: string;
    typePayment: string;
    address: string | null
    email: string
    // fileImageApprove: File | FileCustom | null;
    // fileImageCardIdentification: File | FileCustom | null;
    // fileImagePayment: File | FileCustom | null;
    acceptTypId: string;
    typeRegister: string;
    agent: string | null;
    status: string;
    renewTag: string;
  }
  