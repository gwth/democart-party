export interface IProvince {
  id: number;
  name: string;
}
export interface IAmphoe extends IProvince {}
export interface ITombon extends IProvince {}
export interface IPrefix extends IProvince {
  sort: number;
  data: string;
}
