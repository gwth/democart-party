export interface FileCustom {
  name: string;
  type: string;
  size: number;
  data: string;
}
