export interface IRegister {
  prefix: string;
  prefixOther: string;
  firstName: string;
  lastName: string;
  firstNameEn: string | null;
  lastNameEn: string | null;
  prefixEn: string | null;
  idCard: string;
  birthDate: Date;
  houseNumber: string;
  building: string | null;
  moo: string;
  soi: string | null;
  road: string | null;
  tombon: string;
  amphoe: string;
  province: string;
  telephone: string;
  typePayment: string;
  // fileImageApprove: File | FileCustom | null;
  // fileImageCardIdentification: File | FileCustom | null;
  // fileImagePayment: File | FileCustom | null;
  acceptTypId: string;
  typeRegister: string;
  agent: string | null;
  status: string;
  renewTag: string;
  email:string;
}
