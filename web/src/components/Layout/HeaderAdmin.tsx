import { Button, Text, Box, Image, Grid, GridCol, Container, Flex } from "@mantine/core";
import classes from "./HeaderMegaMenu.module.css";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../store/hook";
import { logout } from "../../store/auth/authSlice";
import { clearUser } from "../../store/user/userSlice";

export default function HeaderAdmin() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const user = useAppSelector((state) => state.user?.value);
  const handleLogout = () => {
    dispatch(logout());
    dispatch(clearUser());
    navigate("/");
    localStorage.removeItem("search");
  };

  return (
    <Box bg={"blue"} style={{ position: "sticky", top: 0, zIndex: 100, paddingBottom: "10px" }} py={10}>
      <Container size="xl">
        <header className={classes.header}>
          <Grid justify="space-between" align="center" py={20}>
            <GridCol span={{ base: 12, lg: 6 }}>
              <Flex justify="start" align="center" gap={10} direction={{ base: "column", lg: "row" }}>
                <Image w={{ base: 80, lg: 100 }} src={"/login/Group162.svg"} />

                <Text fz={{ base: 22, lg: 30 }} c={"#FFFF"} ta={{ base: "center", lg: "left" }}>
                  พรรคประชาธิปัตย์
                </Text>
              </Flex>
            </GridCol>
            <GridCol span={{ base: 12, lg: 6 }}>
              <Flex justify="end" align="center" gap={10} direction={{ base: "column", lg: "row" }}>
                {user ? (
                  <Text fz={16} c={"#FFFF"} ta={"center"}>
                    คุณ {user?.firstName ? user?.firstName : ""} {user?.lastName ? user?.lastName : ""}
                  </Text>
                ) : null}

                <Button
                  radius={"xl"}
                  onClick={handleLogout}
                  styles={{
                    root: {
                      backgroundColor: "transparent",
                      borderColor: "white",
                    },
                  }}>
                  ออกจากระบบ
                </Button>
              </Flex>
            </GridCol>
          </Grid>
        </header>
      </Container>
    </Box>
  );
}
