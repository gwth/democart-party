import { Text, Box, Image, Grid, GridCol, Flex, Container } from "@mantine/core";
import classes from "./HeaderMegaMenu.module.css";

export function HeaderRegister() {
  return (
    <Box bg={"blue"} style={{ position: "sticky", top: 0, zIndex: 100, overflow: "hidden" }}>
      <Container size="xl">
        <header className={classes.header}>
          <Grid justify="center" align="center" py={20}>
            <GridCol span={{ base: 12 }}>
              <Flex justify="center" align="center" gap={10}>
                <Image w={{ base: 80, lg: 100 }} src={"/login/Group162.svg"} />

                <Text fz={{ base: 22, lg: 30 }} c={"#FFFF"} ta={{ base: "center", lg: "left" }}>
                  พรรคประชาธิปัตย์
                </Text>
              </Flex>
            </GridCol>
          </Grid>
        </header>
      </Container>
    </Box>
  );
}
