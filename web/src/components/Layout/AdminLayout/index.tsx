import { BackgroundImage, Container } from "@mantine/core";
import HeaderAdmin from "../HeaderAdmin";

type AdminProps = Readonly<{
  children: React.ReactNode;
}>;

export default function AdminLayout({ children }: AdminProps) {
  return (
    <>
      <HeaderAdmin />
      <BackgroundImage
        style={{
          backgroundPosition: "contain",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
        src={"/login/Group213.png"}>
        <Container
          className="scrollable__content"
          size="xl"
          style={{
            overflow: "auto",
            height: "calc(100vh - 160px )",
          }}
          px={{ base: 10, md: "auto" }}>
          {children}
        </Container>
      </BackgroundImage>
    </>
  );
}
