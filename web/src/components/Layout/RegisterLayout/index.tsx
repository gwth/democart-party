import { BackgroundImage, Container } from "@mantine/core";
import { HeaderRegister } from "../HeaderRegister";

type RegisterProps = Readonly<{
  children: React.ReactNode;
}>;

export default function RegisterLayout({ children }: RegisterProps) {
  return (
    <>
      <HeaderRegister />
      <BackgroundImage
        style={{
          backgroundPosition: "contain",
          backgroundSize: "cover",
          backgroundRepeat: "no-repeat",
        }}
        src={"/login/Group213.png"}>
        <Container
          className="scrollable__content"
          size="xl"
          style={{
            overflow: "auto",
            height: "calc(100vh - 140px )",
          }}
          px={{ base: 10, md: "auto" }}>
          {children}
        </Container>
      </BackgroundImage>
    </>
  );
}
