import { Flex, Paper } from "@mantine/core";
import { ContextModalProps } from "@mantine/modals";

export function BaseModal({ innerProps }: ContextModalProps<{ modalBody: string }>) {
  return (
    <Paper radius={25}>
      <Flex direction="column" justify="center" align="center" mih={100}>
        {innerProps.modalBody}
        {/*  <Button onClick={() => context.closeModal(id)} variant="filled" radius="xl" w="60%" mt={20} h={40} fz={18} fw={300} bg={"#01A1E8"}>
          ปิด
        </Button> */}
      </Flex>
    </Paper>
  );
}
