import { useAppDispatch, useAppSelector } from "../../../store/hook";
import { closePopup } from "../../../store/popup/popupSlice";

export default function Popup() {
  const dispatch = useAppDispatch();

  const isOpen = useAppSelector((state) => state.popup.isOpen);
  const content = useAppSelector((state) => state.popup.content);
  const withButton = useAppSelector((state) => state.popup.withButton);
  const onClose = useAppSelector((state) => state.popup.onClose);

  const handleClose = () => {
    dispatch(
      closePopup({
        onClose: () => {
          console.log("from popup component");
          onClose;
        },
      })
    );
  };

  return (
    isOpen && (
      <div className="popup">
        <div className="popup-content">
          {content}
          {withButton && <button onClick={handleClose}>Close</button>}
        </div>
      </div>
    )
  );
}
