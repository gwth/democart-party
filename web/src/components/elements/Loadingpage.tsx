import { Grid, Loader, GridCol } from "@mantine/core";
export default function Loadingpage() {
    return (

        <Grid justify='center' align='center' h={'100vh'} mt={{ base: 20, lg: 100 }}>
            <GridCol span={{ base: 2, lg: 1 }} ><Loader size={50} /></GridCol>
            <GridCol fz={20} span={{ base: 12, lg: 2 }} mr={{ base: 0, lg: 100 }} ta={{ base: "center", lg: "left" }}>กำลังโหลดข้อมูล</GridCol>
        </Grid>

    )
}
