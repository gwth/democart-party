import { Button } from "@mantine/core";

export default function DowloadButton() {
  return (
    <Button variant="contained" component="label" mt={10} mb={{ base: 10, lg: 10 }} w={{ base: "100%", lg: "auto" }} bg={"#01A1E8"} radius="xl">
      <a href="/qr-code-payment.png" target="_blank" download style={{ color: "white", textDecoration: "none" }}>
        ดาวน์โหลด QR code
      </a>
    </Button>
  );
}
