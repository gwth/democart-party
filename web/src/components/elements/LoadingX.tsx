
import { Image, Flex } from "@mantine/core";
import FadeIn from "react-fade-in";
const LoadingX = () => {


    return (
        <>
            <Flex justify='center' align='center' h={'100vh'}>
                <FadeIn delay={500}>
                    <Image pt={20} src={"/login/Group162.svg"}
                        // style={{ width: "100%" }}
                        w={{ base: "50%", sm: "70%", md: "70%", lg: "auto" }}
                        ml={{ base: 90, sm: 50, md: 0, lg: "auto" }}
                    />
                </FadeIn>
            </Flex >
        </>
    )
}
export default LoadingX
