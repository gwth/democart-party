import "./PleaseWaitLoader.scss";

// reference: https://codepen.io/nxworld/pen/zwGpXr

export default function PleaseWaitLoader() {
  const text = "Please Wait";
  return (
    <div className="loading loading03">
      {text.split("").map((letter, index) => (
        <span key={index}>{letter}</span>
      ))}
    </div>
  );
}
