import { Button } from '@mantine/core';
import copy from "copy-to-clipboard";


export default function CopyButton() {

    const copyToClipboard = () => {
        let copyText = '0686035852';
        copy(copyText);
    };

    return (
        <Button
            mt={10}
            mb={{ base: 25, lg: 60 }}
            w={{ base: "100%", lg: "auto" }}
            onClick={copyToClipboard}
            bg={'#01A1E8'}
            variant="filled"
            radius="xl">คัดลอกบัญชีธนาคาร
        </Button>
    )
}
