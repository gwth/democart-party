import CookieConsent from "react-cookie-consent";
import { Text } from "@mantine/core";
export default function Cookie() {
  return (
    <CookieConsent
      location="bottom"
      buttonText="ยอมรับทั้งหมด"
      cookieName="democratCookie"
      style={{
        borderRadius: "5px",
        width: "70%",
        background: "white",
        position: "absolute",
        left: "50%",
        transform: "translateX(-50%)",
        alignItems: "center",
      }}
      buttonStyle={{
        background: "#01A1E8",
        color: "white",
        fontSize: "15px",
        borderRadius: "5px",
      }}
      expires={150}>
      <Text
        w={"80%"}
        fw={"bold"}
        fz={{ base: 12, sm: 20, md: 17, lg: 20 }}
        style={{
          color: "black",
          paddingBottom: "10px",
        }}>
        เว็บไซต์นี้ใช้คุกกี้
      </Text>
      <Text
        w={"80%"}
        fz={{ base: 12, sm: 17, md: 15, lg: 17 }}
        style={{
          color: "black",
          paddingBottom: "10px",
          // fontSize: "17px",
        }}>
        เว็บไซต์ของเราใช้คุกกี้เพื่อพัฒนาประสิทธิภาพและช่วยให้เว็บไซต์นี้ทำงานได้อย่างถูกต้อง พร้อมการสร้างประสบการณ์ที่ดีในการใช้เว็บไซต์ของท่าน
      </Text>
    </CookieConsent>
  );
}
