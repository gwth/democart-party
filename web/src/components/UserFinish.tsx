import { Box, Grid, Text } from "@mantine/core";
import { registerLog } from "../interfaces/userDataType";

interface Props {
  dataUser: registerLog;
  index: number;
}

export default function UserFinish({ dataUser, index }: Props) {
  const options: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    // timeStyle: "long",
  };
  return (
    <>
      <Box p={15} w={"100%"} bg={index % 2 == 0 ? "white" : "#F1F1F1"}>
        <Grid mt={10}>
          <Grid.Col span={{ base: 12, xs: 4 }}>
            <Text c={"#00A1E9"}>เลขบัตรประชาชน</Text>
          </Grid.Col>
          <Grid.Col span={{ base: 12, xs: 8 }}>
            <Text>{dataUser.id_card}</Text>
          </Grid.Col>
          <Grid.Col span={{ base: 12, xs: 4 }}>
            <Text c={"#00A1E9"}>ชื่อ-นามสกุล</Text>
          </Grid.Col>
          <Grid.Col span={{ base: 12, xs: 8 }}>
            <Text>
              {
                /*dataUser.type_prefix_th +*/
                dataUser.name_th + " " + dataUser.surname_th
              }
            </Text>
          </Grid.Col>
          <Grid.Col span={{ base: 12, xs: 4 }}>
            <Text c={"#00A1E9"}>วันที่สมัคร</Text>
          </Grid.Col>
          <Grid.Col span={{ base: 12, xs: 8 }}>
            <Text>
              {new Date(dataUser.created_at).toLocaleDateString("th", options) +
                " น."}
            </Text>
          </Grid.Col>
        </Grid>
      </Box>
    </>
  );
}
