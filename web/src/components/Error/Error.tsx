import { Button, Flex, Image, Paper, Title } from "@mantine/core";
import { ErrorBoundary } from "react-error-boundary";

type ErrorProps = Readonly<{
  children: React.ReactNode;
}>;

export default function Error({ children }: ErrorProps) {
  return (
    <ErrorBoundary
      fallbackRender={({ error, resetErrorBoundary }) => (
        <Flex justify="center" align="center" h="100vh">
          <Paper
            radius="xl"
            p={{ base: 20, md: 25 }}
            style={{ display: "flex", flexDirection: "column", justifyItems: "center", alignItems: "center" }}>
            <Image w={80} my={20} src={`user_status/reject.svg`}></Image>
            <Title style={{ textAlign: "center", wordBreak: "break-word" }} order={1}>
              เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง
            </Title>
            {typeof error == "string" ? (
              <Title style={{ textAlign: "center" }} order={2}>
                {error.toString()}
              </Title>
            ) : (
              <>
                <Title style={{ textAlign: "center" }} order={2}>
                  {error.status}
                </Title>
                <Title style={{ textAlign: "center" }} order={2}>
                  {error.message}
                </Title>
              </>
            )}
            <Button radius={50} size="md" justify="center" mt={20} onClick={resetErrorBoundary}>
              refresh
            </Button>
          </Paper>
        </Flex>
      )}
      onReset={() => {
        window.location.reload();
      }}>
      {children}
    </ErrorBoundary>
  );
}
