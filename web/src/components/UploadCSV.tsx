import { FileInput, FileInputProps, Pill, Text } from "@mantine/core";
import { useRef, useState } from "react";
import Papa from "papaparse";
import { UserData } from "../interfaces/userDataType";

interface Props {
  callBackUploadCSV?: (idUsers: UserData[]) => void;
}

export default function UploadCSV({ callBackUploadCSV }: Props) {
  const ValueComponent: FileInputProps["valueComponent"] = ({ value }) => {
    if (value === null) {
      return null;
    }
    if (Array.isArray(value)) {
      return (
        <Pill.Group>
          {value.map((file, index) => (
            <Pill color="blue" key={index}>
              {file.name}
            </Pill>
          ))}
        </Pill.Group>
      );
    }
    return <Pill>{value.name}</Pill>;
  };

  const refFile = useRef(null);
  const [key, setKey] = useState(0);
  let userData: UserData[] = [];
  const handleDrop = (files: File | null) => {
    const file = files;
    setKey((prevKey) => prevKey + 1);
    if (file == null) return;

    // Check if the dropped file is a CSV file
    if (file.type === "text/csv" || true) {
      try {
        // refFile.current.value = null;
        Papa.parse(file, {
          complete: (result: { data: any[] }) => {
            // `result.data` contains the parsed CSV data
            localStorage.removeItem("userDataExcel");
            if (
              result.data.length > 0 &&
              !("เลขประจำตัวประชาชน" in result.data[0])
            ) {
              alert("กรุณาอัพไฟล์ CSV ที่ถูกต้อง");
              return;
            }
            result.data.forEach((element, index) => {
              let temp: UserData = new UserData();
              temp.id = index + 1;
              temp.idCard = element["เลขประจำตัวประชาชน"]
                .replace('"', "")
                .replace("=", "")
                .replace('"', "");
              if (checkIdCard(temp.idCard)) {
                // console.log("dupIdcard ", temp.idCard);
                return;
              }

              temp.prefixTh = element["คำนำหน้าชื่อ"];
              temp.nameTh = element["ชื่อ"];
              temp.lastNameTh = element["นามสกุล"];
              temp.prefixEng = element["Title"];
              temp.nameEng = element["Name"];
              temp.lastNameEng = element["Surname"];
              temp.address_number = element["เลขที่"];
              temp.village_no = element["หมู่ที่"];
              temp.alley = element["ตรอก"];
              temp.soi = element["ซอย"];
              temp.road = element["ถนน"];
              temp.tombon = element["ตำบล/แขวง"];
              temp.amphoe = element["อำเภอ/เขต"];
              temp.province = element["จังหวัด"];

              // temp.address =
              //   element["เลขที่"] +
              //   " " +
              //   element["หมู่ที่"] +
              //   " " +
              //   element["ตรอก"] +
              //   " " +
              //   element["ซอย"] +
              //   " " +
              //   element["ตำบล/แขวง"] +
              //   " " +
              //   element["อำเภอ/เขต"] +
              //   " " +
              //   element["จังหวัด"];
              temp.birthDate = new Date(element["วันเกิด"]);
              temp.registDate = new Date();
              userData.push(temp);
            });
            localStorage.setItem("userDataExcel", JSON.stringify(userData));
            if (callBackUploadCSV) callBackUploadCSV(userData);
            // console.log(userData);
          },
          header: true, // Set to true if the CSV file has a header row
        });
      } catch (e) {
        alert("กรุณาอัพไฟล์ CSV ที่ถูกต้อง");
        // alert(e);
      }
    } else {
    }
  };
  function checkIdCard(idCard: string) {
    let temp = userData.map((m) => m.idCard);
    return temp.includes(idCard);
  }
  return (
    <>
      <Text mt={30} fw={500} size="md" c={"#00A1E9"}>
        เลือกไฟล์ CSV
      </Text>
      <FileInput
        ref={refFile}
        mt={15}
        radius="xl"
        placeholder="เลือกไฟล์ CSV"
        // multiple
        styles={{
          input: {
            borderColor: "#00A1E9",
          },
        }}
        valueComponent={ValueComponent}
        accept={"text/csv"}
        key={key}
        onChange={handleDrop}
        clearable
      />
    </>
  );
}
