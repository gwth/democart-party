import { Button, Flex, Grid, GridCol, Title } from "@mantine/core";
import { useNavigate } from "react-router-dom";

export default function Forbidden() {
  const navigate = useNavigate();
  return (
    <Grid>
      <GridCol span={12}>
        <Flex justify={"center"} direction={"column"} align={"center"}>
          <Title size={250} c={"gray"}>
            403
          </Title>
          <Title size={80} c={"gray"}>
            Forbidden
          </Title>
          <Button size="md" onClick={() => navigate(-1)}>
            Take me back to previous page
          </Button>
          <br />
          <Button size="md" onClick={() => navigate("/main")}>
            Take me back to Home
          </Button>
        </Flex>
      </GridCol>
    </Grid>
  );
}
