import { useAppSelector } from "../../store/hook";
import { selectUser } from "../../store/user/userSlice";

import { Navigate, Outlet } from "react-router-dom";

interface RequiredRoleProps {
  allowedRoles: number[];
  children?: React.ReactNode;
}

export default function RequiredRole({ allowedRoles, children }: RequiredRoleProps) {
  const user = useAppSelector(selectUser);

  if (user) {
    if (user.role) {
      // const allowed = user?.roles.find((role) => allowedRoles.includes(role));
      const allowed = allowedRoles.includes(user.role);
      if (allowed) {
        return children ? children : <Outlet />;
      }

      if (!allowed) {
        return <Navigate to="/forbidden" replace />;
      }
    } else {
      return <Navigate to="/" replace />;
    }
  }
}
