import { Navigate, Outlet, useLocation } from "react-router-dom";

interface ProtectedRouteProps {
  redirectPath?: string;
  children?: JSX.Element;
  isAllowed: boolean;
}

export default function RequiredAuth({ isAllowed, redirectPath = "/", children }: ProtectedRouteProps) {
  const location = useLocation();

  if (!isAllowed) {
    return <Navigate to={redirectPath} state={{ from: location }} />;
  }

  return children ? children : <Outlet />;
}
