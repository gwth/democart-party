import { FileInput, FileInputProps, Grid, GridCol, Image, Pill } from "@mantine/core";
import { useRef, useState } from "react";
import { BsPaperclip } from "react-icons/bs";
import { FileCustom } from "../../interfaces/file.interface";
import Resizer from "react-image-file-resizer";

const ValueComponent: FileInputProps["valueComponent"] = ({ value }) => {
  if (value === null) {
    return null;
  }

  if (Array.isArray(value)) {
    return (
      <Pill.Group>
        {value.map((file, index) => (
          <Pill color="blue" key={index}>
            {file.name}
          </Pill>
        ))}
      </Pill.Group>
    );
  }

  return <Pill>{value.name}</Pill>;
};

type ImgProps = {
  name: string;
  preview?: string;
  onChange: (file: File | null, fileCustom: FileCustom | null) => void;
  required?: boolean;
};
export default function Imgupload(props: ImgProps) {
  const fileInputRef = useRef<HTMLButtonElement>(null);
  const selectedFileRef = useRef<File | null>(null);

  const [previewUrl, setPreviewUrl] = useState<string | ArrayBuffer | null>(null);

  const handleFileChange = (files: File[]) => {
    const file = files?.[0];

    if (file) {
      selectedFileRef.current = file;

      const reader = new FileReader();
      reader.onloadend = () => {
        const base64String = reader.result?.toString().split(",")[1];
        const fileCustom: FileCustom = {
          name: file.name,
          type: file.type,
          size: file.size,
          data: base64String || "",
        };

        Resizer.imageFileResizer(
          file,
          1280,
          960,
          "JPEG",
          300,
          0,
          (fileResize) => {
            props.onChange(fileResize as File, fileCustom);
          },
          "file",
          800,
          600
        );

        setPreviewUrl(reader.result);
      };

      reader.readAsDataURL(file);
    } else {
      handleClearFile();
    }
  };

  const handleClearFile = () => {
    selectedFileRef.current = null;

    setPreviewUrl(null);
    props.onChange(null, null);

    if (fileInputRef.current) {
      fileInputRef.current.value = "";
    }
  };

  return (
    <Grid pt={10}>
      <GridCol span={12}>
        <FileInput
          {...props}
          leftSection={<BsPaperclip size={20} style={{ color: "#00A1E9" }} />}
          label={props.name}
          withAsterisk={props.required || false}
          required={props.required || false}
          accept="image/*"
          miw="100%"
          styles={{
            input: {
              borderRadius: "20px",
              height: "40px",
              borderColor: "#00A1E9",
            },
            label: {
              color: "#00A1E9",
              fontWeight: "bold",
              fontSize: "20px",
            },
          }}
          clearable
          placeholder="Upload files"
          multiple
          ref={fileInputRef}
          onChange={handleFileChange}
          valueComponent={ValueComponent}
        />
      </GridCol>

      {(selectedFileRef.current || props.preview) && (
        <GridCol span={12}>
          <Image src={previewUrl || props.preview} alt="File Preview" style={{ maxWidth: "60%" }} />
        </GridCol>
      )}
    </Grid>
  );
}
