import { Button, Flex, Group, Highlight, Image, Modal, Text } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { TiInfoLarge } from "react-icons/ti";

export default function ModalExampleRenew() {
    const [opened, { open, close }] = useDisclosure(false);
    return (
        <>
            <Group justify="start" gap={5} pt={25}>
                <Text fz={{ base: 16, lg: 20 }}>เอกสารยืนยันการต่ออายุให้ผู้ต่ออายุเขียนดังนี้</Text>
                <Button
                    onClick={open}
                    style={{
                        backgroundColor: "#00A1E9",
                        borderRadius: "50%",
                        padding: "2px",
                        height: "25px",
                        width: "25px",
                    }}>
                    <TiInfoLarge size={30} style={{ color: "white" }} />
                </Button>
            </Group>
            <Group>
                <Highlight
                    highlight="(ชื่อ - นามสกุลผู้สมัคร)"
                    highlightStyles={{
                        WebkitBackgroundClip: "text",
                        WebkitTextFillColor: "#00A1E9",
                    }}
                    fz={{ base: 16, lg: 20 }}>
                    "ข้าพเจ้า (ชื่อ - นามสกุลผู้สมัคร) มีความประสงค์ต่ออายุสมาชิกพรรคประชาธิปัตย์"
                </Highlight>
            </Group>
            <Modal
                withCloseButton={false}
                opened={opened}
                centered={true}
                onClose={close}
                closeOnClickOutside={false}
                size="lg"
                w={{ base: "100%" }}
                radius={"lg"}>
                <Flex direction="column" align="center">
                    <Text fz={{ base: 18, lg: 30 }}>ตัวอย่างรูปภาพเอกสารยืนยันการต่ออายุ</Text>
                    <Text fz={{ base: 18, lg: 24 }} c={"#00A1E9"}>
                        ท่านสามารถดาวน์โหลดแบบฟอร์มได้ที่หน้าแรกของระบบ
                    </Text>
                    <Image src={"/login/MaskGroup5.png"} style={{ width: "50%", marginBottom: "20px" }} />
                    <Button onClick={close} variant="filled" radius="xl" w="100%" fz={18} fw={300} bg={"#01A1E8"}>
                        ปิด
                    </Button>
                </Flex>
            </Modal>
        </>
    )
}
