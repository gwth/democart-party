import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { MantineProvider } from "@mantine/core";
import { ModalsProvider } from "@mantine/modals";
import "mantine-datatable/styles.css";
import "mantine-datatable/styles.layer.css";
import "@mantine/dates/styles.css";
import "@mantine/core/styles.css";
import "@mantine/notifications/styles.css";

import { Provider } from "react-redux";
import { store } from "./store";
import { Notifications } from "@mantine/notifications";
import { BaseModal } from "./components/Modals/ModalContext/BaseModal";
import { DatesProvider } from "@mantine/dates";
import Cookie from "./components/CookieConsent/CookieConset";

import Error from "./components/Error/Error";
import Popup from "./components/Modals/ModalContext/Pupup";
ReactDOM.createRoot(document.getElementById("root")!).render(
  <MantineProvider
    withCssVariables
    theme={{
      fontFamily: "Prompt",
    }}>
    <DatesProvider settings={{ locale: "th", timezone: "Asia/Bangkok" }}>
      <ModalsProvider modals={{ baseModal: BaseModal }}>
        <Notifications />
        {/* <NavigationProgress /> */}
        <BrowserRouter>
          <Provider store={store}>
            <Error>
              <Cookie />
              <Popup />
              <App />
            </Error>
          </Provider>
        </BrowserRouter>
      </ModalsProvider>
    </DatesProvider>
  </MantineProvider>
);
