import secureLocalStorage from "react-secure-storage";
type ContentTypeValue = "application/json" | "empty";

type ContentType = {
  "Content-Type": ContentTypeValue;
};
type HttpMethod = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";

/**
 *
 * @param url api endpoint - example: /api/v1/exam/set-exam
 * @param method 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE'
 * @param options 'RequestInit' - example: { body: JSON.stringify({ name: 'John' }) }
 * @param headers 'HeadersInit' - example: { 'Content-Type': 'application/json' } or {} required `empty` when no need to send body is FormData
 * @returns Promise<T & { statusCode: number }>
 */
export async function fetchWithToken<T>(
  url: string,
  method: HttpMethod = "GET",
  options: RequestInit = {},
  headers: HeadersInit | ContentType | "empty" = { "Content-Type": "application/json" }
): Promise<T & { statusCode: number }> {
  const accessToken = secureLocalStorage.getItem("accessToken");

  const response = await fetch(url, {
    method,
    ...options,
    headers: {
      Authorization: `Bearer ${accessToken}`,
      ...(headers === "empty" ? {} : { ...headers }),
    },
  });

  const json = await response.json();

  return { ...json, statusCode: response.status } as T & { statusCode: number };
}
