import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import secureLocalStorage from "react-secure-storage";

interface AuthState {
  status: "loggedIn" | "unauthorized" | "failed";
}

const initialState: AuthState = {
  status: secureLocalStorage.getItem("accessToken") ? "loggedIn" : "unauthorized",
};

export const login = createAsyncThunk("auth/login", async (userLogin: { username: string; password: string }) => {
  const res = await fetch("/api/auth/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(userLogin),
  });

  if (res.status === 401) {
    return null;
  }

  const json = await res.json();

  return { ...json, statusCode: res.status };
});

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    logout: (state) => {
      state.status = "unauthorized";
      secureLocalStorage.removeItem("accessToken");
    },
  },
  extraReducers(builder) {
    builder
      .addCase(login.fulfilled, (state, action: PayloadAction<{ message: string; token: string; statusCode: number }>) => {
        if (action.payload.statusCode === 200) {
          secureLocalStorage.setItem("accessToken", action.payload.token);
          state.status = "loggedIn";
        } else {
          state.status = "unauthorized";
        }
      })
      .addCase(login.pending, (state) => {
        state.status = "unauthorized";
      })
      .addCase(login.rejected, (state) => {
        secureLocalStorage.removeItem("accessToken");

        state.status = "unauthorized";
      });
  },
});

export const { logout } = authSlice.actions;

export const selectAuthStatus = (state: { auth: AuthState }) => state.auth.status;

export default authSlice.reducer;
