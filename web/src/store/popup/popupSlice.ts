import { PayloadAction, createSlice } from "@reduxjs/toolkit";

interface initialState {
  isOpen: boolean;
  content: string | null;
  withButton?: boolean | null;
  onClose: () => void;
}

const initialState: initialState = {
  isOpen: false,
  content: null,
  withButton: true,
  onClose: () => {},
};

const popupSlice = createSlice({
  name: "popup",
  initialState,
  reducers: {
    openPopup: (state, action: PayloadAction<{ content: string; withButton?: boolean | null; onClose?: () => void }>) => {
      state.isOpen = true;
      state.content = action.payload.content;
      state.withButton = action.payload.withButton;
    },
    closePopup: (state, action: PayloadAction<{ onClose: () => void }>) => {
      action.payload.onClose();
      state.isOpen = false;
      state.content = null;
      state.withButton = true;
    },
  },
});

export const { openPopup, closePopup } = popupSlice.actions;
export default popupSlice.reducer;
