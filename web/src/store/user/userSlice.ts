import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import secureLocalStorage from "react-secure-storage";

export interface User {
  id?: number | null;
  username: string;
  password: string;
  birthDate: Date | null;
  role: number;
  firstName: string;
  lastName: string;
  create_at: Date;
  update_at: Date;
}

export interface UserState {
  value: User | null;
  status: "loggedIn" | "unauthorized" | "failed";
}

const initialState: UserState = {
  value: null,
  status: "unauthorized",
};

export const fetchUserMe = createAsyncThunk("user/fetchUserMe", async () => {
  const res = await fetchWithToken<{ data: User; message: string }>("/api/user/me", "GET");
  if (res.statusCode === 200) {
    return res;
  }
  return null;
});

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    clearUser: (state) => {
      state.value = null;
      state.status = "unauthorized";
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchUserMe.fulfilled, (state, action) => {
        if (action.payload?.statusCode === 200) {
          state.status = "loggedIn";
          state.value = action.payload.data;
        } else {
          secureLocalStorage.removeItem("accessToken");
          state.status = "unauthorized";
          state.value = null;
        }
      })
      .addCase(fetchUserMe.pending, (state) => {
        state.status = "unauthorized";
        state.value = null;
      })
      .addCase(fetchUserMe.rejected, (state) => {
        secureLocalStorage.removeItem("accessToken");
        state.status = "unauthorized";
        state.value = null;
      });
  },
});

export const { clearUser } = userSlice.actions;

export const selectUser = (state: { user: UserState }) => state.user.value;
export const selectUserStatus = (state: { user: UserState }) => state.user.status;

export default userSlice.reducer;
