import { Action, ThunkAction, configureStore } from "@reduxjs/toolkit";

import userSlice from "./user/userSlice";
import authSIice from "./auth/authSlice";
import popupSlice from "./popup/popupSlice";

export const store = configureStore({
  reducer: {
    user: userSlice,
    auth: authSIice,
    popup: popupSlice,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
