import { useEffect, useState } from "react";
import {
  Button,
  Checkbox,
  Flex,
  Grid,
  GridCol,
  Group,
  Image,
  Paper,
  Text,
  TextInput,
  Textarea,
} from "@mantine/core";
import { FaAngleLeft } from "react-icons/fa";
import { notifications } from "@mantine/notifications";

import { useNavigate, useParams } from "react-router-dom";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import dayjsCustom from "../../utils/dayjsCustom";
import AdminLayout from "../../components/Layout/AdminLayout";

interface registerLog {
  id: number;
  name_th: string;
  surname_th: string;
  name_en: string;
  agent_id: number;
  surname_en: string;
  birth_date: string;
  created_at: string;
  status: number;
  id_card: number;
  phone: number;
  type_prefix_th: any;
  province: any;
  tumbon: any;
  aumper: any;
  address_number: string;
  alley: string;
  soi: string;
  road: string;
  accept_type_id: number;
  payment: number;
  photo_bank: string;
  photo_id_card: string;
  photo_me: string;
  renew_tag: any;
  email: string;
  prefix_other_th: string;
  prefix_th_id: number;
  village_no: string;
}

export default function DetailAgent() {
  const { id } = useParams();
  const navigate = useNavigate();
  // const [value, setValue] = useState<ComboboxItem>();
  // const [valueother, setValueOther] = useState({ value: "" });
  const [chackValue, onChange] = useState(true);
  // const [opened, setClose] = useState(false);
  const [register, setRegister] = useState<registerLog>();

  const data = async () => {
    const getRegisteLogs = await fetchWithToken<{ data: registerLog }>(
      `/api/register/logs/${id}`
    );
    setRegister(getRegisteLogs.data);
  };

  useEffect(() => {
    if (id !== undefined) {
      data();
    }
  }, [id]);
  useEffect(() => {
    console.log("id", id);
  }, []);
  // const handleClose = () => {
  //   setClose(false);
  //   setValue(undefined);
  // };
  // const handleChange = (event: ChangeEvent<{ value: string }>) => {
  //   setValueOther({ value: event?.currentTarget?.value });
  // };
  // const handleSubmit = async (status: number, comment: string) => {
  //   function timeout(delay: number) {
  //     return new Promise((res) => setTimeout(res, delay));
  //   }
  //   const dataSent = {
  //     ids: [Number(id)],
  //     status: status,
  //     comment: comment,
  //   };
  //   if (value) {
  //     await fetchWithToken<{ data: registerLog }>(
  //       `/api/approval/set-status`,
  //       "POST",
  //       {
  //         body: JSON.stringify(dataSent),
  //       }
  //     ).then(
  //       async (res) => (
  //         res.statusCode === 200
  //           ? (notifications.show({
  //             color: "lime",
  //             title: "บันทึกสำเร็จ",
  //             message: "ระบบได้ทำการเปลี่ยนแปลงสถานะเรียบร้อย",
  //           }),
  //             setClose(false),
  //             await timeout(1000),
  //             notifications.clean(),
  //             navigate("/home"))
  //           : notifications.show({
  //             color: "red",
  //             title: "บันทึไม่สำเร็จ",
  //             message: `ไม่สามารถบันทึกได้เนื่องจากติดปัญหา ${res.statusCode}`,
  //           }),
  //         await timeout(1000),
  //         notifications.clean(),
  //         navigate("/home")
  //       )
  //     );
  //   } else {
  //     notifications.show({
  //       color: "red",
  //       title: "บันทึไม่สำเร็จ",
  //       message: "กรุณาระบุสาเหตุให้เรียบร้อย",
  //     });
  //   }
  // };

  const handleSubmitPass = async (status: number, comment: string) => {
    function timeout(delay: number) {
      return new Promise((res) => setTimeout(res, delay));
    }
    const dataSent = {
      ids: [Number(id)],
      status: status,
      comment: comment,
    };
    await fetchWithToken<{ data: registerLog }>(
      `/api/approval/set-status`,
      "POST",
      {
        body: JSON.stringify(dataSent),
      }
    ).then(
      async (res) => (
        res.statusCode === 200
          ? (notifications.show({
              color: "lime",
              title: "บันทึกสำเร็จ",
              message: "ระบบได้ทำการเปลี่ยนแปลงสถานะเรียบร้อย",
            }),
            // setClose(false),
            await timeout(1000),
            notifications.clean(),
            navigate("/home"))
          : notifications.show({
              color: "red",
              title: "บันทึไม่สำเร็จ",
              message: `ไม่สามารถบันทึกได้เนื่องจากติดปัญหา ${res.statusCode}`,
            }),
        await timeout(1000),
        notifications.clean(),
        navigate("/home")
      )
    );
  };

  const typePayment = (data: number) => {
    switch (data) {
      case 1:
        return (
          <Text w={"80%"} fz={{ base: 16, lg: 20 }}>
            จ่ายเงินสด
          </Text>
        );

      case 2:
        return (
          <Text w={"80%"} fz={{ base: 16, lg: 20 }}>
            แสกน QR Code
          </Text>
        );
      default:
        return null;
    }
  };
  const apcceptType = (data: number) => {
    switch (data) {
      case 1:
        return (
          <Text w={"80%"} fz={{ base: 16, lg: 20 }}>
            สมัครแบบรายปี 20 บาท/ปี
          </Text>
        );

      case 2:
        return (
          <Text w={"80%"} fz={{ base: 16, lg: 20 }}>
            สมัครแบบตลอดชีพ 200 บาท
          </Text>
        );
      default:
        return null;
    }
  };
  return (
    <AdminLayout>
      <Paper p={{ base: 10, md: 40 }}>
        <Group mb={{ base: 10, lg: 20 }}>
          <Button
            w={{ base: 60, lg: 86 }}
            h={33}
            bg={"#00A1E9"}
            radius={"xl"}
            onClick={() => navigate("/home")}
          >
            <FaAngleLeft size={25} />
          </Button>
          <Text c={"#00A1E9"}>ข้อมูลผู้สมัครสมาชิก</Text>
        </Group>
        <Grid>
          <GridCol span={{ base: 12, lg: 6 }}>
            <Image
              p={10}
              width={"100%"}
              height={"auto"}
              src={`${register?.photo_me ? register?.photo_me : ""}`}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 6 }}>
            <Image
              p={10}
              width={"100%"}
              height={"auto"}
              src={`${register?.photo_id_card ? register?.photo_id_card : ""}`}
            />
          </GridCol>
        </Grid>
        <Grid mt={{ base: 0, lg: 50 }}>
          <GridCol span={{ base: 12, lg: 4 }}>
            <Image
              p={10}
              width={"100%"}
              height={"auto"}
              src={`${register?.photo_bank ? register?.photo_bank : ""}`}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 8 }}>
            <Grid>
              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  คำนำหน้าชื่อ (ภาษาไทย)
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={
                    register?.type_prefix_th.data
                      ? register?.prefix_th_id == 4
                        ? register?.prefix_other_th
                        : register?.type_prefix_th.data
                      : ""
                  }
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>

              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  ชื่อ (ภาษาไทย)
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={register?.name_th ? register?.name_th : ""}
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>

              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  นามสกุล (ภาษาไทย)
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={
                    register?.surname_th ? register?.surname_th : ""
                  }
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>
            </Grid>
            <Grid>
              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  เลขประจำตัวประชาชน
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={register?.id_card ? register?.id_card : ""}
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>
              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  วันเกิด
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={
                    register?.birth_date
                      ? dayjsCustom.DateLongTH(new Date(register?.birth_date))
                      : ""
                  }
                  styles={{
                    input: {
                      height: "40px",
                      color: "black",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>
            </Grid>
            <Grid>
              <GridCol span={{ base: 12, lg: 12 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  ที่อยู่
                </Text>
                <Textarea
                  disabled
                  c={"#00A1E9"}
                  radius="lg"
                  variant="filled"
                  styles={{
                    input: {
                      height: "100px",
                      color: "black",
                      borderColor: "#00A1E9",
                    },
                  }}
                  value={`${
                    register?.address_number ? register?.address_number : ""
                  }  ${register?.village_no ? register?.village_no : ""} ${
                    register?.alley ? "ตรอก " + register?.alley : ""
                  } ${register?.soi ? "ซอย " + register?.soi : ""} ${
                    register?.road ? "ถนน " + register?.road : ""
                  }`}
                />
              </GridCol>
            </Grid>
            <Grid>
              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  จังหวัด
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={
                    register?.province?.name ? register?.province?.name : ""
                  }
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>

              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  เขต/อำเภอ
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={
                    register?.tumbon.name ? register?.tumbon.name : ""
                  }
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>

              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  แขวง/ตำบล
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={
                    register?.aumper.name ? register?.aumper.name : ""
                  }
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>
            </Grid>
            <Grid>
              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  เบอร์โทร
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={register?.phone ? register?.phone : ""}
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>
              <GridCol span={{ base: 12, lg: 4 }}>
                <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                  อีเมล
                </Text>
                <TextInput
                  disabled
                  c={"#00A1E9"}
                  radius="xl"
                  defaultValue={register?.email ? register?.email : ""}
                  styles={{
                    input: {
                      color: "black",
                      height: "40px",
                      borderColor: "#00A1E9",
                    },
                    label: {
                      fontSize: "20px",
                    },
                  }}
                />
              </GridCol>
            </Grid>
            {register?.accept_type_id ? (
              <>
                <Grid mt={{ base: 10, lg: 50 }}>
                  <GridCol span={{ base: 12, lg: 7 }}>
                    <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                      รูปแบบการสมัคร
                    </Text>
                    <Flex
                      justify="flex-start"
                      align="center"
                      direction="row"
                      wrap="wrap"
                    >
                      <Checkbox
                        onClick={() => onChange(!chackValue)}
                        checked={chackValue}
                        onChange={() => {}}
                        tabIndex={-1}
                        disabled
                        size="md"
                        radius={"xl"}
                        mr="xl"
                        styles={{ input: { cursor: "pointer" } }}
                        aria-hidden
                      />
                      {apcceptType(
                        register?.accept_type_id ? register?.accept_type_id : 0
                      )}
                    </Flex>
                  </GridCol>
                </Grid>
              </>
            ) : null}
            {register?.payment ? (
              <>
                <Grid mt={{ base: 5, lg: 10 }}>
                  <GridCol span={{ base: 12, lg: 7 }}>
                    <Text fz={{ base: 16, lg: 20 }} c={"#00A1E9"}>
                      การจ่ายเงิน
                    </Text>
                    <Flex
                      justify="flex-start"
                      align="center"
                      direction="row"
                      wrap="wrap"
                    >
                      <Checkbox
                        onClick={() => onChange(!chackValue)}
                        checked={chackValue}
                        onChange={() => {}}
                        tabIndex={-1}
                        disabled
                        size="md"
                        radius={"xl"}
                        mr="xl"
                        styles={{ input: { cursor: "pointer" } }}
                        aria-hidden
                      />
                      {typePayment(register?.payment ? register?.payment : 0)}
                    </Flex>
                  </GridCol>
                </Grid>
              </>
            ) : null}
          </GridCol>
        </Grid>
        {register?.status == 1 || register?.status == 2 ? (
          <></>
        ) : (
          <>
            <Grid
              justify="center"
              align="center"
              mt={{ base: 40, lg: 60 }}
              ml={{ base: 20, lg: 0 }}
            >
              {/* <Grid.Col span={{ base: 12, lg: 4 }}>
                {" "}
                <Button
                  w={{ base: 250, lg: 353 }}
                  bg={"transparent"}
                  c={"#00A1E9"}
                  radius={"xl"}
                  style={{ borderColor: "#00A1E9" }}
                  onClick={() => setClose(true)}
                >
                  ไม่ผ่านการตรวจสอบ
                </Button>
              </Grid.Col> */}
              <Grid.Col span={{ base: 12, lg: 4 }}>
                <Button
                  w={{ base: 250, lg: 353 }}
                  bg={"#00A1E9"}
                  radius={"xl"}
                  onClick={() => {
                    handleSubmitPass(1, "");
                  }}
                >
                  ผ่านการตรวจสอบ
                </Button>
              </Grid.Col>
            </Grid>
          </>
        )}
      </Paper>
      {/* {opened && (
        <Modal
          withCloseButton={false}
          opened={opened}
          centered={true}
          onClose={close}
          closeOnClickOutside={false}
          w={30}
          h={30}
          radius={"lg"}
        >
          <Flex direction="column" align="center">
            <Text fz={{ base: 18, lg: 21 }} mb={30} c={"#00A1E9"}>
              เหตุผลที่ไม่ผ่านการตรวจสอบ
            </Text>

            <Select
              display={"block"}
              pb={value?.value == "อื่นๆ" ? 10 : 70}
              flex={1}
              checkIconPosition="right"
              data={[
                "สลิปชื่อไม่ตรง",
                "ใบหน้าและภาพบัตรประชาชนไม่ตรงกัน",
                "อื่นๆ",
              ]}
              onChange={(_value, option) => setValue(option)}
              placeholder="กรุณาเลือก"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  width: "380px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                },
              }}
            />
            {value?.value == "อื่นๆ" ? (
              <TextInput
                radius="xl"
                pb={20}
                onChange={handleChange}
                styles={{
                  input: {
                    height: "40px",
                    width: "380px",
                    borderColor: "#00A1E9",
                  },
                  label: {
                    fontSize: "20px",
                  },
                }}
              />
            ) : null}
            <Grid justify="between">
              <GridCol span={{ base: 6, lg: 6 }}>
                <Button
                  type="submit"
                  bg={"transparent"}
                  c={"#00A1E9"}
                  w={180}
                  radius={"xl"}
                  style={{ borderColor: "#00A1E9" }}
                  onClick={handleClose}
                >
                  ยกเลิก
                </Button>
              </GridCol>
              <GridCol span={{ base: 6, lg: 6 }}>
                <Button
                  onClick={() => {
                    handleSubmit(
                      2,
                      value?.value
                        ? value?.value == "อื่นๆ"
                          ? valueother?.value
                          : value?.value
                        : ""
                    );
                  }}
                  variant="filled"
                  w={180}
                  radius="xl"
                  fw={300}
                  bg={"#01A1E8"}
                >
                  ยืนยัน
                </Button>
              </GridCol>
            </Grid>
          </Flex>
        </Modal>
      )} */}
    </AdminLayout>
  );
}
