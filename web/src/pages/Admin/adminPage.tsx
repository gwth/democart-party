import { useEffect, useState } from "react";
import sortBy from "lodash/sortBy";
import {
  Button,
  Flex,
  Grid,
  GridCol,
  Modal,
  Paper,
  Select,
  Text,
  TextInput,
} from "@mantine/core";
import { IoSearch } from "react-icons/io5";
import { rem } from "@mantine/core";
import { DataTable, DataTableSortStatus } from "mantine-datatable";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import { useNavigate } from "react-router-dom";
import dayjs from "dayjs";
import AdminLayout from "../../components/Layout/AdminLayout";
import { IconChevronUp, IconSelector } from "@tabler/icons-react";
import { notifications } from "@mantine/notifications";
import Loadingpage from "../../components/elements/Loadingpage";
const PAGE_SIZE = 20;
export default function AdminPage() {
  const navigate = useNavigate();
  const search = localStorage.getItem("search");
  const [page, setPage] = useState(1);
  const [opened, setClose] = useState(false);
  const [loading, setLoading] = useState(false);
  const navgate = useNavigate();
  const [totalcount, setTotalcount] = useState(0);
  const [register, setRegister] = useState<registerLog[]>([]);
  const [status, setStatus] = useState(-1);
  const [textSearch, setTextSearch] = useState("");
  const [selectedRecords, setSelectedRecords] = useState<any[]>([]);
  const [sortStatus, setSortStatus] = useState<
    DataTableSortStatus<registerLog>
  >({
    columnAccessor: "name",
    direction: "asc",
  });

  useEffect(() => {
    const data = sortBy(register, sortStatus.columnAccessor) as registerLog[];
    setRegister(sortStatus.direction === "desc" ? data.reverse() : data);
  }, [sortStatus]);

  interface registerLog {
    id: number;
    id_card: number;
    name_th: string;
    surname_th: string;
    name_en: string;
    agent_id: number;
    surname_en: string;
    birth_date: string;
    created_at: string;
    status: number;
    comment: string;
    type_register_id: number;
    users_register_log_agent_idTousers: {
      first_name: string;
      last_name: string;
    };
  }

  const icon = <IoSearch style={{ width: rem(16), height: rem(16) }} />;
  const data = async () => {
    const getRegisteLogs = await fetchWithToken<{
      data: registerLog[];
      totalCount: number;
    }>(
      `/api/register/logs-list?skip=${(page - 1) * PAGE_SIZE
      }&pagesize=${PAGE_SIZE}&textSearch=${search ? search : ""}`
    );
    setRegister(getRegisteLogs?.data);
    setTotalcount(getRegisteLogs?.totalCount);
  };

  useEffect(() => {
    data();
  }, [page]);

  const handleSelectChange = (option: string) => {
    switch (option) {
      case "ทั้งหมด":
        return setStatus(-1);
      case "สมัครตนเอง":
        return setStatus(1);
      case "สมัครผ่านตัวแทน":
        return setStatus(2);
      case "ต่ออายุด้วยตนเอง":
        return setStatus(3);
      case "ต่ออายุผ่านตัวแทน":
        return setStatus(4);
      default:
        break;
    }
  };
  const refresh = () => window.location.reload();
  const dataId = selectedRecords.map((data) => data.id);
  const handleSearch = async () => {
    setPage(1);
    const getRegisteLogs = await fetchWithToken<{
      data: registerLog[];
      totalCount: number;
    }>(
      ` /api/register/logs-list?skip=${(page - 1) * PAGE_SIZE
      }&pagesize=${PAGE_SIZE}&textSearch=${textSearch
      }&status=${status == -1 ? "" : status}`
    );
    // &status=${status}
    setRegister(getRegisteLogs?.data);
    setTotalcount(getRegisteLogs?.totalCount);
  };
  console.log('status', status)
  const handleSubmitPass = async (status: number, comment: string) => {
    // function timeout(delay: number) {
    //   return new Promise((res) => setTimeout(res, delay));
    // }

    console.log("dataId.lenght", dataId.length);
    if (dataId.length) {
      const dataSent = {
        ids: dataId,
        status: status,
        comment: comment,
      };
      setLoading(true);
      await fetchWithToken<{ data: registerLog }>(
        `/api/approval/set-status`,
        "POST",
        {
          body: JSON.stringify(dataSent),
        }
      ).then(
        (res) => (
          res.statusCode === 200
            ? (notifications.show({
              color: "lime",
              title: "บันทึกสำเร็จ",
              message: "ระบบได้ทำการเปลี่ยนแปลงสถานะเรียบร้อย",
            }),
              setLoading(false),
              notifications.clean(),
              refresh(),
              navigate("/home"))
            : notifications.show({
              color: "red",
              title: "บันทึไม่สำเร็จ",
              message: `ไม่สามารถบันทึกได้เนื่องจากติดปัญหา ${res.statusCode}`,
            }),
          refresh(),
          setSelectedRecords([]),
          notifications.clean(),
          navigate("/home")
        )
      );
    } else {
      notifications.show({
        color: "red",
        title: "บันทึไม่สำเร็จ",
        message: `กรุณาเลือกข้อมูลที่ต้องการยืนยัน`,
      });
    }
  };
  return (
    <AdminLayout>
      <Paper p={{ base: 10, md: 40 }}>
        <Grid justify="space-between" align="center">
          <GridCol span={{ base: 12, lg: 6 }}>
            <Text fz={17} c={"#00A1E9"}>
              รายชื่อผู้สมัครสมาชิกพรรคประชาธิปัตย์
            </Text>
          </GridCol>
          <GridCol span={{ base: 12, lg: 3 }}>
            <TextInput
              w={"100%"}
              c={"#00A1E9"}
              leftSection={icon}
              value={textSearch || localStorage.getItem("search") || ""}
              onChange={(_event) => {
                localStorage.setItem("search", _event.currentTarget.value);
                setTextSearch(_event.currentTarget.value);
              }}
              placeholder="ค้นหา"
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
              }}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 2 }}>
            <Select
              display={"block"}
              flex={1}
              checkIconPosition="right"
              data={["ทั้งหมด", "สมัครตนเอง", "สมัครผ่านตัวแทน", "ต่ออายุด้วยตนเอง", "ต่ออายุผ่านตัวแทน"]}
              onChange={(_value, option) => {
                handleSelectChange(option?.value);
              }}
              placeholder="เลือกรูปแบบ"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
              }}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 1 }}>
            <Button
              w={{ base: "100%", lg: "100px" }}
              radius={"xl"}
              bg={"#00A1E9"}
              onClick={handleSearch}
            >
              ค้นหา
            </Button>
          </GridCol>
          <GridCol span={{ base: 12, lg: 2 }}>
            <Button
              w={{ base: "100%", lg: "150px" }}
              ml={{ base: 0, lg: 40 }}
              disabled={dataId.length <= 0 || loading}
              radius={"xl"}
              bg={dataId.length <= 0 || loading ? "#d4dfeb" : "#01A1E8"}
              onClick={() => setClose(true)}
            >
              ผ่านการตรวจสอบ
            </Button>
          </GridCol>
        </Grid>
        {loading ? (
          <>
            <Loadingpage />
          </>
        ) : (
          <>
            {" "}
            <DataTable
              mt={30}
              sortStatus={sortStatus}
              onSortStatusChange={(sortStatus) => {
                setSortStatus(sortStatus);
              }}
              withTableBorder
              borderRadius={"5px"}
              paginationSize="md"
              striped
              paginationActiveBackgroundColor={"#00A1E9"}
              height={942}
              rowStyle={() => ({
                minHeight: "70px",
                fontSize: "16px",
              })}
              styles={{
                table: {
                  color: "black",
                },
                header: {
                  color: "white",
                  fontSize: "20",
                  backgroundColor: "#00A1E9",
                  height: "70px",
                },
                pagination: {
                  color: "black",
                  height: "80px",
                },
              }}
              noRecordsText={totalcount ? "ไม่พบข้อมูล" : ""}
              records={register}
              sortIcons={{
                sorted: <IconChevronUp size={20} />,
                unsorted: <IconSelector size={20} />,
              }}
              verticalAlign="center"
              recordsPerPage={PAGE_SIZE}
              totalRecords={totalcount ? totalcount : 0}
              page={page}
              selectedRecords={selectedRecords}
              onSelectedRecordsChange={setSelectedRecords}
              paginationText={({ from, to, totalRecords }) =>
                `รายการที่ ${from} - ${to} จากทั้งหมด ${totalRecords} รายการ`
              }
              onPageChange={(p) => setPage(p)}
              loadingText="กำลังโหลดข้อมูล"
              columns={[
                {
                  // sortable: true,
                  accessor: "id_card",
                  title: "เลขประจำตัวประชาชน",
                  textAlign: "left",
                  width: "15%",
                },
                {
                  // sortable: true,
                  accessor: "name_th",
                  render: ({ name_th, surname_th }) => {
                    return (
                      <Text>
                        {name_th} {surname_th}
                      </Text>
                    );
                  },
                  title: "ชื่อ-สกุล",
                  width: "20%",
                },
                {
                  // sortable: true,
                  accessor: "created_at",
                  title: "วันและเวลาที่สมัคร",
                  width: "15%",
                  render: ({ created_at }) => {
                    return (
                      <Text>
                        {dayjs(created_at).format("DD/MM/YYYY HH:mm")}
                      </Text>
                    );
                  },
                },
                {
                  // sortable: true,
                  accessor: "agent_id",
                  title: "รูปแบบการสมัคร",
                  render: ({
                    agent_id,
                    type_register_id,
                    users_register_log_agent_idTousers,
                  }) => {
                    if (agent_id && type_register_id === 4) {
                      return (
                        <Text c={"orange"}>
                          ต่ออายุโดย{" "}
                          {users_register_log_agent_idTousers?.first_name}{" "}
                          {users_register_log_agent_idTousers?.last_name}
                        </Text>
                      );
                    } else if (agent_id) {
                      return (
                        <Text>
                          {users_register_log_agent_idTousers?.first_name}{" "}
                          {users_register_log_agent_idTousers?.last_name}
                        </Text>
                      );
                    } else {
                      switch (type_register_id) {
                        case 1:
                          return <Text>สมัครด้วยตัวเอง</Text>;
                        case 3:
                          return <Text c={"orange"}>ต่ออายุสมาชิกพรรค</Text>;
                        default:
                          return null;
                      }
                    }
                  },
                  width: "35%",
                },
                {
                  // sortable: true,
                  accessor: "status",
                  title: "สถานะ",
                  render: ({ status, comment }) => {
                    switch (status) {
                      case 0:
                        return <Text>กำลังตรวจสอบ</Text>;
                      case 1:
                        return <Text c={"green"}>ผ่านการตรวจสอบ</Text>;
                      case 2:
                        return (
                          <Text c={"red"}>
                            {comment ? comment : "ไม่ได้ระบุสาเหตุ"}
                          </Text>
                        );
                      default:
                        return null;
                    }
                  },
                  width: "15%",
                },
              ]}
              // execute this callback when a row is clicked
              onRowClick={({ record: data }) =>
                navgate(`/detail-agent/${data?.id}`)
              }
            />
          </>
        )}
        {opened && (
          <Modal
            withCloseButton={false}
            opened={opened}
            centered={true}
            onClose={close}
            closeOnClickOutside={false}
            w={30}
            h={30}
            radius={"lg"}
          >
            <Flex direction="column" align="center">
              <Text fz={{ base: 18, lg: 21 }} my={30} c={"#00A1E9"}>
                ยืนยันผ่านการตรวจสอบ
              </Text>

              <Grid justify="between">
                <GridCol span={{ base: 6, lg: 6 }}>
                  <Button
                    type="submit"
                    bg={"transparent"}
                    c={"#00A1E9"}
                    w={180}
                    radius={"xl"}
                    style={{ borderColor: "#00A1E9" }}
                    onClick={() => setClose(false)}
                  >
                    ยกเลิก
                  </Button>
                </GridCol>
                <GridCol span={{ base: 6, lg: 6 }}>
                  <Button
                    onClick={() => {
                      handleSubmitPass(1, "");
                      setClose(false);
                    }}
                    variant="filled"
                    w={180}
                    radius="xl"
                    fw={300}
                    bg={"#01A1E8"}
                  >
                    ยืนยัน
                  </Button>
                </GridCol>
              </Grid>
            </Flex>
          </Modal>
        )}
      </Paper>
    </AdminLayout>
  );
}
