import { useEffect, useState } from "react";

import {
  Box,
  Button,
  Divider,
  Flex,
  Grid,
  GridCol,
  Image,
  Paper,
  Text,
  TextInput,
} from "@mantine/core";
import { isNotEmpty, useForm } from "@mantine/form";
import { rem } from "@mantine/core";
import { DatePickerInput } from "@mantine/dates";
import { IconCalendar } from "@tabler/icons-react";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import RegisterLayout from "../../components/Layout/RegisterLayout";
import { GoChevronLeft } from "react-icons/go";
import { useNavigate } from "react-router-dom";

export default function CheckUserData() {
  const navigate = useNavigate();

  const [data, setData] = useState<any | null>(null);

  const icon = (
    <IconCalendar style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
  );
  const userForm = useForm({
    initialValues: {
      idCard: "",
      birthDate: null,
    },
    validate: {
      birthDate: isNotEmpty("กรุณาใส่วันเกิด"),
      idCard: isNotEmpty("กรุณาใส่เลขประจําตัวประชาชน"),
    },
  });
  async function searchClick() {
    const validationErrors = userForm.validate();
    if (validationErrors.hasErrors) return;
    const data = userForm.getTransformedValues();
    let birthDate = new Date();
    if (data.birthDate) {
      birthDate = new Date(data.birthDate);
    }
    const res = await fetchWithToken<{
      data: any;
      message: string;
      statusCode: number;
    }>(
      `/api/checkstatus/get-status-user-register?idCard=${data.idCard}&birthDate=${birthDate}`,
      "GET"
    );
    if (res.data) {
      console.log("searchClick ", res.data);
      setData(res.data);
    }
  }
  const statusElement = () => {
    // 0 = wait ,1 = approve ,2 = reject
    const text =
      data.status == 1
        ? "พบข้อมูลสมาชิกพรรค"
        : "ไม่พบข้อมูลสมาชิกพรรค<br/>โปรดตรวจสอบเลขประจำตัวประชาชน และ วันเกิดของท่านอีกครั้ง";
    const textFix = data.status == 1 ? "ข้อมูลสมาชิก" : "คำชี้แจง";
    const image =
      data.status == 1 ? "user_status/approve.svg" : "user_status/reject.svg";
    return (
      <Box mt={50}>
        <Divider my="md" />
        <Flex gap="xl" justify="center" align="center" direction="row">
          <Image mah={57} maw={57} src={image}></Image>
          <Text dangerouslySetInnerHTML={{ __html: text || "" }} fz={22}></Text>
        </Flex>

        {data.status == 1 && data.dataUser && (
          <Flex pt={30} direction="column" gap="md">
            <Text fz={20} fw="bold" c={"#00A1E9"}>
              {textFix}
            </Text>
            <Grid>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>{"หมายเลขสมาชิก : " + data.dataUser.mid}</Text>
              </Grid.Col>

              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>
                  {"วันที่เป็นสมาชิก : " + data.dataUser.since_member}
                </Text>
              </Grid.Col>
            </Grid>

            <Grid>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>
                  {"สมาชิกประเภท : " +
                    (data.dataUser.deprived_date == "ตลอดชีพ"
                      ? "ตลอดชีพ"
                      : "รายปี")}
                </Text>
              </Grid.Col>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>
                  {/* {"วันที่สิ้นสุด : " +
                    (data.dataUser.deprived_date == "ตลอดชีพ"
                      ? "ตลอดชีพ"
                      : data.dataUser.expire)} */}
                </Text>
              </Grid.Col>
            </Grid>
            <Grid>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>
                  {"ชื่อ-นามสกุล : " +
                    data.dataUser.title +
                    data.dataUser.f_name +
                    " " +
                    data.dataUser.l_name}
                </Text>
              </Grid.Col>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>
                  {"เลขประจำตัวประชาชน : " + data.dataUser.idcard}
                </Text>
              </Grid.Col>
            </Grid>

            <Grid>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>{"วันเกิด : " + data.dataUser.birthday}</Text>
              </Grid.Col>
              <Grid.Col span={{ base: 12, xs: 6 }}>
                <Text fz={20}>{"เบอร์โทร : " + data.dataUser.tel}</Text>
              </Grid.Col>
            </Grid>

            <Grid>
              <Grid.Col span={{ base: 12 }}>
                <Text fz={20}>
                  {"ที่อยู่ : " +
                    data.dataUser.add_no_current +
                    " " +
                    data.dataUser.sub_district_current +
                    " " +
                    data.dataUser.district_current +
                    " " +
                    data.dataUser.province}
                </Text>
              </Grid.Col>
            </Grid>
          </Flex>
        )}
        <Divider my="md" />
        <Text fz={20}>{"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}</Text>
      </Box>
    );
  };
  useEffect(() => {}, []);
  return (
    <RegisterLayout>
      <Paper p={{ base: 10, md: 40 }} mih={"100%"}>
        <Grid pt={30} pb={{ base: 10, lg: 20 }} align="center" justify="start">
          <Button
            radius="lg"
            onClick={() => {
              navigate("/");
            }}
          >
            <GoChevronLeft size="md"></GoChevronLeft>
          </Button>
          <Text mx={10} fz={{ base: 16, lg: 20 }} fw="bold" c={"#00A1E9"}>
            ข้อมูลสมาชิกพรรค
          </Text>
        </Grid>

        <Grid pt={20}>
          <GridCol span={{ base: 12, lg: 4 }}>
            <Text fz={20} fw="bold" c={"#00A1E9"}>
              เลขประจําตัวประชาชน
            </Text>
            <TextInput
              placeholder="เลขประจําตัวประชาชน"
              // c={"#00A1E9"}

              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },

                label: {
                  fontSize: "20px",
                },
              }}
              {...userForm.getInputProps("idCard")}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 4 }}>
            <Text fz={20} fw="bold" c={"#00A1E9"}>
              วันเกิด
            </Text>
            <DatePickerInput
              placeholder="วัน เดือน ปี"
              valueFormat="DD MMM BBBB"
              yearsListFormat="BBBB"
              yearLabelFormat="BBBB"
              decadeLabelFormat="BBBB"
              monthLabelFormat="MMMM BBBB"
              locale="th-th"
              withAsterisk
              display={"block"}
              c={"#00A1E9"}
              radius="xl"
              rightSection={icon}
              rightSectionPointerEvents="none"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...userForm.getInputProps("birthDate")}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 4 }} mt={{ base: 0, lg: 30 }}>
            <Button
              variant="filled"
              radius="xl"
              h={"40px"}
              fz={20}
              fw={300}
              bg={"#01A1E8"}
              type="submit"
              w={{ base: "100%", lg: "auto" }}
              onClick={searchClick}
            >
              ค้นหาข้อมูล
            </Button>
          </GridCol>
        </Grid>
        {data && statusElement()}
        {/* //have data showdetail */}
      </Paper>
    </RegisterLayout>
  );
}
