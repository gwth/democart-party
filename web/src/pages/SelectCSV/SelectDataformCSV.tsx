import {
  TextInput,
  Text,
  Textarea,
  Button,
  Paper,
  Flex,
  Loader,
  Group,
  Checkbox,
  Image,
  LoadingOverlay,
  NumberInput,
  Grid,
  GridCol,
  Title,
  Box,
} from "@mantine/core";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { IRegisterlog, UserData } from "../../interfaces/userDataType";
import Imgupload from "../../components/upload/img";
import { FileCustom } from "../../interfaces/file.interface";
import { useForm } from "@mantine/form";
import { DatePickerInput } from "@mantine/dates";
import AdminLayout from "../../components/Layout/AdminLayout";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import { modals } from "@mantine/modals";
import { useDisclosure } from "@mantine/hooks";
import { IconPlaystationX } from "@tabler/icons-react";
import { GoChevronLeft } from "react-icons/go";

export default function SelectDataformCSV() {
  const { id: selectDataId } = useParams();
  const navigate = useNavigate();
  const [check1, setCheck1] = useState<boolean>(false);
  const [check2, setCheck2] = useState<boolean>(false);
  const [user, setUser] = useState<UserData | null>(null);

  // const [fileImagePayment, setFileImagePayment] = useState<File | null>(null);
  const [fileImageApprove, setFileImageApprove] = useState<File | null>(null);
  const [fileImageCardIdentification, setFileImageCardIdentification] =
    useState<File | null>(null);

  const [fileImageApproveCustomFile, setFileImageApproveCustomFile] =
    useState<FileCustom | null>(null);
  const [
    fileImageCardIdentificationCustomFile,
    setFileImageCardIdentificationCustomFile,
  ] = useState<FileCustom | null>(null);
  // const [fileImagePaymentCustomFile, setFileImagePaymentCustomFile] =
  //   useState<FileCustom | null>(null);

  const [openedLoader, { open: openLoader, close: closeLoader }] =
    useDisclosure(false);

  const typeRegister = [
    { name: "สมัครแบบรายปี", des: "สมัครแบบรายปี 20 บาท/ปี", id: 1 },
    { name: "สมัครแบบตลอดชีพ", des: "สมัครแบบตลอดชีพ 200 บาท", id: 2 },
  ];

  const typePayment = [
    { name: "จ่ายเงินสด", des: "จ่ายเงินสด", id: 1 },
    // { name: "แสกน QR Code", des: "แสกน QR Code", id: 2 },
  ];

  const fetchTombon = async (amphoeId: string): Promise<{ data: any }> => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await fetch(`/api/common/tumbon/${amphoeId}`);
        const data = await response.json();
        resolve(data);
      } catch (error) {
        reject();
        // console.error("Error fetching tombons:", error);
      }
    });
  };

  const registerForm = useForm<IRegisterlog>({
    transformValues: (values) => ({
      ...values,
      telephone: values.telephone.toString(),
    }),
    validate: {
      telephone: (value) => {
        return !value || value.toString().length < 9
          ? "กรุณาใส่เบอร์โทรศัพท์"
          : null;
      },
    },
  });

  useEffect(() => {
    window.scrollTo(0, 0);
    async function initialize() {
      const userArr: UserData[] = JSON.parse(
        localStorage.getItem("userDataExcel") || "[]"
      );
      const userIndex = userArr.findIndex((u) => u.id === Number(selectDataId));
      const dataPrefix = await fetchWithToken<{ data: any }>(
        `/api/common/prefix-name?prefix_name=${userArr[userIndex].prefixTh}`
      );
      const dataprovince = await fetchWithToken<{ data: any }>(
        `/api/common/province-name?province_name=${userArr[
          userIndex
        ]?.province.replace("จังหวัด", "")}`
      );
      const dataAmphoe = await fetchWithToken<{ data: any }>(
        `/api/common/aumper-name?aumper_name=${userArr[userIndex].amphoe
          .replace("เขต", "")
          .replace("อำเภอ", "")}`
      );

      const data = await fetchTombon(dataAmphoe.data[0].id.toString());

      const tombonSubString = userArr[userIndex].tombon
        .replace("แขวง", "")
        .replace("ตำบล", "");
      const tombonId = data.data.filter((f: { name: string | undefined }) => {
        return f.name == tombonSubString;
      });

      if (userArr) {
        registerForm.initialize({
          // prefix: userArr[userIndex].prefixTh,
          // prefixOther: "",
          prefixTh: userArr[userIndex].prefixTh,
          prefix:
            dataPrefix.data === null ? "4" : dataPrefix.data.id.toString(),
          prefixOther: userArr[userIndex].prefixTh,
          firstName: userArr[userIndex].nameTh,
          lastName: userArr[userIndex].lastNameTh,
          firstNameEn: userArr[userIndex].nameEng,
          lastNameEn: userArr[userIndex].lastNameEng,
          idCard: userArr[userIndex].idCard,
          birthDate: new Date(userArr[userIndex].birthDate),
          houseNumber: userArr[userIndex].address_number,
          moo: userArr[userIndex].village_no,
          building: userArr[userIndex].alley,
          soi: userArr[userIndex].soi,
          road: userArr[userIndex].road,
          tombon: tombonId[0].id.toString(),
          amphoe: dataAmphoe.data[0].id.toString(),
          province: dataprovince.data.id.toString(),
          address:
            userArr[userIndex].address_number +
            " " +
            userArr[userIndex].village_no +
            " " +
            userArr[userIndex].alley +
            " " +
            userArr[userIndex].soi +
            " " +
            userArr[userIndex].road +
            " " +
            userArr[userIndex].tombon +
            " " +
            userArr[userIndex].amphoe +
            " " +
            userArr[userIndex].province,
          prefixEn: null,
          telephone: "",
          typePayment: "1",
          acceptTypId: "1",
          typeRegister: "2",
          agent: null,
          status: "0",
          renewTag: "",
          email: "",
        });
      }
      setUser(userArr[userIndex]);
    }
    initialize();
  }, [selectDataId]);

  const handleSubmit = async () => {
    const validate = registerForm.validate();
    if (
      validate.hasErrors ||
      !fileImageApprove ||
      !fileImageCardIdentification
    ) {
      let str = "กรุณากรอกข้อมูลให้ครบ <br/>";
      Object.keys(validate.errors).forEach((key) => {
        const value = validate.errors[key];
        console.log(`Key: ${key}, Value: ${value}`);
        str += `- ${value}<br/>`;
      });
      if (!fileImageApprove) {
        str += `- รูปถ่ายตนเองพร้อมเอกสารยืนยันการสมัคร<br/>`;
      }
      if (!fileImageCardIdentification) {
        str += `- รูปบัตรประชาชน<br/>`;
      }

      modals.openContextModal({
        modal: "baseModal",
        withCloseButton: false,
        centered: true,

        styles: {
          content: {
            borderRadius: "25px",
          },
        },
        innerProps: {
          modalBody: (
            <>
              <Text
                ml={20}
                fz={{ base: 16, md: 20, lg: 20 }}
                c={"red"}
                dangerouslySetInnerHTML={{ __html: str }}
              ></Text>
              <Button onClick={() => modals.closeAll()} mt={20} w={"100%"}>
                ตกลง
              </Button>
            </>
          ),
        },
      });

      return;
    }

    if (!fileImageApprove) {
      return;
    }
    if (!fileImageCardIdentification) {
      return;
    }
    openLoader();
    // console.log(registerForm.getTransformedValues());
    // console.log(registerForm.values);
    if (fileImageApprove && fileImageCardIdentification) {
      const formData = new FormData();
      formData.append("fileImageApprove", fileImageApprove);
      formData.append(
        "fileImageCardIdentification",
        fileImageCardIdentification
      );
      // if (fileImagePayment) {
      //   formData.append("fileImagePayment", fileImagePayment);
      // }

      const res = await fetch(
        `/api/register/check-member/${
          registerForm.getTransformedValues().idCard
        }`,
        {
          headers: {
            "x-api-key":
              "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
          },
        }
      );

      const result: { message: string; isMember: boolean } = await res.json();

      if (res.status == 200) {
        if (result.isMember) {
          modals.openContextModal({
            modal: "baseModal",
            withCloseButton: false,
            centered: true,
            styles: {
              content: {
                borderRadius: "25px",
              },
            },
            innerProps: {
              modalBody: (
                <Flex
                  my={25}
                  mx={15}
                  w={"100%"}
                  justify="center"
                  align="center"
                  direction="column"
                >
                  <IconPlaystationX size={90} color="#F93B48" stroke="1" />
                  <Title mt={15} size={20} c="gray">
                    ดำเนินการไม่สำเร็จ
                  </Title>
                  <Text
                    fz={18}
                    mt={10}
                    fw="normal"
                    ta="center"
                    c={"gray"}
                    dangerouslySetInnerHTML={{ __html: result.message }}
                  ></Text>
                  <Button
                    radius="xl"
                    onClick={() => modals.closeAll()}
                    w={"40%"}
                    mt={20}
                  >
                    ตกลง
                  </Button>
                </Flex>
              ),
            },
          });
          closeLoader();
          return;
        }

        formData.append(
          "registerData",
          JSON.stringify(registerForm.getTransformedValues())
        );

        fetchWithToken<{ statusCode: number }>(
          `/api/register/logs`,
          "POST",
          {
            body: formData,
          },
          "empty"
        )
          .then((res) => {
            if (res.statusCode == 201) {
              modals.openContextModal({
                modal: "baseModal",
                withCloseButton: false,
                centered: true,
                styles: {
                  content: {
                    borderRadius: "25px",
                  },
                },
                innerProps: {
                  modalBody: (
                    <>
                      <Image
                        src={"/login/Group334.png"}
                        style={{ width: "35%", padding: "25px" }}
                      />
                      <Text fz={22} fw="bold" c={"#00A1E9"}>
                        การสมัครสมาชิกสำเร็จ
                      </Text>
                      <Button
                        onClick={() => {
                          modals.closeAll();
                          navigate("/selectcsv", { replace: true });
                        }}
                        variant="filled"
                        radius="xl"
                        w="60%"
                        mt={20}
                        h={40}
                        fz={18}
                        fw={300}
                        bg={"#01A1E8"}
                      >
                        ปิด
                      </Button>
                    </>
                  ),
                },
              });
            }
          })
          .finally(() => closeLoader());
      } else {
        modals.openContextModal({
          modal: "baseModal",
          withCloseButton: false,
          centered: true,
          styles: {
            content: {
              borderRadius: "25px",
            },
          },
          innerProps: {
            modalBody: (
              <Flex
                my={25}
                mx={15}
                w={"100%"}
                justify="center"
                align="center"
                direction="column"
              >
                <IconPlaystationX size={90} color="#F93B48" stroke="1" />
                <Title mt={15} size={20} c="gray">
                  ดำเนินการไม่สำเร็จ
                </Title>
                <Text
                  fz={18}
                  mt={10}
                  fw="normal"
                  ta="center"
                  c={"gray"}
                  dangerouslySetInnerHTML={{ __html: result.message }}
                ></Text>
                <Button
                  radius="xl"
                  onClick={() => modals.closeAll()}
                  w={"40%"}
                  mt={20}
                >
                  ตกลง
                </Button>
              </Flex>
            ),
          },
        });
      }
    }
  };

  return (
    <AdminLayout>
      <Paper p={{ base: 10, md: 40 }}>
        <Button
          radius="lg"
          onClick={() => {
            navigate("/selectcsv");
          }}
        >
          <GoChevronLeft size="md"></GoChevronLeft>
        </Button>
        <LoadingOverlay
          visible={openedLoader}
          zIndex={1000}
          overlayProps={{ radius: "sm", blur: 2 }}
        />

        {user ? (
          <div>
            <Text pt={15} fw={500} size="md" c={"#00A1E9"}>
              คำนำหน้าชื่อ (ภาษาไทย)
            </Text>
            <TextInput
              disabled
              variant="filled"
              size="sm"
              radius="xl"
              placeholder=""
              c="#00A1E9"
              styles={{
                input: {
                  backgroundColor: "#EFFBFF",
                },
              }}
              {...registerForm.getInputProps("prefixTh")}
            />

            <Text pt={30} fw={500} size="md" c={"#00A1E9"}>
              ชื่อ (ภาษาไทย)
            </Text>
            <TextInput
              disabled
              variant="filled"
              size="sm"
              radius="xl"
              placeholder=""
              styles={{
                input: {
                  backgroundColor: "#EFFBFF",
                },
              }}
              {...registerForm.getInputProps("firstName")}
            />
            <Text pt={30} fw={500} size="md" c={"#00A1E9"}>
              นามสกุล (ภาษาไทย)
            </Text>
            <TextInput
              disabled
              variant="filled"
              size="sm"
              radius="xl"
              placeholder=""
              styles={{
                input: {
                  backgroundColor: "#EFFBFF",
                },
              }}
              {...registerForm.getInputProps("lastName")}
            />
            <Text pt={30} fw={500} size="md" c={"#00A1E9"}>
              เลขประจำตัวประชาชน
            </Text>
            <TextInput
              disabled
              variant="filled"
              size="sm"
              radius="xl"
              placeholder=""
              styles={{
                input: {
                  backgroundColor: "#EFFBFF",
                },
              }}
              {...registerForm.getInputProps("idCard")}
            />
            <Text pt={30} fw={500} size="md" c={"#00A1E9"}>
              วันเกิด
            </Text>
            <DatePickerInput
              disabled
              variant="filled"
              size="sm"
              radius="xl"
              placeholder=""
              valueFormat="DD MMM BBBB"
              yearsListFormat="BBBB"
              yearLabelFormat="BBBB"
              decadeLabelFormat="BBBB"
              monthLabelFormat="MMMM BBBB"
              styles={{
                input: {
                  backgroundColor: "#EFFBFF",
                },
              }}
              {...registerForm.getInputProps("birthDate")}
            />
            <Text pt={30} fw={500} size="md" c={"#00A1E9"}>
              ที่อยู่
            </Text>
            <Textarea
              disabled
              variant="filled"
              size="sm"
              radius="xl"
              placeholder=""
              styles={{
                input: {
                  backgroundColor: "#EFFBFF",
                },
              }}
              autosize
              minRows={5}
              maxRows={5}
              {...registerForm.getInputProps("address")}
            />
            <Text pt={30} fw={500} size="md" c={"#00A1E9"}>
              เบอร์โทรของผู้สมัคร{" "}
              <span style={{ fontSize: "20px", color: "red" }}>*</span>
            </Text>
            <NumberInput
              size="sm"
              radius="xl"
              placeholder=""
              c={"#00A1E9"}
              withAsterisk
              maxLength={10}
              hideControls
              allowNegative={false}
              allowLeadingZeros={true}
              styles={{
                input: {
                  borderColor: "#00A1E9",
                },
              }}
              {...registerForm.getInputProps("telephone")}
            ></NumberInput>
            {/* <TextInput
              maxLength={10}
              size="sm"
              radius="xl"
              placeholder=""
              styles={{
                input: {
                  borderColor: "#00A1E9",
                },
              }}
              {...registerForm.getInputProps("telephone")}
            /> */}
            <Text pt={20} fw={500} size="md" c={"#00A1E9"}>
              Email
            </Text>
            <TextInput
              size="sm"
              radius="xl"
              placeholder=""
              styles={{
                input: {
                  borderColor: "#00A1E9",
                },
              }}
              {...registerForm.getInputProps("email")}
            />
            <Text pt={20} fw={500} size="md" c={"#00A1E9"}>
              รูปถ่ายพร้อมเอกสารยืนยัน{" "}
              <span style={{ fontSize: "20px", color: "red" }}>*</span>
            </Text>
            <Imgupload
              name=""
              onChange={(file, fileCustom) => {
                setFileImageApprove(file);
                setFileImageApproveCustomFile(fileCustom);
              }}
              preview={
                fileImageApproveCustomFile
                  ? `data:${fileImageApproveCustomFile?.type};base64,${fileImageApproveCustomFile?.data}`
                  : undefined
              }
              required
            />
            <Box>
              <Text pt={25} fz={16} pb={20}>
                ตัวอย่างรูปภาพเอกสารยืนยันการสมัคร
              </Text>
              <Image
                src={"/login/demo_register_pic.png"}
                style={{ width: "60%", marginBottom: "20px" }}
              />
            </Box>
            <Text pt={20} fw={500} size="md" c={"#00A1E9"}>
              รูปบัตรประชาชน{" "}
              <span style={{ fontSize: "20px", color: "red" }}>*</span>
            </Text>
            <Imgupload
              name=""
              onChange={(file, fileCustom) => {
                setFileImageCardIdentification(file);
                setFileImageCardIdentificationCustomFile(fileCustom);
              }}
              required
              preview={
                fileImageCardIdentificationCustomFile
                  ? `data:${fileImageCardIdentificationCustomFile?.type};base64,${fileImageCardIdentificationCustomFile?.data}`
                  : undefined
              }
            />
            <Group justify="start" pt={25}>
              <Text c={"#00A1E9"}>รูปแบบการสมัคร</Text>
            </Group>
            {typeRegister.map((tr) => (
              <Flex
                key={tr.id}
                pt={24}
                justify="flex-start"
                align="center"
                direction="row"
                wrap="wrap"
              >
                <Checkbox
                  onChange={() =>
                    registerForm.setFieldValue("acceptTypId", tr.id.toString())
                  }
                  checked={registerForm.values.acceptTypId === tr.id.toString()}
                  size="md"
                  radius={"xl"}
                  mr="xl"
                  styles={{ input: { cursor: "pointer" } }}
                  aria-hidden
                />
                <Text w={"80%"}>{tr.des}</Text>
              </Flex>
            ))}
            <Group justify="start" pt={25}>
              <Text c={"#00A1E9"}>การจ่ายเงิน</Text>
            </Group>
            {typePayment.map((tp) => (
              <Flex
                key={tp.id}
                pt={24}
                justify="flex-start"
                align="center"
                direction="row"
                wrap="wrap"
              >
                <Checkbox
                  onChange={() =>
                    registerForm.setFieldValue("typePayment", tp.id.toString())
                  }
                  checked={registerForm.values.typePayment === tp.id.toString()}
                  size="md"
                  radius={"xl"}
                  mr="xl"
                  styles={{ input: { cursor: "pointer" } }}
                  aria-hidden
                />
                <Text w={"80%"}>{tp.des}</Text>
              </Flex>
            ))}
            {/* {registerForm.values.typePayment === "2" && (
              <div>
                <Text mt={20}>ชื่อบัญชี พรรคประชาธิปัตย์ทุนประเดิม</Text>
                <Text>ธนาคารกรุงไทย เลขที่ 068-6-03585-2</Text>
                <Image
                  mt={20}
                  src="/qr-code-payment.png"
                  w={{ base: "100%", lg: "25%" }}
                  fit="contain"
                />
                <Text pt={20} fw={500} size="md" c={"#00A1E9"}>
                  แนบรูปภาพหลักฐานการโอน
                </Text>
                <Imgupload
                  name=""
                  required
                  onChange={(file, fileCustom) => {
                    setFileImagePayment(file);
                    setFileImagePaymentCustomFile(fileCustom);
                  }}
                  preview={
                    fileImagePaymentCustomFile
                      ? `data:${fileImagePaymentCustomFile?.type};base64,${fileImagePaymentCustomFile?.data}`
                      : undefined
                  }
                />
              </div>
            )} */}
            <Grid mt={25}>
              <GridCol span={1}>
                <Checkbox
                  onClick={() => setCheck1(!check1)}
                  checked={check1}
                  onChange={() => {}}
                  tabIndex={-1}
                  size="md"
                  radius={"xl"}
                  mr="xl"
                  styles={{ input: { cursor: "pointer" } }}
                  aria-hidden
                />
              </GridCol>
              <GridCol span={11}>
                <Text>
                  การสมัครสมาชิกพรรคในครั้งนี้
                  ข้าพเจ้ากระทำโดยความสมัครใจของข้าพเจ้าเองและเงิน
                  ค่าบำรุงพรรคเป็นของข้าพเจ้า
                  รวมทั้งข้าพเจ้าเป็นผู้มีคุณสมบัติและไม่มีลักษณะต้องห้ามตามมาตรา
                  24 แห่ง พรป.ว่าด้วยพรรคการเมือง พ.ศ. 2560
                  และหากพรรคตรวจสอบแล้วพบว่า ข้อมูล ดังกล่าวไม่เป็นความจริง
                  พรรคอาจปฏิเสธการสมัครเป็นสมาชิกพรรค ของข้าพเจ้าได้
                </Text>
              </GridCol>
            </Grid>
            <Grid mt={15}>
              <GridCol span={1}>
                <Checkbox
                  onClick={() => setCheck2(!check2)}
                  radius={"xl"}
                  checked={check2}
                  onChange={() => {}}
                  tabIndex={-1}
                  size="md"
                  mr="xl"
                  styles={{ input: { cursor: "pointer" } }}
                  aria-hidden
                />
              </GridCol>
              <GridCol span={11}>
                <Text>
                  ข้าพเจ้าตกลงยินยอมให้พรรคประชาธิปัตย์สามารถเก็บรวบรวม ใช้
                  และเปิดเผยข้อมูลส่วนบุคคลสำหรับ
                  ข้อมูลภาพถ่ายบัตรประจำตัวประชาชนของข้าพเจ้าที่ทำการถ่ายภาพและข้อมูลต่าง
                  ๆ ในภาพดังกล่าว ได้แก่ เลขบัตรประจำตัวประชาชน ชื่อ นามสกุล
                  วันเดือนปีเกิด ที่อยู่ วันที่ออกบัตร วันบัตรหมดอายุ
                  และรูปถ่ายของข้าพเจ้าในบัตรประจำตัวประชาชน
                  และรูปถ่ายใบหน้าของข้าพเจ้า ทั้งนี้เพื่อเป็นการตรวจสอบ
                  และยืนยันตัวตนในการสมัครและเป็นหลักฐานในการสมัครสมาชิกพรรค
                </Text>
              </GridCol>
            </Grid>
            <Flex
              pt={25}
              mih={50}
              gap="md"
              justify="center"
              align="center"
              direction="row"
              wrap="wrap"
            >
              <Button
                onClick={handleSubmit}
                variant="filled"
                radius="xl"
                h={"50px"}
                fw={300}
                // bg={"#01A1E8"}
                bg={!check1 || !check2 ? "#d4dfeb" : "#01A1E8"}
                // disabled={false}
                disabled={!check1 || !check2}
                fullWidth
              >
                สมัครสมาชิก
              </Button>
            </Flex>
          </div>
        ) : (
          <Paper>
            <Flex justify="center">
              <Loader color="blue" />
              {/* <Text fz={20}>ไม่พบข้อมูล</Text> */}
            </Flex>
          </Paper>
        )}
      </Paper>
    </AdminLayout>
  );
}
