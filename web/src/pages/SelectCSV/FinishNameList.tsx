import { Button, Flex, Paper, Text } from "@mantine/core";
import { GoChevronLeft } from "react-icons/go";
// import { DataTable } from "mantine-datatable";
import { useNavigate } from "react-router-dom";
import UserFinish from "../../components/UserFinish";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import { useEffect, useState } from "react";
// import { useAppSelector } from "../../store/hook";
import { registerLog } from "../../interfaces/userDataType";
import AdminLayout from "../../components/Layout/AdminLayout";

export default function FinishNameList() {
  const navigate = useNavigate();
  // const userId = useAppSelector((state) => state.user.value);
  // console.log("Agent", userId);
  const [user, setUserFinish] = useState<registerLog[]>([]);

  async function getUserFinish() {
    fetchWithToken<{ data: registerLog[] }>(
      `/api/registerByAgent/register-finish `
    )
      .then((res) => {
        setUserFinish(res.data);
      })
      .catch(() => {});
  }

  useEffect(() => {
    getUserFinish();
    window.scrollTo(0, 0);
  }, []);

  return (
    <AdminLayout>
      <Paper p={{ base: 10, md: 40 }}>
        <Flex pt={30} align="center" justify="start">
          <Button
            radius="lg"
            onClick={() => {
              navigate("/selectcsv");
            }}
          >
            <GoChevronLeft size="md"></GoChevronLeft>
          </Button>
          <Text mx={10} fw={500} size="md" c={"#00A1E9"}>
            รายชื่อผู้สมัครสำเร็จ
            {/* {JSON.stringify(user)} */}
          </Text>
        </Flex>
        {user.length > 0 &&
          user.map((u, index) => (
            <UserFinish dataUser={u} index={index} key={index}></UserFinish>
          ))}
      </Paper>
    </AdminLayout>
  );
}
