import { Button, Divider, Flex, Paper, Table, Text } from "@mantine/core";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import UploadCSV from "../../components/UploadCSV";
import { UserData } from "../../interfaces/userDataType";
// import { useAppSelector } from "../../store/hook";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import AdminLayout from "../../components/Layout/AdminLayout";
import dayjsCustom from "../../utils/dayjsCustom";

export default function index() {
  // const userId = useAppSelector((state) => state.user.value);
  const navigate = useNavigate();
  const [dt, setdt] = useState<UserData[]>([]);
  const [setId, setIdCard] = useState<UserData[]>([]);

  async function ReceiveDataUser(userData: UserData[]) {
    let temp = await GetUserInDB();
    setdt(userData.filter((f) => !temp.includes(f.idCard)));
  }

  async function GetUserInDB() {
    let temp: any[] = [];
    let tempID = JSON.parse(localStorage.getItem("userDataExcel") || "[]");
    temp = tempID.map((m: { idCard: any }) => m.idCard);
    const dataSent = { idCard: temp };
    await fetchWithToken<{ data: any[] }>(
      `/api/registerByAgent/list-register `,
      "POST",
      { body: JSON.stringify(dataSent) }
    )
      .then((res) => {
        tempID = tempID.filter(
          (f: { idCard: any; birthDate: string }) =>
            !res.data.includes(f.idCard) &&
            dayjsCustom.DateCheckAge(new Date(f.birthDate)) >= 18
        );
        setIdCard(tempID);
      })
      .catch(() => {});
    return tempID;
  }
  function handleClickSelectDataCSV(id: number) {
    navigate(`/selectdataformcsv/${Number(id)}`);
  }
  useEffect(() => {}, [dt]);

  const rows = setId.map((element) => (
    <Table.Tr
      onClick={() => handleClickSelectDataCSV(element.id)}
      key={element.id}
    >
      <Table.Td style={{ fontSize: "16px" }}>
        {element.prefixTh + "" + element.nameTh + " " + element.lastNameTh}
      </Table.Td>
    </Table.Tr>
  ));

  async function checkLocalUser() {
    let tempUserInDB = await GetUserInDB();
    let temp = JSON.parse(localStorage.getItem("userDataExcel") || "[]").filter(
      (f: { idCard: any }) => !tempUserInDB.includes(f.idCard)
    );
    temp.forEach((e: any) => {
      e.registDate = new Date(e.registDate);
    });
    setdt(temp);
  }

  function handlerClickOpenGuide() {
    window.open(
      `https://drive.google.com/file/d/1RVKYfZAA2osp-3kIykgwZYjul8H_WeNu/view`
    );
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    checkLocalUser();
  }, []);

  return (
    <AdminLayout>
      <Paper p={{ base: 10, md: 40 }}>
        <Flex align="center" justify="center">
          <Text c={"#00A1E9"} fw="bold" fz={20}>
            ต่ออายุ
          </Text>
        </Flex>
        <Button
          onClick={() => {
            navigate("/renew-agent");
          }}
          mt={15}
          mb={30}
          fullWidth
          size="md"
          radius="xl"
          variant="filled"
        >
          ต่ออายุสมาชิกพรรค
        </Button>
        <Divider my="sm" />
        <Flex align="center" justify="center">
          <Text c={"#00A1E9"} fz={20} fw="bold">
            สมัครสมาชิกพรรค
          </Text>
        </Flex>
        <Flex align="center" justify="center">
          <Text
            style={{ cursor: "pointer" }}
            mt={5}
            fw={500}
            size="xl"
            td="underline"
            c={"#0077FF"}
            onClick={() => handlerClickOpenGuide()}
          >
            ขั้นตอนการใช้งาน
          </Text>
        </Flex>
        {/* Components */}
        <UploadCSV callBackUploadCSV={ReceiveDataUser}></UploadCSV>

        <Text mt={15} fw={500} size="md" c={"#FF0000"}>
          *กรณีที่ผู้สมัครเป็นสมาชิกพรรคอยู่แล้ว หรือมีอายุไม่ถึง 18 ปีบริบูรณ์
          จะไม่ปรากฏข้อมูล
        </Text>
        <Text mt={50} fw={500} size="md" c={"#00A1E9"}>
          รายชื่อผู้สมัคร
        </Text>
        <Table mt={15} stickyHeader stickyHeaderOffset={60}>
          <Table.Thead>
            <Table.Tr></Table.Tr>
          </Table.Thead>
          <Table.Tbody>{rows}</Table.Tbody>
        </Table>

        <Button
          onClick={() => {
            navigate("/finishnamelist");
          }}
          mt={15}
          fullWidth
          size="md"
          radius="xl"
          variant="filled"
        >
          ดูรายชื่อผู้สมัครสำเร็จ
        </Button>
      </Paper>
    </AdminLayout>
  );
}
