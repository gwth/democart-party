import { useEffect, useState } from "react";
import { DatePickerInput } from "@mantine/dates";
import { useNavigate } from "react-router-dom";
import {
  Box,
  Button,
  Checkbox,
  Flex,
  Grid,
  GridCol,
  // Group,
  // Highlight,
  Image,
  LoadingOverlay,
  // Modal,
  NumberInput,
  Paper,
  Select,
  Text,
  TextInput,
  Title,
} from "@mantine/core";
import { rem } from "@mantine/core";
import { IconCalendar, IconPlaystationX } from "@tabler/icons-react";
import Imgupload from "../../components/upload/img";
// import { TiInfoLarge } from "react-icons/ti";
import { useDisclosure } from "@mantine/hooks";
import { isNotEmpty, useForm } from "@mantine/form";

import { IRegister } from "../../interfaces/register.interface";
import { IAmphoe, IPrefix, IProvince, ITombon } from "../../interfaces/common.interface";
import { sortThaiDictionary } from "../../utils/sort";
import RegisterLayout from "../../components/Layout/RegisterLayout";
import { FileCustom } from "../../interfaces/file.interface";
import { modals } from "@mantine/modals";
import CopyButton from "../../components/elements/CopyButton/CopyButton";
import DowloadButton from "../../components/elements/DowloadButton/DowloadButton";
import { GoChevronLeft } from "react-icons/go";
import dayjsCustom from "../../utils/dayjsCustom";

export default function Register() {
  const navigate = useNavigate();
  const [check1, setCheck1] = useState<boolean>(false);
  const [check2, setCheck2] = useState<boolean>(false);

  const [fileImageApprove, setFileImageApprove] = useState<File | null>(null);
  const [fileImageCardIdentification, setFileImageCardIdentification] = useState<File | null>(null);
  const [fileImagePayment, setFileImagePayment] = useState<File | null>(null);

  const [fileImageApproveCustomFile, setFileImageApproveCustomFile] = useState<FileCustom | null>(null);
  const [fileImageCardIdentificationCustomFile, setFileImageCardIdentificationCustomFile] = useState<FileCustom | null>(null);
  const [fileImagePaymentCustomFile, setFileImagePaymentCustomFile] = useState<FileCustom | null>(null);

  const typeRegister = [
    { name: "สมัครแบบรายปี", des: "สมัครแบบรายปี 20 บาท/ปี", id: 1 },
    { name: "สมัครแบบตลอดชีพ", des: "สมัครแบบตลอดชีพ 200 บาท", id: 2 },
  ];

  const [provinces, setProvinces] = useState<IProvince[]>([]);
  const [amphoes, setAmphoes] = useState<IAmphoe[]>([]);
  const [tombons, setTombons] = useState<ITombon[]>([]);
  const [prefixs, setPrefixs] = useState<IPrefix[]>([]);
  const [errorV, setErrorV] = useState<any>(null);

  // const [opened, { open, close }] = useDisclosure(false);
  const [openedLoader, { open: openLoader, close: closeLoader }] = useDisclosure(false);
  const icon = <IconCalendar style={{ width: rem(18), height: rem(18) }} stroke={1.5} />;

  const fetchPrefix = () => {
    fetch("/api/common/prefix")
      .then(async (res) => await res.json())
      .then((data) => setPrefixs(data.data));
  };
  const fetchProvince = () => {
    fetch("/api/common/province")
      .then(async (res) => await res.json())
      .then((data) => setProvinces(data.data as IProvince[]));
  };

  const fetchAmphoe = (provinceId: string) => {
    fetch(`/api/common/aumper/${provinceId}`)
      .then(async (res) => await res.json())
      .then((data) => setAmphoes(data.data));
  };

  const fetchTombon = (amphoeId: string) => {
    fetch(`/api/common/tumbon/${amphoeId}`)
      .then(async (res) => await res.json())
      .then((data) => setTombons(data.data));
  };

  const registerForm = useForm<IRegister>({
    transformValues: (values) => ({
      ...values,
      telephone: values.telephone.toString(),
    }),
    validate: {
      prefix: isNotEmpty("กรุณาใส่คำนำหน้า"),
      firstName: isNotEmpty("กรุณาใส่ชื่อ"),
      lastName: isNotEmpty("กรุณาใส่นามสกุล"),
      birthDate: isNotEmpty("กรุณาใส่วันเกิด"),
      province: isNotEmpty("กรุณาใส่จังหวัด"),
      amphoe: isNotEmpty("กรุณาใส่อำเภอ"),
      tombon: isNotEmpty("กรุณาใส่ตำบล"),
      // telephone:  isNotEmpty("กรุณาใส่เบอร์โทรศัพท์"),
      telephone: (value) => {
        return !value || value.toString().length < 9 ? "กรุณาใส่เบอร์โทรศัพท์" : null;
      },
      idCard: isNotEmpty("กรุณาใส่เลขประจําตัวประชาชน"),
      acceptTypId: isNotEmpty("กรุณาเลือกประเภทการสมัคร"),
    },
  });

  useEffect(() => {
    fetchProvince();
    fetchPrefix();
  }, []);

  const handleSubmission = async () => {
    const validate = registerForm.validate();
    if (validate.hasErrors || !fileImageApprove || !fileImageCardIdentification || !fileImagePayment) {
      setErrorV(validate.errors);
      let str = "กรุณากรอกข้อมูลให้ครบ <br/>";
      Object.keys(validate.errors).forEach((key) => {
        const value = validate.errors[key];
        // console.log(`Key: ${key}, Value: ${value}`);
        str += `- ${value}<br/>`;
      });
      if (!fileImageApprove) {
        str += `- รูปถ่ายตนเองพร้อมเอกสารยืนยันการสมัคร<br/>`;
      }
      if (!fileImageCardIdentification) {
        str += `- รูปบัตรประชาชน<br/>`;
      }
      if (!fileImagePayment) {
        str += `- รูปภาพหลักฐานการโอน<br/>`;
      }
      modals.openContextModal({
        modal: "baseModal",
        withCloseButton: false,
        centered: true,

        styles: {
          content: {
            borderRadius: "25px",
          },
        },
        innerProps: {
          modalBody: (
            <>
              <Text ml={20} fz={{ base: 16, md: 20, lg: 20 }} c={"red"} dangerouslySetInnerHTML={{ __html: str }}></Text>
              <Button onClick={() => modals.closeAll()} mt={20} w={"100%"}>
                ตกลง
              </Button>
            </>
          ),
        },
      });
      return;
    }

    const age = dayjsCustom.DateCheckAge(registerForm.values.birthDate.toDateString());
    if (age < 18) {
      modals.openContextModal({
        modal: "baseModal",
        withCloseButton: false,
        centered: true,
        styles: {
          content: {
            borderRadius: "25px",
          },
        },
        innerProps: {
          modalBody: (
            <Flex my={25} mx={15} w={"100%"} justify="center" align="center" direction="column">
              <IconPlaystationX size={90} color="#F93B48" stroke="1" />
              <Title mt={15} size={20} c="gray">
                ดำเนินการไม่สำเร็จ
              </Title>
              <Text
                fz={18}
                mt={10}
                fw="normal"
                ta="center"
                c={"gray"}
                dangerouslySetInnerHTML={{ __html: "ผู้สมัครสมาชิกจะต้องมีอายุไม่ต่ำกว่า 18 ปีบริบูรณ์" }}></Text>
              <Button radius="xl" onClick={() => modals.closeAll()} w={"40%"} mt={20}>
                ตกลง
              </Button>
            </Flex>
          ),
        },
      });
      return;
    }

    if (!fileImageCardIdentification) {
      return;
    }
    if (!fileImagePayment) {
      return;
    }

    const res = await fetch(`/api/register/check-member/${registerForm.getTransformedValues().idCard}`, {
      headers: {
        "x-api-key":
          "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
      },
    });

    const result: { message: string; isMember: boolean } = await res.json();

    if (res.status == 200) {
      if (result.isMember) {
        modals.openContextModal({
          modal: "baseModal",
          withCloseButton: false,
          centered: true,
          styles: {
            content: {
              borderRadius: "25px",
            },
          },
          innerProps: {
            modalBody: (
              <Flex my={25} mx={15} w={"100%"} justify="center" align="center" direction="column">
                <IconPlaystationX size={90} color="#F93B48" stroke="1" />
                <Title mt={15} size={20} c="gray">
                  ดำเนินการไม่สำเร็จ
                </Title>
                <Text fz={18} mt={10} fw="normal" ta="center" c={"gray"} dangerouslySetInnerHTML={{ __html: result.message }}></Text>
                <Button radius="xl" onClick={() => modals.closeAll()} w={"40%"} mt={20}>
                  ตกลง
                </Button>
              </Flex>
            ),
          },
        });

        return;
      }

      if (fileImageApprove && fileImageCardIdentification && fileImagePayment && Number(age) >= 18) {
        const formData = new FormData();
        formData.append("fileImageApprove", fileImageApprove);
        formData.append("fileImageCardIdentification", fileImageCardIdentification);
        formData.append("fileImagePayment", fileImagePayment);
        /*  formData.append("fileImageApprove", JSON.stringify(fileImageApprove));
        formData.append("fileImageCardIdentification", JSON.stringify(fileImageCardIdentification));
        formData.append("fileImagePayment", JSON.stringify(fileImagePayment)); */
        formData.append(
          "registerData",
          JSON.stringify({
            ...registerForm.getTransformedValues(),
            typePayment: "2",
            typeRegister: "1",
            agent: null,
            status: "0",
          })
        );

        openLoader();

        fetch("/api/register/logs", {
          method: "POST",
          headers: {
            "x-api-key":
              "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
          },
          body: formData,
        })
          .then(async (res) => await res.json())
          .then(({ statusCode }) => {
            if (statusCode == 201) {
              modals.openContextModal({
                modal: "baseModal",
                withCloseButton: false,
                centered: true,
                styles: {
                  content: {
                    borderRadius: "25px",
                  },
                },
                innerProps: {
                  modalBody: (
                    <>
                      <Image src={"/login/Group334.png"} style={{ width: "35%", padding: "25px" }} />
                      <Text fz={22} fw="bold" c={"#00A1E9"}>
                        การสมัครสมาชิกสำเร็จ
                      </Text>

                      <Button
                        onClick={() => {
                          modals.closeAll();
                          navigate("/", { replace: true });
                        }}
                        variant="filled"
                        radius="xl"
                        w="60%"
                        mt={20}
                        h={40}
                        fz={18}
                        fw={300}
                        bg={"#01A1E8"}>
                        ปิด
                      </Button>
                    </>
                  ),
                },
              });

              // navigate("/", { replace: true });
            }
          })
          .finally(() => closeLoader());
      }
    } else {
      modals.openContextModal({
        modal: "baseModal",
        withCloseButton: false,
        centered: true,
        styles: {
          content: {
            borderRadius: "25px",
          },
        },
        innerProps: {
          modalBody: (
            <Flex my={25} mx={15} w={"100%"} justify="center" align="center" direction="column">
              <IconPlaystationX size={90} color="#F93B48" stroke="1" />
              <Title mt={15} size={20} c="gray">
                ดำเนินการไม่สำเร็จ
              </Title>
              <Text fz={18} mt={10} fw="normal" ta="center" c={"gray"} dangerouslySetInnerHTML={{ __html: result.message }}></Text>
              <Button radius="xl" onClick={() => modals.closeAll()} w={"40%"} mt={20}>
                ตกลง
              </Button>
            </Flex>
          ),
        },
      });
    }
  };

  return (
    <RegisterLayout>
      <Paper p={{ base: 10, md: 40 }}>
        <LoadingOverlay visible={openedLoader} zIndex={1000} overlayProps={{ radius: "sm", blur: 2 }} />
        <Grid pt={30} pb={{ base: 10, lg: 20 }} align="center" justify="start">
          <Button
            radius="lg"
            onClick={() => {
              navigate("/");
            }}>
            <GoChevronLeft size="md"></GoChevronLeft>
          </Button>
          <Text mx={10} fz={{ base: 16, lg: 20 }} fw="bold" c={"#00A1E9"}>
            สมัครสมาชิกพรรคประชาธิปัตย์
          </Text>
        </Grid>

        <Grid pt={{ base: 10, lg: 20 }}>
          <Grid.Col span={{ base: 12, md: 4 }}>
            <Select
              label="คำนำหน้าชื่อ (ภาษาไทย)"
              display={"block"}
              flex={1}
              checkIconPosition="right"
              data={prefixs.map((p) => ({
                value: p.id.toString(),
                label: p.data,
              }))}
              placeholder="คำนำหน้าชื่อ (ภาษาไทย)"
              c={"#00A1E9"}
              radius="xl"
              withAsterisk
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("prefix")}
            />
          </Grid.Col>
          {registerForm.values.prefix === "4" && (
            <Grid.Col span={{ base: 12, md: 4 }}>
              <TextInput
                label="ระบุ"
                placeholder="ระบุ"
                withAsterisk
                c={"#00A1E9"}
                radius="xl"
                styles={{
                  input: {
                    height: "40px",
                    borderColor: "#00A1E9",
                  },
                  label: {
                    fontSize: "20px",
                    fontWeight: "bold",
                  },
                }}
                {...registerForm.getInputProps("prefixOther")}
              />
            </Grid.Col>
          )}
        </Grid>
        <Grid pt={{ base: 10, lg: 20 }}>
          <Grid.Col span={{ base: 12, md: 4 }}>
            <TextInput
              label="ชื่อ (ภาษาไทย)"
              placeholder="ชื่อ (ภาษาไทย)"
              withAsterisk
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("firstName")}
            />
          </Grid.Col>
          <Grid.Col span={{ base: 12, md: 4 }}>
            <TextInput
              label="นามสกุล (ภาษาไทย)"
              placeholder="นามสกุล (ภาษาไทย)"
              c={"#00A1E9"}
              withAsterisk
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("lastName")}
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="เลขประจําตัวประชาชน"
              placeholder="เลขประจําตัวประชาชน"
              maxLength={13}
              withAsterisk
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("idCard")}
            />
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <DatePickerInput
              label="วันเกิด"
              placeholder="วันเกิด"
              withAsterisk
              valueFormat="DD MMMM BBBB"
              yearsListFormat="BBBB"
              yearLabelFormat="BBBB"
              decadeLabelFormat="BBBB"
              monthLabelFormat="MMMM BBBB"
              display={"block"}
              c={"#00A1E9"}
              radius="xl"
              rightSection={icon}
              rightSectionPointerEvents="none"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("birthDate")}
            />
          </GridCol>
        </Grid>
        <Grid>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="เลขที่บ้าน"
              placeholder="เลขที่บ้าน"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("houseNumber")}
            />
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="อาคาร"
              placeholder="อาคาร"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("building")}
            />
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="หมู่"
              placeholder="หมู่"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("moo")}
            />
          </GridCol>
        </Grid>
        <Grid>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="ซอย"
              placeholder="ซอย"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("soi")}
            />
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="ถนน"
              placeholder="ถนน"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("road")}
            />
          </GridCol>
        </Grid>
        <Grid>
          <GridCol span={{ base: 12, md: 4 }}>
            <Select
              label="จังหวัด"
              withAsterisk
              display={"block"}
              flex={1}
              checkIconPosition="right"
              data={sortThaiDictionary(provinces, "name").map((p) => ({
                value: p.id.toString(),
                label: p.name,
              }))}
              error={errorV && errorV.province}
              placeholder="จังหวัด"
              c={"#00A1E9"}
              allowDeselect={false}
              value={registerForm.values.province}
              onChange={(value) => {
                if (value) {
                  registerForm.setFieldValue("province", value);
                  fetchAmphoe(value);
                }
              }}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
            />
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <Select
              label="เขต/อําเภอ"
              withAsterisk
              display={"block"}
              flex={1}
              checkIconPosition="right"
              error={errorV && errorV.amphoe}
              data={sortThaiDictionary(amphoes, "name").map((a) => ({
                value: a.id.toString(),
                label: a.name,
              }))}
              placeholder="เขต/อําเภอ"
              c={"#00A1E9"}
              radius="xl"
              value={registerForm.values.amphoe}
              onChange={(value) => {
                if (value) {
                  registerForm.setFieldValue("amphoe", value);
                  fetchTombon(value);
                }
              }}
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
            />
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <Select
              label="แขวง/ตำบล"
              withAsterisk
              display={"block"}
              flex={1}
              checkIconPosition="right"
              data={sortThaiDictionary(tombons, "name").map((t) => ({
                value: t.id.toString(),
                label: t.name,
              }))}
              placeholder="แขวง/ตำบล"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("tombon")}
            />
          </GridCol>
        </Grid>
        <Grid>
          <GridCol span={{ base: 12, md: 4 }}>
            <NumberInput
              label="เบอร์โทรศัพท์"
              placeholder="เบอร์โทรศัพท์"
              c={"#00A1E9"}
              withAsterisk
              radius="xl"
              maxLength={10}
              hideControls
              allowNegative={false}
              allowLeadingZeros={true}
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("telephone")}></NumberInput>
            {/* <TextInput
              label="เบอร์โทรศัพท์"
              placeholder="เบอร์โทรศัพท์"
              c={"#00A1E9"}
              withAsterisk
              radius="xl"
              maxLength={10}
              max={10}
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              type="number"
              {...registerForm.getInputProps("telephone")}
            /> */}
          </GridCol>
          <GridCol span={{ base: 12, md: 4 }}>
            <TextInput
              label="อีเมล"
              placeholder="อีเมล"
              c={"#00A1E9"}
              radius="xl"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                  fontWeight: "bold",
                },
              }}
              {...registerForm.getInputProps("email")}
            />
          </GridCol>
        </Grid>
        <Imgupload
          name="รูปถ่ายตนเองพร้อมเอกสารยืนยันการสมัครที่เขียนข้อความ ข้าพเจ้า (ชื่อ - นามสกุลผู้สมัคร) มีความประสงค์สมัครสมาชิกพรรคประชาธิปัตย์"
          onChange={(file, fileCustom) => {
            setFileImageApprove(file);
            setFileImageApproveCustomFile(fileCustom);
          }}
          preview={fileImageApproveCustomFile ? `data:${fileImageApproveCustomFile?.type};base64,${fileImageApproveCustomFile?.data}` : undefined}
          required
        />
        <Box>
          <Text pt={25} fz={20} pb={20} >ตัวอย่างรูปภาพเอกสารยืนยันการสมัคร</Text>
          <Image src={"/login/demo_register_pic.png"} style={{ width: "40%", marginBottom: "20px" }} />
        </Box>

        {/* <Group justify="start" gap={5} pt={25}>
          <Text fz={{ base: 16, lg: 20 }}>เอกสารยืนยันการสมัครให้ผู้สมัครเขียนดังนี้</Text>
          <Button
            onClick={open}
            style={{
              backgroundColor: "#00A1E9",
              borderRadius: "50%",
              padding: "2px",
              height: "25px",
              width: "25px",
            }}>
            <TiInfoLarge size={30} style={{ color: "white" }} />
          </Button>
        </Group> */}
        {/* <Modal
          withCloseButton={false}
          opened={opened}
          centered={true}
          onClose={close}
          closeOnClickOutside={false}
          size="lg"
          w={{ base: "100%" }}
          radius={"lg"}>
          <Flex direction="column" align="center">
            <Text fz={{ base: 18, lg: 30 }}>ตัวอย่างรูปภาพเอกสารยืนยันการสมัคร</Text>
            <Text fz={{ base: 18, lg: 24 }} c={"#00A1E9"}>
              ท่านสามารถดาวน์โหลดแบบฟอร์มได้ที่หน้าแรกของระบบ
            </Text>
            <Image src={"/login/MaskGroup5.png"} style={{ width: "50%", marginBottom: "20px" }} />
            <Button onClick={close} variant="filled" radius="xl" w="100%" fz={18} fw={300} bg={"#01A1E8"}>
              ปิด
            </Button>
          </Flex>
        </Modal> */}
        {/* <Group>
          <Highlight
            highlight="(ชื่อ - นามสกุลผู้สมัคร)"
            highlightStyles={{
              WebkitBackgroundClip: "text",
              WebkitTextFillColor: "#00A1E9",
            }}
            fz={{ base: 16, lg: 20 }}>
            "ข้าพเจ้า (ชื่อ - นามสกุลผู้สมัคร) มีความประสงค์สมัครสมาชิกพรรคประชาธิปัตย์"
          </Highlight>
        </Group> */}
        <Imgupload
          name="รูปบัตรประชาชน"
          onChange={(file, fileCustom) => {
            setFileImageCardIdentification(file);
            setFileImageCardIdentificationCustomFile(fileCustom);
          }}
          required
          preview={
            fileImageCardIdentificationCustomFile
              ? `data:${fileImageCardIdentificationCustomFile?.type};base64,${fileImageCardIdentificationCustomFile?.data}`
              : undefined
          }
        />
        <Grid mt={15}>
          <GridCol>
            <Text fz={20} fw="bold" c={"#00A1E9"}>
              รูปแบบการต่ออายุ
            </Text>
          </GridCol>
          <GridCol span={12}>
            {typeRegister.map((tr) => (
              <Flex key={tr.id} pt={15} justify="flex-start" align="center" direction="row" wrap="wrap">
                <Checkbox
                  onChange={() => {
                    registerForm.setFieldValue("acceptTypId", tr.id.toString());
                  }}
                  checked={Number(registerForm.values.acceptTypId) === tr.id}
                  size="md"
                  radius={"xl"}
                  mr="xl"
                  styles={{
                    input: { cursor: "pointer" },
                    label: { fontSize: 20 },
                  }}
                  aria-hidden
                  label={tr.des}
                />
              </Flex>
            ))}
            {registerForm.errors.acceptTypId && (
              <>
                <Text fz={20} c={"#FF0000"} pt={10}>
                  *{registerForm.errors.acceptTypId}
                </Text>
              </>
            )}
            <Text fz={20} c={"#FF0000"} pt={30}>
              *ท่านต้องชำระเงินผ่านบัญชีของตนเองเท่านั้น
            </Text>
            <Text fz={20} fw="bold" c={"#00A1E9"} pt={10}>
              แสกน QR Code เพื่อจ่ายเงิน
            </Text>
            <Text fz={20}>ชื่อบัญชีพรรคประชาธิปัตย์ทุนประเดิม</Text>
            <Text fz={20}>ธนาคารกรุงไทย เลขที่ 068-6-03585-2</Text>
            <CopyButton />
            <Image src="/qr-code-payment.png" w={{ base: "60%", md: "25%" }} fit="contain" />
            <DowloadButton />
            <Imgupload
              name="รูปภาพหลักฐานการโอน"
              required
              onChange={(file, fileCustom) => {
                setFileImagePayment(file);
                setFileImagePaymentCustomFile(fileCustom);
              }}
              preview={fileImagePaymentCustomFile ? `data:${fileImagePaymentCustomFile?.type};base64,${fileImagePaymentCustomFile?.data}` : undefined}
            />
          </GridCol>
        </Grid>
        <Grid mt={15}>
          <GridCol span={1}>
            <Checkbox
              onClick={() => setCheck1(!check1)}
              checked={check1}
              onChange={() => { }}
              tabIndex={-1}
              size="md"
              radius={"xl"}
              mr="xl"
              styles={{ input: { cursor: "pointer" } }}
              aria-hidden
            />
          </GridCol>
          <GridCol span={11}>
            <Text>
              การสมัครสมาชิกพรรคในครั้งนี้ ข้าพเจ้ากระทำโดยความสมัครใจของข้าพเจ้าเองและเงิน ค่าบำรุงพรรคเป็นของข้าพเจ้า
              รวมทั้งข้าพเจ้าเป็นผู้มีคุณสมบัติและไม่มีลักษณะต้องห้ามตามมาตรา 24 แห่ง พรป.ว่าด้วยพรรคการเมือง พ.ศ. 2560 และหากพรรคตรวจสอบแล้วพบว่า
              ข้อมูล ดังกล่าวไม่เป็นความจริง พรรคอาจปฏิเสธการสมัครเป็นสมาชิกพรรค ของข้าพเจ้าได้
            </Text>
          </GridCol>
        </Grid>
        <Grid mt={15}>
          <GridCol span={1}>
            <Checkbox
              onClick={() => setCheck2(!check2)}
              radius={"xl"}
              checked={check2}
              onChange={() => { }}
              tabIndex={-1}
              size="md"
              mr="xl"
              styles={{ input: { cursor: "pointer" } }}
              aria-hidden
            />
          </GridCol>
          <GridCol span={11}>
            <Text>
              ข้าพเจ้าตกลงยินยอมให้พรรคประชาธิปัตย์สามารถเก็บรวบรวม ใช้ และเปิดเผยข้อมูลส่วนบุคคลสำหรับ
              ข้อมูลภาพถ่ายบัตรประจำตัวประชาชนของข้าพเจ้าที่ทำการถ่ายภาพและข้อมูลต่าง ๆ ในภาพดังกล่าว ได้แก่ เลขบัตรประจำตัวประชาชน ชื่อ นามสกุล
              วันเดือนปีเกิด ที่อยู่ วันที่ออกบัตร วันบัตรหมดอายุ และรูปถ่ายของข้าพเจ้าในบัตรประจำตัวประชาชน และรูปถ่ายใบหน้าของข้าพเจ้า
              ทั้งนี้เพื่อเป็นการตรวจสอบ และยืนยันตัวตนในการสมัครและเป็นหลักฐานในการสมัครสมาชิกพรรค
            </Text>
          </GridCol>
        </Grid>
        <Flex pt={25} gap="md" justify="center" align="center" direction="row" wrap="wrap">
          <Button
            variant="filled"
            radius="xl"
            w={300}
            h={45}
            fz={20}
            fw={300}
            bg={!check1 || !check2 ? "#d4dfeb" : "#01A1E8"}
            disabled={!check1 || !check2}
            // disabled={false}
            onClick={handleSubmission}>
            สมัครสมาชิก
          </Button>
        </Flex>
      </Paper>
    </RegisterLayout>
  );
}
