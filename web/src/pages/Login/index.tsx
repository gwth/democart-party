import { useNavigate } from "react-router-dom";

import { Flex, BackgroundImage, Text, Button, TextInput, Image, Checkbox, Group, Grid, GridCol, PasswordInput } from "@mantine/core";
import { useState } from "react";

import classes from "./Login.module.css";
import { FaRegUser } from "react-icons/fa";
import { FaSyncAlt } from "react-icons/fa";
import { FaRegEdit } from "react-icons/fa";
import { TbReportSearch } from "react-icons/tb";
import { isNotEmpty, useForm } from "@mantine/form";

import { useAppDispatch } from "../../store/hook";
import { login } from "../../store/auth/authSlice";
import FadeIn from "react-fade-in/lib/FadeIn";
import { getHotkeyHandler } from "@mantine/hooks";
export default function Login() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [onClick, setOnClick] = useState(true);

  const loginForm = useForm<{ username: string; password: string; rememberMe: boolean }>({
    initialValues: {
      username: "",
      password: "",
      rememberMe: false,
    },
    validate: {
      username: isNotEmpty("กรุณาใส่ชื่อผู้ใช้งาน"),
      password: isNotEmpty("กรุณาใส่รหัสผ่าน"),
    },
  });

  const handleSubmission = () => {
    const validationErrors = loginForm.validate();
    if (validationErrors.hasErrors) return;

    const userLogin = loginForm.values;

    dispatch(login(userLogin))
      .unwrap()
      .then((res) => {
        if (res.statusCode == 200) {
          navigate("/main");
        } else {
          alert("user invalid!");
        }
      });
  };

  return (
    <>
      <FadeIn>
        <Grid style={{ overflow: "hidden" }} gutter="0">
          <GridCol span={{ base: 12, lg: 4 }} order={{ base: 2, lg: 1 }}>
            <BackgroundImage
              style={{
                minHeight: "100vh",
                backgroundPosition: "contain",
                backgroundSize: "cover",
                backgroundRepeat: "no-repeat",
              }}
              src={"/login/Group213.png"}
              p={30}>
              {onClick === true ? (
                <>
                  <Text fz={{ base: 20, lg: 30 }} mt={{ base: 0, lg: 70 }} mb={50} ta={"center"} c={"#00A1E9"}>
                    ยินดีต้อนรับ
                  </Text>
                  <Group gap={20}>
                    <Button
                      w={"100%"}
                      h={{ base: 50, lg: 60 }}
                      style={{ borderRadius: "60px" }}
                      bg={"#00A1E9"}
                      variant="filled"
                      onClick={() => {
                        localStorage.removeItem("registerData");
                        navigate("/register");
                      }}>
                      <FaRegEdit size={30} />
                      <Text fz={{ base: 14, xs: 14, sm: 14, md: 17, xl: 20, lg: 20 }} ml={{ base: 5, lg: 20 }} lineClamp={10}>
                        {" "}
                        สมัครสมาชิกพรรค
                      </Text>
                    </Button>
                    <Button
                      w={"100%"}
                      h={{ base: 50, lg: 60 }}
                      style={{ borderRadius: "60px" }}
                      bg={"#00A1E9"}
                      variant="filled"
                      onClick={() => navigate("/renew-party")}>
                      <FaSyncAlt size={25} />
                      <Text fz={{ base: 14, xs: 14, sm: 14, md: 17, xl: 20, lg: 20 }} ml={{ base: 10, lg: 20 }} lineClamp={10}>
                        {" "}
                        ต่ออายุสมาชิกพรรค
                      </Text>
                    </Button>
                    <Button
                      w={{ base: "100%", lg: "100%" }}
                      h={{ base: 50, lg: 60 }}
                      style={{ borderRadius: "60px" }}
                      bg={"#00A1E9"}
                      variant="filled"
                      onClick={() => navigate("/check-status")}>
                      <TbReportSearch size={30} />
                      <Text fz={{ base: 14, xs: 14, sm: 14, md: 17, xl: 20, lg: 20 }} ml={{ base: 5, lg: 20 }} lineClamp={10}>
                        {" "}
                        ตรวจสอบสถานะการสมัครสมาชิกพรรค
                      </Text>
                    </Button>
                    <Button
                      w={"100%"}
                      h={{ base: 50, lg: 60 }}
                      style={{ borderRadius: "60px" }}
                      bg={"#00A1E9"}
                      variant="filled"
                      onClick={() => navigate("/check-user-data")}>
                      <FaRegUser size={25} />
                      <Text fz={{ base: 14, xs: 14, sm: 14, md: 17, xl: 20, lg: 20 }} ml={{ base: 5, lg: 20 }} lineClamp={10}>
                        {" "}
                        ข้อมูลสมาชิกพรรค
                      </Text>
                    </Button>
                  </Group>

                  <Flex mih={50} my={30} gap={18} justify="center" align="center" direction="column" wrap="wrap">
                    <Text
                      fz={{ base: 16, lg: 20 }}
                      c={"#00A1E9"}
                      ta={"center"}
                      style={{ cursor: "pointer" }}
                      td="underline"
                      onClick={() => window.open("https://drive.google.com/file/d/1f-ERoBAIswZbd7gdVvF_LIjsFDjpatmL/view")}
                      px={40}>
                      คุณสมบัติและลักษณะต้องห้ามของผู้สมัครสมาชิกพรรค{" "}
                    </Text>
                    <Text
                      fz={{ base: 16, lg: 20 }}
                      c={"#00A1E9"}
                      ta={"center"}
                      style={{ cursor: "pointer" }}
                      td="underline"
                      onClick={() => window.open("https://drive.google.com/file/d/1UAaGBJt1N4C2O7PcT-P3nvaIVQF3wXSf/view?usp=sharing")}>
                      เอกสารยืนยันการสมัครสมาชิก{" "}
                    </Text>
                    <Text
                      fz={{ base: 16, lg: 20 }}
                      c={"#00A1E9"}
                      ta={"center"}
                      style={{ cursor: "pointer" }}
                      td="underline"
                      onClick={() => window.open("https://drive.google.com/file/d/1ZBv7fACdZaAS8VKCOkjPkaY5mGk_1cdO/view")}>
                      เอกสารยืนยันการต่ออายุสมาชิก
                    </Text>
                  </Flex>

                  <Flex justify="center">
                    <Text
                      fz={{ base: 20 }}
                      component="a"
                      c={"#00A1E9"}
                      td="underline"
                      ta={"center"}
                      onClick={() => setOnClick(false)}
                      style={{ cursor: "pointer" }}>
                      สำหรับเจ้าหน้าที่
                    </Text>
                  </Flex>
                </>
              ) : (
                <>
                  <Text fz={{ base: 20, lg: 30 }} mt={{ base: 0, lg: 70 }} mb={50} ta={"center"} c={"#00A1E9"}>
                    ยินดีต้อนรับเข้าสู่ระบบ
                  </Text>

                  <Flex mih={50} my={50} justify="center" align="flex-start" direction="column" wrap="wrap">
                    <TextInput
                      w={"100%"}
                      fz={{ base: 20 }}
                      label="ขื่อผู้ใช้งาน"
                      placeholder="ชื่อผู้ใช้งาน"
                      required
                      radius="xl"
                      styles={{
                        input: {
                          height: "40px",
                          borderColor: "#00A1E9",
                        },
                        label: {
                          fontSize: "20px",
                          fontWeight: "bold",
                        },
                      }}
                      c={"#00A1E9"}
                      onKeyDown={getHotkeyHandler([["Enter", handleSubmission]])}
                      {...loginForm.getInputProps("username")}
                    />

                    <PasswordInput
                      w={"100%"}
                      fz={{ base: 20, lg: 30 }}
                      label="รหัสผ่าน"
                      placeholder="รหัสผ่าน"
                      radius="xl"
                      required
                      styles={{
                        input: {
                          height: "40px",
                          borderColor: "#00A1E9",
                        },
                        label: {
                          fontSize: "20px",
                          fontWeight: "bold",
                        },
                      }}
                      c={"#00A1E9"}
                      onKeyDown={getHotkeyHandler([["Enter", handleSubmission]])}
                      {...loginForm.getInputProps("password")}
                    />
                    <Group my={20}>
                      <Checkbox radius={"xl"} size="md" c={"#00A1E9"} {...loginForm.getInputProps("rememberMe", { type: "checkbox" })} />
                      <Text>จดจำไว้ในระบบ</Text>
                    </Group>

                    <Button variant="filled" radius="xl" w={"100%"} bg={"#01A1E8"} h={{ base: "45px" }} fz={{ base: 20 }} onClick={handleSubmission}>
                      เข้าสู่ระบบ
                    </Button>
                  </Flex>
                  <Flex justify="center">
                    <Text
                      fz={{ base: 20 }}
                      component="a"
                      c={"#00A1E9"}
                      td="underline"
                      ta={"center"}
                      onClick={() => setOnClick(true)}
                      style={{ cursor: "pointer" }}>
                      สำหรับผู้ใช้งานทั่วไป
                    </Text>
                  </Flex>
                </>
              )}
            </BackgroundImage>
          </GridCol>
          <GridCol span={{ base: 12, lg: 8 }} order={{ base: 1, lg: 2 }}>
            <BackgroundImage className={classes.demo} src={"/login/group211.svg"}>
              <Flex h={"100%"} gap="md" justify="center" align="center" direction="column" wrap="wrap">
                <Image pt={20} src={"/login/Group162.svg"} style={{ width: "21%" }} />
                <Text fz={{ base: 20, lg: 40 }} c={"#FFFF"} mb={{ base: 50, lg: 0 }}>
                  พรรคประชาธิปัตย์
                </Text>
              </Flex>
            </BackgroundImage>
          </GridCol>
        </Grid>
      </FadeIn>
    </>
  );
}
