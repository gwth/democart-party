import { useEffect, useState } from "react";
import {
  Box,
  Button,
  Checkbox,
  Divider,
  Flex,
  Grid,
  GridCol,
  Image,
  LoadingOverlay,
  NumberInput,
  Paper,
  Select,
  Text,
  TextInput,
} from "@mantine/core";
import { isNotEmpty, useForm } from "@mantine/form";
import { rem } from "@mantine/core";
import { DatePickerInput } from "@mantine/dates";
import { IconCalendar } from "@tabler/icons-react";
import Imgupload from "../../components/upload/img";
import { fetchWithToken } from "../../hooks/fetchWithToken";
import dayjs from "dayjs";
import { IRegister } from "../../interfaces/register.interface";
import {
  IAmphoe,
  IPrefix,
  IProvince,
  ITombon,
} from "../../interfaces/common.interface";
import { sortThaiDictionary } from "../../utils/sort";
import { FileCustom } from "../../interfaces/file.interface";
import { useNavigate } from "react-router-dom";

import RegisterLayout from "../../components/Layout/RegisterLayout";
import { modals } from "@mantine/modals";
import { useDisclosure } from "@mantine/hooks";
import CopyButton from "../../components/elements/CopyButton/CopyButton";
import DowloadButton from "../../components/elements/DowloadButton/DowloadButton";
import { GoChevronLeft } from "react-icons/go";
// import ModalExampleRenew from "../../components/upload/modalExampleRenew";

export default function RenewParty() {
  const navigate = useNavigate();
  const [check1, setCheck1] = useState<boolean>(false);
  const [check2, setCheck2] = useState<boolean>(false);
  const [value] = useState<Date | null>(null);
  const [statusRenew, setStatusRenew] = useState<number>(0);
  const [checkRenew, setCheckRenew] = useState<number>(6);
  const [checkPhotoId, setCheckPhotoId] = useState<boolean>(true);
  const [status, setstatus] = useState<any>([]);
  const [yearRenewq, setYearRenew] = useState<number>(0);
  const [dataRenew, setDataRenew] = useState<any>([]);
  const [provinces, setProvinces] = useState<IProvince[]>([]);
  const [amphoes, setAmphoes] = useState<IAmphoe[]>([]);
  const [tombons, setTombons] = useState<ITombon[]>([]);
  const [prefixs, setPrefixs] = useState<IPrefix[]>([]);
  const [checkRegister, setCheckRegister] = useState<{
    message: string;
    isRegister: boolean;
  }>({ message: "", isRegister: false });
  const [errorV, setErrorV] = useState<any>(null);
  const [openedLoader, { open: openLoader, close: closeLoader }] =
    useDisclosure(false);
  const [typeRegisterSelected, setTypeRegisterSelected] = useState<number>(0);
  const [fileImageApprove, setFileImageApprove] = useState<File | null>(null);
  const [fileImageApproveFileCustom, setFileImageApproveFileCustom] =
    useState<FileCustom | null>(null);
  const [fileImageCardId, setFileImageCardId] = useState<File | null>(null);
  const [fileImageCardIdFileCustom, setFileImageCardIdFileCustom] =
    useState<FileCustom | null>(null);
  const [fileImagePayment, setFileImagePayment] = useState<File | null>(null);
  const [fileImagePaymentFileCustom, setFileImagePaymentFileCustom] =
    useState<FileCustom | null>(null);
  const typeRegister = [
    { name: "สมัครแบบรายปี", des: "สมัครแบบรายปี 20 บาท/ปี", id: 1 },
    { name: "สมัครแบบตลอดชีพ", des: "สมัครแบบตลอดชีพ 200 บาท", id: 2 },
  ];
  console.log("checkPhotoId", checkPhotoId);
  const icon = (
    <IconCalendar style={{ width: rem(18), height: rem(18) }} stroke={1.5} />
  );
  const renewForm = useForm({
    initialValues: {
      idCard: "",
      birthDate: value,
    },
    validate: {
      idCard: isNotEmpty("กรุณากรอกเลขประจําตัวประชาชน"),
      birthDate: isNotEmpty("กรุณาเลือกวันเกิด"),
    },
  });
  const fetchPrefix = () => {
    fetch("/api/common/prefix")
      .then(async (res) => await res.json())
      .then((data) => setPrefixs(data.data));
  };
  const fetchProvince = () => {
    fetch("/api/common/province")
      .then(async (res) => await res.json())
      .then((data) => setProvinces(data.data as IProvince[]));
  };

  const fetchAmphoe = (provinceId: string) => {
    fetch(`/api/common/aumper/${provinceId}`)
      .then(async (res) => await res.json())
      .then((data) => setAmphoes(data.data));
  };

  const fetchTombon = (amphoeId: string) => {
    fetch(`/api/common/tumbon/${amphoeId}`)
      .then(async (res) => await res.json())
      .then((data) => setTombons(data.data));
  };

  const registerForm = useForm<IRegister>({
    transformValues: (values) => ({
      ...values,
      telephone: values.telephone,
    }),
    validate: {
      prefix: isNotEmpty("กรุณาใส่คำนำหน้า"),
      firstName: isNotEmpty("กรุณาใส่ชื่อ"),
      lastName: isNotEmpty("กรุณาใส่นามสกุล"),
      houseNumber: isNotEmpty("กรุณาใส่บ้านเลขที่"),
      birthDate: isNotEmpty("กรุณาใส่วันเกิด"),
      province: isNotEmpty("กรุณาใส่จังหวัด"),
      amphoe: isNotEmpty("กรุณาใส่อำเภอ"),
      tombon: isNotEmpty("กรุณาใส่ตำบล"),
      telephone: (value) => {
        return !value || value.toString().length < 9
          ? "กรุณาใส่เบอร์โทรศัพท์"
          : null;
      },
      idCard: isNotEmpty("กรุณาใส่เลขประจําตัวประชาชน"),
    },
  });

  const year = yearRenewq;
  const newyear = dayjs().get("year") + 543;
  const yearRenew = newyear - year;
  useEffect(() => {
    const arryCost = [];

    if (Number(yearRenew) === 1) {
      if (typeRegisterSelected === 1) {
        arryCost.push({
          year: Number(year) + 1,
          cost: 20,
        });
      } else if (typeRegisterSelected === 2) {
        arryCost.push({
          year: Number(year) + 1,
          cost: 200,
        });
      }
    } else {
      arryCost.push({
        year: Number(year) + 1,
        cost: 20,
      });
      if (Number(yearRenew) == 2 && typeRegisterSelected === 1) {
        arryCost.push({
          year: Number(year) + 2,
          cost: 20,
        });
      } else if (Number(yearRenew) == 2 && typeRegisterSelected === 2) {
        arryCost.push({
          year: Number(year) + 2,
          cost: 200,
        });
      }
    }

    registerForm.setFieldValue("renewTag", JSON.stringify(arryCost));
    registerForm.setFieldValue(
      "acceptTypId",
      JSON.stringify(typeRegisterSelected)
    );
  }, [dataRenew, typeRegisterSelected]);

  const checkT = (value: number) => {
    if (value === 0) {
      setCheckRenew(0);
      // Notify user about membership renewal.
    } else if (value === 1 || value === 2) {
      setCheckRenew(1);
      //สมัครได้
    } else if (value < 0) {
      setCheckRenew(2);
      //เป็นสมาชิก
    } else if (value == 0.5) {
      setCheckRenew(3);
      //สมัครได้
    } else {
      setCheckRenew(4);
    }
  };

  const handleSearch = async () => {
    const validationErrors = renewForm.validate();
    if (validationErrors.hasErrors) return;
    const renewRegister = renewForm.getTransformedValues();
    const dataRenew = await fetchWithToken<{
      data: any;
      message: string;
      statusCode: number;
    }>(
      `/api/checkstatus/get-status-user-register?idCard=${renewRegister.idCard}&birthDate=${renewRegister.birthDate}`,
      "GET"
    );

    const res = await fetch(
      `/api/register/check-logs/${renewRegister.idCard}`,
      {
        headers: {
          "x-api-key":
            "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
        },
      }
    );
    const data = await res.json();
    setCheckRegister(data);

    if (dataRenew.statusCode != 200) {
      setStatusRenew(2);
    } else {
      setStatusRenew(0);
    }

    //check in db party
    const checkRenew = await fetchWithToken<{ data: any; message: string }>(
      `/api/checkstatus/get-check-renew?idCard=${renewRegister.idCard}`
    );
    const year = Number(
      dataRenew?.data?.renewYear
        ? dataRenew?.data?.renewYear
        : dataRenew?.data?.newmemYear
    );
    console.log("year", year);
    const newyear = dayjs().get("year") + 543;
    const yearRenew = newyear - year;

    if (checkRenew?.message == "success") {
      const checkRenewyear = Number(checkRenew.data?.renew_year);
      checkT(newyear - checkRenewyear);
    } else {
      checkT(0.5);
    }
    setYearRenew(year);

    //Check Photo ID
    if (dataRenew.data.dataUser) {
      const CheckPhoto = Number(dataRenew.data.dataUser.mid.substring(0, 2));
      if (CheckPhoto < 61) {
        setCheckPhotoId(true);
      } else {
        setCheckPhotoId(false);
      }
    }
    if (
      dataRenew?.data?.tstatus === "1" &&
      (dataRenew?.data?.dataUser.deprived_date == "ตลอดชีพ" ||
        dataRenew?.data?.dataUser.expire == "ตลอดชีพ")
    ) {
      console.log("ตลอดชีพ");
      const dataRenew = await fetchWithToken<{ data: any }>(
        `/api/checkstatus/get-check-member-detail?idCard=${renewRegister.idCard}`
      );
      setDataRenew(dataRenew?.data);
    } else if (
      dataRenew?.data?.tstatus === "1" &&
      yearRenew <= 2 &&
      yearRenew > 0
    ) {
      const dataRenew = await fetchWithToken<{ data: any }>(
        `/api/checkstatus/get-check-member-detail?idCard=${renewRegister.idCard}`
      );
      setDataRenew(dataRenew?.data);
      const dataPrefix = await fetchWithToken<{ data: any }>(
        `/api/common/prefix-name?prefix_name=${dataRenew?.data.title}`
      );
      const dataprovince = await fetchWithToken<{ data: any }>(
        `/api/common/province-name?province_name=${dataRenew?.data.province}`
      );
      const dataAmphoe = await fetchWithToken<{ data: any }>(
        `/api/common/aumper-name?aumper_name=${dataRenew?.data.district
          .replace("เขต", "")
          .replace("อำเภอ", "")}`
      );
      const dataTombon = await fetchWithToken<{ data: any }>(
        `/api/common/tumbon-name?tumbon_name=${dataRenew?.data.sub_district
          .replace("แขวง", "")
          .replace("ตําบล", "")}`
      );

      fetchAmphoe(dataprovince.data.id.toString());
      fetchTombon(dataAmphoe.data[0].id.toString());
      registerForm.initialize({
        prefix: dataPrefix.data === null ? "4" : dataPrefix.data.id.toString(),
        prefixOther: dataRenew?.data.title.toString(),
        firstName: dataRenew?.data.f_name,
        lastName: dataRenew?.data.l_name,
        firstNameEn: "",
        lastNameEn: "",
        prefixEn: "",
        idCard: dataRenew?.data.idcard,
        houseNumber: dataRenew?.data.add_no,
        building: dataRenew?.data.village,
        moo: dataRenew?.data.moo,
        soi: dataRenew?.data.soi,
        road: dataRenew?.data.road,
        tombon: dataTombon.data[0].id.toString(),
        amphoe: dataAmphoe.data[0].id.toString(),
        province: dataprovince.data.id.toString(),
        telephone: dataRenew?.data.tel,
        birthDate: renewRegister.birthDate
          ? new Date(renewRegister.birthDate)
          : new Date(),
        typePayment: "2",
        acceptTypId: "",
        typeRegister: "3",
        agent: null,
        status: "",
        renewTag: "",
        email: dataRenew?.data.email,
      });
      setStatusRenew(0);
      setTypeRegisterSelected(1);
      setstatus([]);
    }
    //boss add เงื่อนไขตลอดชีพ
    else if (dataRenew?.data?.tstatus === "1") {
      const dataRenewdetail = await fetchWithToken<{ data: any }>(
        `/api/checkstatus/get-check-member-detail?idCard=${renewRegister.idCard}`
      );
      console.log("dataRenewdetail", dataRenewdetail);
      setstatus(dataRenewdetail?.data);
      setCheckRenew(2);
      setDataRenew([]);
    } else {
      setStatusRenew(2);
    }
  };

  const handleSubmit = async () => {
    const validate = registerForm.validate();
    if (
      validate.hasErrors ||
      !fileImageApprove ||
      (!fileImageCardId && checkPhotoId) ||
      !fileImagePayment
    ) {
      setErrorV(validate.errors);
      let str = "กรุณากรอกข้อมูลให้ครบ <br/>";
      Object.keys(validate.errors).forEach((key) => {
        const value = validate.errors[key];
        console.log(`Key: ${key}, Value: ${value}`);
        str += `- ${value}<br/>`;
      });
      if (!fileImageApprove) {
        str += `- รูปถ่ายตนเองพร้อมเอกสารยืนยันการสมัคร<br/>`;
      }
      if (!fileImageCardId && checkPhotoId) {
        str += `- รูปบัตรประชาชน<br/>`;
      }
      if (!fileImagePayment) {
        str += `- รูปภาพหลักฐานการโอน<br/>`;
      }
      modals.openContextModal({
        modal: "baseModal",
        withCloseButton: false,
        centered: true,

        styles: {
          content: {
            borderRadius: "25px",
          },
        },
        innerProps: {
          modalBody: (
            <>
              <Text
                ml={20}
                fz={{ base: 16, md: 20, lg: 20 }}
                c={"red"}
                dangerouslySetInnerHTML={{ __html: str }}
              ></Text>
              <Button onClick={() => modals.closeAll()} mt={20} w={"100%"}>
                ตกลง
              </Button>
            </>
          ),
        },
      });
      return;
    }
    if (!fileImageApprove) {
      return;
    }
    if (!fileImageCardId && checkPhotoId) {
      return;
    }
    if (!fileImagePayment) {
      return;
    }
    const res = await fetch(
      `/api/register/check-member/${
        registerForm.getTransformedValues().idCard
      }`,
      {
        headers: {
          "x-api-key":
            "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
        },
      }
    );

    const result: { message: string; isMember: boolean } = await res.json();
    if (res.status == 200) {
      openLoader();

      if (fileImageApprove && fileImagePayment) {
        const formData = new FormData();
        formData.append("fileImageApprove", fileImageApprove);
        if (fileImageCardId) {
          formData.append("fileImageCardIdentification", fileImageCardId);
        }
        formData.append("fileImagePayment", fileImagePayment);
        formData.append(
          "registerData",
          JSON.stringify({
            ...registerForm.getTransformedValues(),
            typePayment: "2",
            typeRegister: "3",
            agent: null,
            status: "0",
          })
        );
        // console.log('registerForm', registerForm.values)
        fetch("/api/register/logs", {
          method: "POST",
          headers: {
            "x-api-key":
              "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
          },
          body: formData,
        })
          .then(async (res) => await res.json())
          .then(({ statusCode }) => {
            if (statusCode == 201) {
              modals.openContextModal({
                modal: "baseModal",
                withCloseButton: false,
                centered: true,
                styles: {
                  content: {
                    borderRadius: "25px",
                  },
                },
                innerProps: {
                  modalBody: (
                    <>
                      <Image
                        src={"/login/Group334.png"}
                        style={{ width: "35%", padding: "25px" }}
                      />
                      <Text fz={22} fw="bold" c={"#00A1E9"}>
                        การต่ออายุสำเร็จ
                      </Text>

                      <Button
                        onClick={() => {
                          modals.closeAll();
                          navigate("/", { replace: true });
                        }}
                        variant="filled"
                        radius="xl"
                        w="60%"
                        mt={20}
                        h={40}
                        fz={18}
                        fw={300}
                        bg={"#01A1E8"}
                      >
                        ปิด
                      </Button>
                    </>
                  ),
                },
              });
            }
          })
          .finally(() => closeLoader());
      }
    } else {
      modals.openContextModal({
        modal: "baseModal",
        withCloseButton: false,
        centered: true,
        styles: {
          content: {
            borderRadius: "25px",
          },
        },
        innerProps: {
          modalBody: (
            <>
              <Text
                fz={22}
                fw="bold"
                c={"#00A1E9"}
                dangerouslySetInnerHTML={{ __html: result.message }}
              ></Text>
            </>
          ),
        },
      });
    }
  };

  useEffect(() => {
    fetchProvince();
    fetchPrefix();
  }, []);
  console.log("dataRenew", dataRenew.deprived_date);
  console.log("status?.deprived_date", status);
  return (
    <RegisterLayout>
      <Paper
        p={{ base: 10, md: 40 }}
        style={{ minHeight: "calc(100vh - (116px + (16px*2)))" }}
      >
        <LoadingOverlay
          visible={openedLoader}
          zIndex={1000}
          overlayProps={{ radius: "sm", blur: 2 }}
        />
        <Grid pt={30} pb={{ base: 10, lg: 20 }} align="center" justify="start">
          <Button
            radius="lg"
            onClick={() => {
              navigate("/");
            }}
          >
            <GoChevronLeft size="md"></GoChevronLeft>
          </Button>
          <Text mx={10} fz={{ base: 16, lg: 20 }} fw="bold" c={"#00A1E9"}>
            ต่ออายุสมาชิกพรรคประชาธิปัตย์
          </Text>
        </Grid>
        <Grid pt={{ base: 10, lg: 20 }}>
          <GridCol span={{ base: 12, lg: 4 }}>
            <Text fz={{ base: 16, lg: 20 }} fw="bold" c={"#00A1E9"}>
              เลขประจําตัวประชาชน
            </Text>
            <TextInput
              placeholder="เลขประจําตัวประชาชน"
              c={"#00A1E9"}
              radius="xl"
              maxLength={13}
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                },
              }}
              {...renewForm.getInputProps("idCard")}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 4 }}>
            <Text fz={{ base: 16, lg: 20 }} fw="bold" c={"#00A1E9"}>
              วันเกิด
            </Text>
            <DatePickerInput
              display={"block"}
              c={"#00A1E9"}
              radius="xl"
              rightSection={icon}
              valueFormat="DD MMMM BBBB"
              yearsListFormat="BBBB"
              yearLabelFormat="BBBB"
              decadeLabelFormat="BBBB"
              monthLabelFormat="MMMM BBBB"
              placeholder="วัน เดือน ปี"
              rightSectionPointerEvents="none"
              styles={{
                input: {
                  height: "40px",
                  borderColor: "#00A1E9",
                },
                label: {
                  fontSize: "20px",
                },
              }}
              {...renewForm.getInputProps("birthDate")}
            />
          </GridCol>
          <GridCol span={{ base: 12, lg: 4 }} mt={{ base: 10, lg: 30 }}>
            <Button
              variant="filled"
              radius="xl"
              h={"40px"}
              fz={20}
              fw={300}
              bg={"#01A1E8"}
              type="submit"
              w={{ base: "100%", lg: "auto" }}
              onClick={handleSearch}
              onClickCapture={() => {
                handleSearch;
              }}
            >
              ค้นหาข้อมูล
            </Button>
          </GridCol>
        </Grid>
        {statusRenew === 2 || checkRenew == 4 ? (
          <>
            <Box>
              <Divider my="md" />
              <Grid align="center" justify="center" my={{ base: 40, lg: 100 }}>
                <GridCol span={{ base: 3, lg: 1 }}>
                  <Image
                    mah={{ base: 70, lg: 100 }}
                    maw={{ base: 70, lg: 100 }}
                    src={`user_status/reject.svg`}
                  ></Image>
                </GridCol>
                <GridCol span={{ base: 12, lg: 5 }}>
                  <Text
                    fz={{ base: 16, lg: 22 }}
                    ta={{ base: "center", lg: "left" }}
                  >
                    ท่านไม่ได้เป็นสมาชิกพรรคประชาธิปัตย์
                  </Text>
                  <Text
                    fz={{ base: 16, lg: 22 }}
                    ta={{ base: "center", lg: "left" }}
                  >
                    โปรดตรวจสอบเลขประจำตัวประชาชน และ วันเกิดของท่านอีกครั้ง
                  </Text>
                </GridCol>
              </Grid>
              <Divider my="md" />
              <Text fz={20}>{"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}</Text>
            </Box>
          </>
        ) : (
          <>
            {(checkRenew == 1 || checkRenew == 3) && (
              <>
                {checkRegister.isRegister ? (
                  <>
                    <Box mt={{ base: 20, lg: 40 }}>
                      <Divider my="md" />
                      <Grid
                        align="center"
                        justify="center"
                        my={{ base: 40, lg: 100 }}
                      >
                        <GridCol span={{ base: 3, lg: 1 }}>
                          <Image
                            mah={{ base: 70, lg: 100 }}
                            maw={{ base: 70, lg: 100 }}
                            src={`user_status/approve.svg`}
                          ></Image>
                        </GridCol>
                        <GridCol span={{ base: 12, lg: 5 }}>
                          <Text
                            fz={{ base: 16, lg: 22 }}
                            ta={{ base: "center", lg: "left" }}
                          >
                            ท่านต่ออายุสามชิกพรรคประชาธิปัตย์เรียบร้อย
                          </Text>
                          <Text
                            fz={{ base: 16, lg: 22 }}
                            ta={{ base: "center", lg: "left" }}
                          >
                            โปรดรอการตรวจสอบจากทางเจ้าหน้าที่
                          </Text>
                        </GridCol>
                      </Grid>
                      <Divider my="md" />
                      <Text fz={20}>
                        {"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}
                      </Text>
                    </Box>
                  </>
                ) : (
                  <>
                    {status?.deprived_date == "ตลอดชีพ" ||
                    dataRenew.deprived_date == "ตลอดชีพ" ? (
                      <>
                        <Box mt={{ base: 20, lg: 40 }}>
                          <Divider my="md" />
                          <Flex
                            gap="xl"
                            justify="center"
                            align="center"
                            direction="row"
                            my={{ base: 40, lg: 100 }}
                          >
                            <Image
                              mah={{ base: 40, lg: 57 }}
                              maw={{ base: 40, lg: 57 }}
                              src={`user_status/approve.svg`}
                            ></Image>
                            <Text fz={{ base: 16, lg: 22 }}>
                              ท่านเป็นสมาชิกพรรคประชาธิปัตย์แบบตลอดชีพ
                            </Text>
                          </Flex>
                          <Divider my="md" />
                          <Text fz={20}>
                            {"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}
                          </Text>
                        </Box>
                      </>
                    ) : (
                      <>
                        {/* //have data showdetail */}
                        {dataRenew?.deprived_date ? (
                          <>
                            <Grid pt={{ base: 10, lg: 20 }}>
                              <Grid.Col span={{ base: 12, md: 4 }}>
                                <Select
                                  label="คำนำหน้าชื่อ (ภาษาไทย)"
                                  display={"block"}
                                  flex={1}
                                  checkIconPosition="right"
                                  data={prefixs.map((p) => ({
                                    value: p.id.toString(),
                                    label: p.data,
                                  }))}
                                  placeholder="คำนำหน้าชื่อ (ภาษาไทย)"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  withAsterisk
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("prefix")}
                                />
                              </Grid.Col>
                              {registerForm.values.prefix === "4" && (
                                <Grid.Col span={{ base: 12, md: 4 }}>
                                  <TextInput
                                    label="ระบุ"
                                    placeholder="ระบุ"
                                    withAsterisk
                                    c={"#00A1E9"}
                                    radius="xl"
                                    styles={{
                                      input: {
                                        height: "40px",
                                        borderColor: "#00A1E9",
                                      },
                                      label: {
                                        fontSize: "20px",
                                        fontWeight: "bold",
                                      },
                                    }}
                                    {...registerForm.getInputProps(
                                      "prefixOther"
                                    )}
                                  />
                                </Grid.Col>
                              )}
                            </Grid>
                            <Grid pt={{ base: 10, lg: 20 }}>
                              <Grid.Col span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="ชื่อ (ภาษาไทย)"
                                  placeholder="ชื่อ (ภาษาไทย)"
                                  withAsterisk
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("firstName")}
                                />
                              </Grid.Col>
                              <Grid.Col span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="นามสกุล (ภาษาไทย)"
                                  placeholder="นามสกุล (ภาษาไทย)"
                                  c={"#00A1E9"}
                                  withAsterisk
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("lastName")}
                                />
                              </Grid.Col>
                            </Grid>
                            <Grid>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="เลขที่บ้าน"
                                  placeholder="เลขที่บ้าน"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("houseNumber")}
                                />
                              </GridCol>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="อาคาร"
                                  placeholder="อาคาร"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("building")}
                                />
                              </GridCol>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="หมู่"
                                  placeholder="หมู่"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("moo")}
                                />
                              </GridCol>
                            </Grid>
                            <Grid>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="ซอย"
                                  placeholder="ซอย"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("soi")}
                                />
                              </GridCol>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="ถนน"
                                  placeholder="ถนน"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("road")}
                                />
                              </GridCol>
                            </Grid>

                            <Grid>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <Select
                                  label="จังหวัด"
                                  withAsterisk
                                  display={"block"}
                                  flex={1}
                                  checkIconPosition="right"
                                  data={sortThaiDictionary(
                                    provinces,
                                    "name"
                                  ).map((p) => ({
                                    value: p.id.toString(),
                                    label: p.name,
                                  }))}
                                  error={errorV && errorV.province}
                                  placeholder="จังหวัด"
                                  c={"#00A1E9"}
                                  allowDeselect={false}
                                  value={registerForm.values.province}
                                  onChange={(value) => {
                                    if (value) {
                                      registerForm.setFieldValue(
                                        "province",
                                        value
                                      );
                                      fetchAmphoe(value);
                                    }
                                  }}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                />
                              </GridCol>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <Select
                                  label="เขต/อําเภอ"
                                  withAsterisk
                                  display={"block"}
                                  flex={1}
                                  checkIconPosition="right"
                                  data={sortThaiDictionary(amphoes, "name").map(
                                    (a) => ({
                                      value: a.id.toString(),
                                      label: a.name,
                                    })
                                  )}
                                  placeholder="เขต/อําเภอ"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  error={errorV && errorV.amphoe}
                                  value={registerForm.values.amphoe}
                                  onChange={(value) => {
                                    if (value) {
                                      registerForm.setFieldValue(
                                        "amphoe",
                                        value
                                      );
                                      fetchTombon(value);
                                    }
                                  }}
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                />
                              </GridCol>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <Select
                                  label="แขวง/ตําบล"
                                  withAsterisk
                                  display={"block"}
                                  flex={1}
                                  checkIconPosition="right"
                                  data={sortThaiDictionary(tombons, "name").map(
                                    (t) => ({
                                      value: t.id.toString(),
                                      label: t.name,
                                    })
                                  )}
                                  placeholder="แขวง/ตําบล"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("tombon")}
                                />
                              </GridCol>
                            </Grid>
                            <Grid>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <NumberInput
                                  label="เบอร์โทรศัพท์"
                                  placeholder="เบอร์โทรศัพท์"
                                  c={"#00A1E9"}
                                  withAsterisk
                                  radius="xl"
                                  maxLength={10}
                                  hideControls
                                  allowNegative={false}
                                  allowLeadingZeros={true}
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("telephone")}
                                ></NumberInput>
                              </GridCol>
                              <GridCol span={{ base: 12, md: 4 }}>
                                <TextInput
                                  label="อีเมล"
                                  placeholder="อีเมล"
                                  c={"#00A1E9"}
                                  radius="xl"
                                  styles={{
                                    input: {
                                      height: "40px",
                                      borderColor: "#00A1E9",
                                    },
                                    label: {
                                      fontSize: "20px",
                                      fontWeight: "bold",
                                    },
                                  }}
                                  {...registerForm.getInputProps("email")}
                                />
                              </GridCol>
                            </Grid>
                            <Imgupload
                              name="รูปภาพตนเองพร้อมเอกสารยืนยันการต่ออายุที่เขียนข้อความ ข้าพเจ้า (ชื่อ - นามสกุลผู้สมัคร) มีความประสงค์ต่ออายุสมาชิกพรรคประชาธิปัตย์"
                              onChange={(_file, fileCustom) => {
                                setFileImageApprove(_file),
                                  setFileImageApproveFileCustom(fileCustom);
                              }}
                              preview={
                                fileImageApprove
                                  ? `data:${fileImageApproveFileCustom?.type};base64,${fileImageApproveFileCustom?.data}`
                                  : undefined
                              }
                              required
                            />
                            <Box>
                              <Text pt={25} fz={20} pb={20}>
                                ตัวอย่างรูปภาพเอกสารยืนยันการต่ออายุ
                              </Text>
                              <Image
                                src={"/login/demo_renew_pic.png"}
                                style={{ width: "40%", marginBottom: "20px" }}
                              />
                            </Box>
                            {/* <ModalExampleRenew /> */}
                            {checkPhotoId ? (
                              <>
                                <Imgupload
                                  name="รูปบัตรประชาชน"
                                  onChange={(_file, fileCustom) => {
                                    setFileImageCardId(_file),
                                      setFileImageCardIdFileCustom(fileCustom);
                                  }}
                                  required
                                  preview={
                                    fileImageCardIdFileCustom
                                      ? `data:${fileImageCardIdFileCustom?.type};base64,${fileImageCardIdFileCustom?.data}`
                                      : undefined
                                  }
                                />
                              </>
                            ) : (
                              ""
                            )}

                            <Grid mt={15}>
                              <GridCol>
                                <Text fz={20} fw="bold" c={"#00A1E9"}>
                                  รูปแบบการต่ออายุ
                                </Text>
                              </GridCol>
                              <GridCol>
                                {typeRegister.map((tr) => (
                                  <Flex
                                    key={tr.id}
                                    pt={15}
                                    justify="flex-start"
                                    align="center"
                                    direction="row"
                                    wrap="wrap"
                                  >
                                    <Checkbox
                                      onChange={() => {
                                        setTypeRegisterSelected(tr.id);
                                        console.log("tr.id", tr.id);
                                      }}
                                      checked={typeRegisterSelected === tr.id}
                                      size="md"
                                      radius={"xl"}
                                      mr="xl"
                                      styles={{ input: { cursor: "pointer" } }}
                                      aria-hidden
                                    />
                                    <Text w={"80%"} fz={20}>
                                      {tr.des}
                                    </Text>
                                  </Flex>
                                ))}
                              </GridCol>
                            </Grid>
                            <Text fz={20} c={"#FF0000"} pt={15}>
                              *ท่านต้องชำระเงินผ่านบัญชีของตนเองเท่านั้น
                            </Text>
                            <Text
                              fz={20}
                              pt={{ base: 25, lg: 40 }}
                              c={"#00A1E9"}
                            >
                              สรุปยอดจำนวนเงินที่ต้องชำระ
                            </Text>
                            <Text style={{ wordBreak: "break-word" }} fz={20}>
                              {Number(yearRenew) === 1 && (
                                <>
                                  {typeRegisterSelected === 1
                                    ? "20 บาท"
                                    : "200 บาท"}
                                </>
                              )}
                              {Number(yearRenew) === 2 && (
                                <>
                                  {typeRegisterSelected === 1
                                    ? "40 บาท"
                                    : "220 บาท"}
                                </>
                              )}
                            </Text>
                            <Text c={"#FF0000"} fz={20}>
                              {`หมายเหตุ คำนวณยอดเงินนับจากปีชำระค่าบำรุงครั้งล่าสุด (${dataRenew?.deprived_date})`}
                            </Text>
                            <Text
                              fz={20}
                              fw="bold"
                              c={"#00A1E9"}
                              pt={{ base: 25, lg: 40 }}
                            >
                              แสกน QR Code เพื่อจ่ายเงิน
                            </Text>
                            <Text fz={20}>
                              ชื่อบัญชีพรรคประชาธิปัตย์ทุนประเดิม
                            </Text>
                            <Text fz={20}>
                              ธนาคารกรุงไทย เลขที่ 068-6-03585-2
                            </Text>
                            <CopyButton />
                            <Image
                              src="/qr-code-payment.png"
                              w={{ base: "100%", lg: "25%" }}
                              fit="contain"
                            />
                            <DowloadButton />
                            <Imgupload
                              name="รูปภาพหลักฐานการโอน"
                              required
                              onChange={(_file, fileCustom) => {
                                setFileImagePayment(_file),
                                  setFileImagePaymentFileCustom(fileCustom);
                              }}
                              preview={
                                fileImagePaymentFileCustom
                                  ? `data:${fileImagePaymentFileCustom?.type};base64,${fileImagePaymentFileCustom?.data}`
                                  : undefined
                              }
                            />
                            <Grid mt={50}>
                              <GridCol span={1}>
                                <Checkbox
                                  onClick={() => setCheck1(!check1)}
                                  checked={check1}
                                  onChange={() => {}}
                                  tabIndex={-1}
                                  size="md"
                                  radius={"xl"}
                                  mr="xl"
                                  styles={{ input: { cursor: "pointer" } }}
                                  aria-hidden
                                />
                              </GridCol>
                              <GridCol span={11}>
                                <Text>
                                  การสมัครสมาชิกพรรคในครั้งนี้
                                  ข้าพเจ้ากระทำโดยความสมัครใจของข้าพเจ้าเองและเงิน
                                  ค่าบำรุงพรรคเป็นของข้าพเจ้า
                                  รวมทั้งข้าพเจ้าเป็นผู้มีคุณสมบัติและไม่มีลักษณะต้องห้ามตามมาตรา
                                  24 แห่ง พรป.ว่าด้วยพรรคการเมือง พ.ศ. 2560
                                  และหากพรรคตรวจสอบแล้วพบว่า ข้อมูล
                                  ดังกล่าวไม่เป็นความจริง
                                  พรรคอาจปฏิเสธการสมัครเป็นสมาชิกพรรค
                                  ของข้าพเจ้าได้
                                </Text>
                              </GridCol>
                            </Grid>
                            <Grid mt={15}>
                              <GridCol span={1}>
                                <Checkbox
                                  onClick={() => setCheck2(!check2)}
                                  radius={"xl"}
                                  checked={check2}
                                  onChange={() => {}}
                                  tabIndex={-1}
                                  size="md"
                                  mr="xl"
                                  styles={{ input: { cursor: "pointer" } }}
                                  aria-hidden
                                />
                              </GridCol>
                              <GridCol span={11}>
                                <Text>
                                  ข้าพเจ้าตกลงยินยอมให้พรรคประชาธิปัตย์สามารถเก็บรวบรวม
                                  ใช้ และเปิดเผยข้อมูลส่วนบุคคลสำหรับ
                                  ข้อมูลภาพถ่ายบัตรประจำตัวประชาชนของข้าพเจ้าที่ทำการถ่ายภาพและข้อมูลต่าง
                                  ๆ ในภาพดังกล่าว ได้แก่ เลขบัตรประจำตัวประชาชน
                                  ชื่อ นามสกุล วันเดือนปีเกิด ที่อยู่
                                  วันที่ออกบัตร วันบัตรหมดอายุ
                                  และรูปถ่ายของข้าพเจ้าในบัตรประจำตัวประชาชน
                                  และรูปถ่ายใบหน้าของข้าพเจ้า
                                  ทั้งนี้เพื่อเป็นการตรวจสอบ
                                  และยืนยันตัวตนในการสมัครและเป็นหลักฐานในการสมัครสมาชิกพรรค
                                </Text>
                              </GridCol>
                            </Grid>
                            <Flex
                              pt={25}
                              mih={50}
                              pb={70}
                              gap="md"
                              justify="center"
                              align="center"
                              direction="row"
                              wrap="wrap"
                            >
                              <Button
                                onClick={handleSubmit}
                                variant="filled"
                                radius="xl"
                                w={300}
                                h={45}
                                fz={20}
                                fw={300}
                                bg={!check1 || !check2 ? "#d4dfeb" : "#01A1E8"}
                                disabled={!check1 || !check2}
                              >
                                ต่ออายุ
                              </Button>
                            </Flex>
                          </>
                        ) : (
                          <>
                            {checkRegister.message ==
                              "ท่านยังไม่ได้ทำการสมัคร" && (
                              <>
                                <Box>
                                  <Divider my="md" />
                                  <Grid
                                    align="center"
                                    justify="center"
                                    my={{ base: 40, lg: 100 }}
                                  >
                                    <GridCol span={{ base: 3, lg: 1 }}>
                                      <Image
                                        mah={{ base: 70, lg: 100 }}
                                        maw={{ base: 70, lg: 100 }}
                                        src={`user_status/reject.svg`}
                                      ></Image>
                                    </GridCol>
                                    <GridCol span={{ base: 12, lg: 5 }}>
                                      <Text
                                        fz={{ base: 16, lg: 22 }}
                                        ta={{ base: "center", lg: "left" }}
                                      >
                                        ท่านไม่ได้เป็นสมาชิกพรรคประชาธิปัตย์
                                      </Text>
                                      <Text
                                        fz={{ base: 16, lg: 22 }}
                                        ta={{ base: "center", lg: "left" }}
                                      >
                                        โปรดตรวจสอบเลขประจำตัวประชาชน และ
                                        วันเกิดของท่านอีกครั้ง
                                      </Text>
                                    </GridCol>
                                  </Grid>
                                  <Divider my="md" />
                                  <Text fz={20}>
                                    {"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}
                                  </Text>
                                </Box>
                              </>
                            )}
                          </>
                        )}
                      </>
                    )}
                  </>
                )}
              </>
            )}
            {checkRenew == 0 && (
              <>
                <Box mt={{ base: 20, lg: 40 }}>
                  <Divider my="md" />
                  <Grid
                    align="center"
                    justify="center"
                    my={{ base: 40, lg: 100 }}
                  >
                    <GridCol span={{ base: 3, lg: 1 }}>
                      <Image
                        mah={{ base: 70, lg: 100 }}
                        maw={{ base: 70, lg: 100 }}
                        src={`user_status/approve.svg`}
                      ></Image>
                    </GridCol>
                    <GridCol span={{ base: 12, lg: 5 }}>
                      <Text
                        fz={{ base: 16, lg: 22 }}
                        ta={{ base: "center", lg: "left" }}
                      >
                        โปรดทำการต่ออายุสมาชิกพรรคในปีถัดไป
                      </Text>
                    </GridCol>
                  </Grid>
                  <Divider my="md" />
                  <Text fz={20}>{"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}</Text>
                </Box>
              </>
            )}
            {checkRenew == 2 && (
              <>
                <Box mt={{ base: 20, lg: 40 }}>
                  <Divider my="md" />
                  <Grid
                    align="center"
                    justify="center"
                    my={{ base: 40, lg: 100 }}
                  >
                    <GridCol span={{ base: 3, lg: 1 }}>
                      <Image
                        mah={{ base: 70, lg: 100 }}
                        maw={{ base: 70, lg: 100 }}
                        src={`user_status/approve.svg`}
                      ></Image>
                    </GridCol>
                    <GridCol span={{ base: 12, lg: 5 }}>
                      <Text
                        fz={{ base: 16, lg: 22 }}
                        ta={{ base: "center", lg: "left" }}
                      >
                        โปรดทำการต่ออายุสมาชิกพรรคในปีถัดไป
                      </Text>
                    </GridCol>
                  </Grid>
                  <Divider my="md" />
                  <Text fz={20}>{"ติดต่อสอบถามเพิ่มเติม : 02-828-1000"}</Text>
                </Box>
              </>
            )}
          </>
        )}
      </Paper>
    </RegisterLayout>
  );
}
