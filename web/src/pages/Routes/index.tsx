import { Navigate } from "react-router-dom";
import { useAppSelector } from "../../store/hook";

import { Container, Flex } from "@mantine/core";
import PleaseWaitLoader from "../../components/elements/Loader/LoaderText/PleaseWaitLoader";

export default function RoutePage() {
  const user = useAppSelector((state) => state.user);

  if (!user.value)
    return (
      <Container>
        <Flex justify="center" align="center" h={"100vh"} gap={10} direction="column">
          <PleaseWaitLoader />
        </Flex>
      </Container>
    );

  if (user.value?.role === 1) {
    return <Navigate to="/home" />;
  }

  if (user.value?.role === 2) {
    return <Navigate to="/selectcsv" />;
  }

  return <Navigate to="/forbidden" />;
}
