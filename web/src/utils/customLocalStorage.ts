export type LocalStorageItem<T> = {
  data: T;
  expireTime?: number; // Expiration time in milliseconds
};

interface CustomLocalStorage {
  getItem<T>(key: string): T | null;
  setItem<T>(key: string, value: T, maxAge?: number): void;
  removeItem(key: string): void;
  clear(): void;
}

export const customLocalStorage: CustomLocalStorage = {
  getItem<T>(key: string): T | null {
    // getting the data from localStorage using the key
    const result: LocalStorageItem<T> = JSON.parse(localStorage.getItem(key) || "null") || null;

    if (result) {
      /*
          if data expireTime is less than the current time
          means the item has expired,
          in this case removing the item using the key
          and return null.
      */
      if (result.expireTime && result.expireTime <= Date.now()) {
        localStorage.removeItem(key);
        return null;
      }
      // else return the data.
      return result.data;
    }
    // if there is no data provided for the key, return null.
    return null;
  },

  /*
      accepting the key, value, and expiry time as a parameter
      default expiry time is 30 days in milliseconds.
  */
  setItem<T>(key: string, value: T, maxAge: number = 30 * 24 * 60 * 60 * 1000): void {
    // Storing the value in the object.
    const result: LocalStorageItem<T> = {
      data: value,
    };

    if (maxAge) {
      /*
          setting the expireTime currentTime + expiry Time 
          provided when the method was called.
      */
      result.expireTime = Date.now() + maxAge;
    }
    localStorage.setItem(key, JSON.stringify(result));
  },

  removeItem(key: string): void {
    localStorage.removeItem(key);
  },

  clear(): void {
    localStorage.clear();
  },
};
