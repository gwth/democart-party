export const sortThaiDictionary = <T>(list: T[], key: keyof T): T[] => {
  const newList: T[] = [...list];
  newList.sort((a, b) => (a[key] as string).localeCompare(b[key] as string, "th"));
  return newList;
};
