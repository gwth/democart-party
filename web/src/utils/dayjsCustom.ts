import dayjs from "dayjs";
import "dayjs/locale/th";
import buddhistEra from "dayjs/plugin/buddhistEra";

dayjs.extend(buddhistEra);

/* 07 กุมภาพันธ์ 2566 */
const DateLongTH = (date: Date) => {
  dayjs.locale("th");
  return dayjs(date).format("DD MMMM BBBB");
};

/* 07 ก.พ. 2566 */
const DateShortTH = (date: Date) => {
  dayjs.locale("th");
  return dayjs(date).format("DD MMM BB");
};

/* 07 February 2023 */
const DateLongEN = (date: Date) => {
  dayjs.locale("en");
  return dayjs(date).format("DD MMMM YYYY");
};

/* 07 Feb 23 */
const DateShortEN = (date: Date) => {
  dayjs.locale("en");
  return dayjs(date).format("DD MMM YY");
};

const DateNumberShortTH = (date: Date) => {
  dayjs.locale("th");
  return dayjs(date).format("DDMMBBBB");
};

const DateLongTimeToTimeTH = (
  dateStart: Date,
  dateEnd: Date,
  onlyTime = false
) => {
  dayjs.locale("th");
  if (onlyTime)
    return dayjs(dateStart)
      .format("HH:mm")
      .concat(" - ", dayjs(dateEnd).format("HH:mm"));
  else
    return dayjs(dateStart)
      .format("DD MMMM BBBB HH:mm")
      .concat(" - ", dayjs(dateEnd).format("HH:mm"));
};

/**
 * use only component cardManageRoom
 * @param date input date format
 * @param time input time format
 * @returns format string
 */
const MapDateAndTimeFormat = (date: string | Date, time: string): string => {
  return dayjs(dayjs(date).format("YYYY-MM-DD") + " " + time).format();
};

function convertThaiDateStringToDate(thaiDateString: string): Date | null {
  // Mapping of Thai month abbreviations to corresponding English month names
  const thaiMonthMapping: Record<string, string> = {
    "ม.ค.": "Jan",
    "ก.พ.": "Feb",
    "มี.ค.": "Mar",
    "เม.ย.": "Apr",
    "พ.ค.": "May",
    "มิ.ย.": "Jun",
    "ก.ค.": "Jul",
    "ส.ค.": "Aug",
    "ก.ย.": "Sep",
    "ต.ค.": "Oct",
    "พ.ย.": "Nov",
    "ธ.ค.": "Dec",
  };

  // Split the input string into day, month, and year parts
  const parts = thaiDateString.split(" ");

  if (parts.length !== 3) {
    console.error("Invalid input format");
    return null;
  }

  const day = parseInt(parts[0], 10);
  const monthAbbreviation = parts[1];
  const year = parseInt(parts[2], 10) - 543; // Convert Thai year to Western year

  // Get the English month name from the mapping
  const month = thaiMonthMapping[monthAbbreviation];

  if (!month) {
    console.error("Invalid month abbreviation");
    return null;
  }

  // Construct the date string in the format "MMM DD, YYYY"
  const dateString = `${month} ${day}, ${year}`;

  // Parse the date string and return the Date object
  const dateObject = new Date(dateString);
  return dateObject;
}
function DateCheckAge(BrithDate: string | Date) {
  const date1 = dayjs();
  return Math.floor(date1.diff(BrithDate, "year", true));
}

export default {
  DateLongEN,
  DateShortEN,
  DateLongTH,
  DateShortTH,
  DateNumberShortTH,
  DateLongTimeToTimeTH,
  DateCheckAge,
  MapDateAndTimeFormat,
  convertThaiDateStringToDate,
};
