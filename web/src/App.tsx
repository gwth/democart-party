import { Route, Routes, useLocation, useNavigate } from "react-router-dom";
import Login from "./pages/Login";
import Register from "./pages/Register";
import RenewParty from "./pages/Renew";
import AdminPage from "./pages/Admin/adminPage";
import DetailAgent from "./pages/Admin/detailAgent";
import SelectCSV from "./pages/SelectCSV";
import SelectDataformCSV from "./pages/SelectCSV/SelectDataformCSV";
import FinishNameList from "./pages/SelectCSV/FinishNameList";
import { useAppDispatch, useAppSelector } from "./store/hook";
import { logout, selectAuthStatus } from "./store/auth/authSlice";
import { useEffect, useState } from "react";
import RequiredAuth from "./components/Auth/RequiredAuth";
import { fetchUserMe } from "./store/user/userSlice";
import LoadingX from "./components/elements/LoadingX";
import CheckStatus from "./pages/Checkstatus/CheckStatus";
import CheckUserData from "./pages/CheckUserData/CheckUserData";
import RequiredRole from "./components/Auth/RequiredRole";
import RoutePage from "./pages/Routes";
import Forbidden from "./components/Forbidden";
import RenewPartyAgent from "./pages/RenewAgent";

function App() {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const authStatus = useAppSelector(selectAuthStatus);
  const location = useLocation();
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    const fakeDataFetch = () => {
      setTimeout(() => {
        setIsLoading(false);
      }, 1500);
    };

    fakeDataFetch();
  }, []);

  const routesRequireUserMe = [
    "/",
    "/home",
    "/detail-agent/:id",
    "/renew-agent",
    "/selectcsv",
    "/selectdataformcsv/:id",
    "/selectpayment",
    "/finishnamelist",
    "/main",
    "/forbidden",
  ];
  useEffect(() => {
    //  nprogress.start();


    if (routesRequireUserMe.some(s => s.includes(location.pathname.split('/')[1]))) {

      dispatch(fetchUserMe())
        .unwrap()
        .then((data) => {
          if (!data) {
            navigate("/");
            dispatch(logout());
          }
          // navigate("/main", { replace: true });
        });
    }
    //  nprogress.complete();

    return () => {
      //  nprogress.complete();
    };
  }, [authStatus]);

  return (
    <>
      <Routes>
        {/* Public routes */}
        {isLoading === true ? <Route path="/" element={<LoadingX />} /> : <Route index element={<Login />} />}
        <Route path="main" element={<RoutePage />} />
        <Route path="register" element={<Register />} />
        <Route path="check-status" element={<CheckStatus />} />
        <Route path="check-user-data" element={<CheckUserData />} />
        <Route path="renew-party" element={<RenewParty />} />
        <Route path="forbidden" element={<Forbidden />} />
        {/* Private routes */}
        <Route element={<RequiredAuth isAllowed={authStatus === "loggedIn"} />}>
          <Route element={<RequiredRole allowedRoles={[2]} />}>
            <Route path="renew-agent" element={<RenewPartyAgent />} />
            <Route path="selectcsv" element={<SelectCSV />} />
            <Route path="selectdataformcsv/:id" element={<SelectDataformCSV />} />
            <Route path="finishnamelist" element={<FinishNameList />} />
          </Route>

          <Route element={<RequiredRole allowedRoles={[1]} />}>
            <Route path="home" element={<AdminPage />} />
            <Route path="detail-agent/:id" element={<DetailAgent />} />
          </Route>
        </Route>
      </Routes>
    </>
  );
}

export default App;
