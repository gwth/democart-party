import { PrismaClient } from "@prisma/client";

export class tMemberStatusService {
  t_mem_status = new PrismaClient().t_mem_status;
  public async listMemberStatus(idCard: string) {
    return this.t_mem_status.findUnique({
      where: {
        idcard: idCard,
      },
      select: {
        idcard: true,
        mem_status: true,
        renew_year: true,
        mem_type: true,
        newmem_year: true,
      },
    });
  }
}
