import { PrismaClient } from "@prisma/client";

export class DmpUserLoginMainService {
  user = new PrismaClient().dmp_user_login_main;

  async getUserByUname(uname: string, pword: string) {
    return await this.user.findFirst({
      where: {
        u_name: uname,
        p_word: pword,
      },
    });
  }
}
