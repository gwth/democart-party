import { PrismaClient } from "@prisma/client";

export class memberStatusService {
  member_status = new PrismaClient().member_status;
  province = new PrismaClient();
  public async listMemberStatus(idCard: string) {
    return this.member_status.findUnique({
      where: {
        idcard: idCard,
      },
      select: {
        idcard: true,
        mem_status: true,
        deprived_date: true,
      },
    });
  }

  async datamemberstatus(idCard: string) {
    const data = await this.member_status.findUnique({
      where: {
        idcard: idCard,
      },
      select: {
        prov_cont: true,
        since_member: true,
        deprived_date: true,
      },
    });
    if (!data?.prov_cont) {
      return null;
    } else {
      const dataRenew = await (this.province as any)[
        data.prov_cont.toLowerCase()
      ].findUnique({
        where: {
          idcard: idCard,
        },
      });

      if (dataRenew) {
        dataRenew.since_member = data.since_member;
        dataRenew.deprived_date = data.deprived_date;
      }

      return dataRenew;
    }
  }

  async findMemberByIdCard(idcard: string) {
    return await this.member_status.findUnique({
      where: {
        idcard,
      },
    });
  }
}
