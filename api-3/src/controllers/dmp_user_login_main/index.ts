import { TypedRequestBody } from "@/interfaces/express/request";
import { DmpUserLoginMainService } from "@/services/dmp_user_login_main/dmp_user_login_main.service";
import { NextFunction, Response } from "express";

export class DmpUserLoginMainController {
  dmp_user_login_main = new DmpUserLoginMainService();

  findUserByUsernameAndPassword = async (
    req: TypedRequestBody<{ username: string; password: string }>,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const { username, password } = req.body;

      if (!username || !password) {
        return res.status(200).json({
          user: null,
          message: "Username or Password required.",
          status: 200,
        });
      }

      const foundUser = await this.dmp_user_login_main.getUserByUname(
        username,
        password
      );

      if (!foundUser) {
        return res
          .status(200)
          .json({
            user: null,
            message: "Username or Password incorrect.",
            status: 404,
          });
      }

      if (foundUser.p_word !== password || foundUser.u_name !== username) {
        return res.status(200).json({
          user: null,
          message: "Username or Password incorrect.",
          status: 404,
        });
      }

      return res
        .status(200)
        .json({ user: foundUser, message: "Found user.", status: 200 });
    } catch (error) {
      next(error);
    }
  };
}
