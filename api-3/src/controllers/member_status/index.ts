import {
  TypedRequest,
  TypedRequestParams,
  TypedRequestQuery,
} from "@/interfaces/express/request";
import { memberStatusService } from "@/services/member_status/member_status.service";
import { HttpException } from "@/shared/http/httpException";
import logger from "@/shared/http/logger";
import { NextFunction, Response } from "express";
export class memberStatus {
  public member_status = new memberStatusService();
  getMemberStatus = async (
    req: TypedRequestQuery<{ idCard: string }>,
    res: Response
  ) => {
    const data = await this.member_status.listMemberStatus(req.query.idCard);
    if (!data) return res.status(404).json({ message: "data not found" });
    return res.status(200).json({ data, message: "success" });
  };
  getMemberStatusRenew = async (
    req: TypedRequestQuery<{ idCard: string }>,
    res: Response
  ) => {
    const data = await this.member_status.datamemberstatus(req.query.idCard);
    if (!data) return res.status(404).json({ message: "data not found" });
    return res.status(200).json({ data, message: "success" });
  };

  getMemberByIdCard = async (
    req: TypedRequest<{ idCard: string }, {}, { status: string }>,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const { idCard } = req.params;
      const { status } = req.query;

      const statusArr: number[] = JSON.parse(status);
      let isCanRegister: boolean;

      if (!idCard || status.length == 0) {
        return res
          .status(200)
          .json({ message: "IdCard or status is required.", status: 404 });
      }

      const foundMember = await this.member_status.findMemberByIdCard(idCard);

      if (!foundMember) {
        return res.status(200).json({
          message: "Member not found",
          status: 200,
          isCanRegister: true,
        });
      }

      isCanRegister = statusArr.some((status) => {
        try {
          return Number(foundMember?.mem_status) === status;
        } catch (error) {
          return false;
        }
      });

      return res
        .status(200)
        .json({ message: "Found member", isCanRegister, status: 200 });
    } catch (error) {
      logger.error("Get member by id failure: ", error);
      next(error);
    }
  };
}
