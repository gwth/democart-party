import { TypedRequestQuery } from "@/interfaces/express/request";
import { tMemberStatusService } from "@/services/t_member_status/t_member_status.service";
import { Response } from "express";
export class tMemberStatus {
  public t_member_status = new tMemberStatusService();
  getTmemberStatus = async (
    req: TypedRequestQuery<{ idCard: string }>,
    res: Response
  ) => {
    const data = await this.t_member_status.listMemberStatus(req.query.idCard);

    if (data) {
      return res.status(200).json({ data: data, message: "success" });
    } else {
      return res.status(200).json({
        message: "success",
        data: {
          mem_status: 0,
        },
      });
    }
  };
}
