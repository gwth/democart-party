import { memberStatus } from "./../controllers/member_status/index";
import { tMemberStatus } from "./../controllers/t_member_status/index";
import { Router } from "express";
import { DmpUserLoginMainController } from "@/controllers/dmp_user_login_main";
export class AuthRoute {
  public path: string = "/data";
  public router = Router();
  public tMemberStatus = new tMemberStatus();
  public memberStatus = new memberStatus();
  public dmpUserLoginMain = new DmpUserLoginMainController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}/t-member-status`,
      this.tMemberStatus.getTmemberStatus
    );
    this.router.get(
      `${this.path}/member-status`,
      this.memberStatus.getMemberStatus
    );
    this.router.get(
      `${this.path}/member-status/:idCard`,
      this.memberStatus.getMemberByIdCard
    );
    this.router.get(
      `${this.path}/member-status-renew`,
      this.memberStatus.getMemberStatusRenew
    );
    this.router.post(
      `${this.path}/user`,
      this.dmpUserLoginMain.findUserByUsernameAndPassword
    );
  }
}
