import jwt from "jsonwebtoken";
import { jwtConfig } from "@/shared/utils/config";

export class JWT {
  static jwtSecretKey: string = jwtConfig.secretKey;
  static jwtExpiresIn: string = jwtConfig.expiresIn;

  public static generateToken(username: string, userId: number) {
    return jwt.sign({ username: username, userId: userId }, this.jwtSecretKey, {
      expiresIn: this.jwtExpiresIn,
    });
  }

  public static verifyToken(token: string) {
    token = token.split(" ")[1];
    return jwt.verify(token, this.jwtSecretKey);
  }
}
