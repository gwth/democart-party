"use strict";
exports.__esModule = true;
exports.indexRouter = void 0;
var express_1 = require("express");
var controllers_1 = require("../controllers");
exports.indexRouter = (0, express_1.Router)();
exports.indexRouter.get("/", controllers_1.fetchData);
//# sourceMappingURL=index.js.map