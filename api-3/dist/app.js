"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
exports.app = void 0;
var express_1 = __importDefault(require("express"));
// import { PrismaClient } from "@prisma/client";
var routes_1 = require("./routes");
// export const prisma = new PrismaClient();
exports.app = (0, express_1["default"])();
exports.app.use("/", routes_1.indexRouter);
//# sourceMappingURL=app.js.map