"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var supertest_1 = __importDefault(require("supertest"));
var app_1 = require("../app");
describe("indexController test", function () {
    it("GET /", function (done) {
        (0, supertest_1["default"])(app_1.app).get("/").expect(200).expect({ msg: "hello!" }, done);
    });
});
//# sourceMappingURL=index.spec.js.map