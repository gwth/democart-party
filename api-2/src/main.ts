import { App } from "./applications/expressApp";

import { AuthRoute } from "./routes";

import { port } from "@/shared/utils/config";
import prisma from "./shared/utils/prisma";
import logger from "./shared/http/logger";

const app = new App([new AuthRoute()]);

app.listen(Number(port));

prisma
  .$connect()
  .then(() => logger.info("Connected to database"))
  .catch((e: Error) => console.log(e));
