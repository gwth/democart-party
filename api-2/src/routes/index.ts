import { Router } from "express";
import { PrismaClient } from "@prisma/client";
import { create } from "domain";
export class AuthRoute {
  public path: string = "/create";
  public router = Router();

  constructor() {
    this.initializeRoutes();
  }
  member_status = new PrismaClient().member_status;
  t_renew_saka = new PrismaClient().t_renew_saka;
  province = new PrismaClient();
  miss_province = new PrismaClient().miss_province;
  t_pic_member_regist_new = new PrismaClient().t_pic_member_regist_new;
  t_pic_member_renew_new = new PrismaClient().t_pic_member_renew_new;
  private initializeRoutes() {
    this.router.post(`${this.path}/member-status`, async (req, res) => {
      // console.log(req.body);
      //data use in member_status
      const {
        idCard,
        prov_cont,
        mem_status,
        mem_type,
        payment_channel,
        user_insert_name,
        newmem_year,
        chk_st_p2,
        g_kkt_id,
        signdate,
      } = req.body;

      //data use in ชื่อจังหวัด(acronym_en)
      const {
        birthday,
        title,
        f_name,
        l_name,
        nationality,
        add_no,
        moo,
        soi,
        alley,
        road,
        sub_district,
        district,
        province,
        tel,
        move_out,
        sex,
        email,
      } = req.body;
      console.log("emial " + email);
      //data use in ชื่อจังหวัด(t_pic_member_regist_new)
      const { pic_data, sc_idcard, sc_reg, registrar_id } = req.body;
      try {
        let memBerData = await this.member_status.findFirst({
          where: {
            idcard: idCard.toString(),
          },
        });
        if (memBerData) {
          console.log("update");
          const dataMemberStatus = await this.member_status.update({
            where: {
              id: memBerData.id,
            },
            data: {
              idcard: idCard.toString(),
              prov_cont: prov_cont.toLowerCase(),
              mem_status: mem_status,
              payment_channel: payment_channel,
              mem_type: mem_type,
              user_insert_name: user_insert_name,
              newmem_year: newmem_year,
              chk_st_p2: chk_st_p2,
              g_kkt_id: g_kkt_id,
              signdate: signdate,
            },
          });
          try {
            console.log(prov_cont.toLowerCase());

            // let x = await (this.province as any)[prov_cont.toLowerCase()];
            // let y = await this.province.rbr;
            // console.log(x.findMany());
            // console.log(y.findMany());
            const result = await (this.province as any)[
              prov_cont.toLowerCase()
            ].upsert({
              where: {
                idcard: idCard.toString(),
              },
              create: {
                idcard: idCard.toString(),
                birthday,
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
              update: {
                birthday,
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
            });
          } catch (err) {
            console.log(err);
            const result = await this.miss_province.upsert({
              where: {
                idcard: idCard.toString(),
              },
              create: {
                idcard: idCard.toString(),
                birthday,
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
              update: {
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
            });
          }
          const dataPathImageRegist =
            await this.t_pic_member_regist_new.updateMany({
              where: {
                member_status_id: dataMemberStatus.id,
              },
              data: {
                idcard: idCard.toString(),
                pic_data,
                sc_idcard,
                sc_reg,
                registrar_id,
              },
            });

          return res
            .status(200)
            .json({ status: 200, message: "success update member_status" });
        } else {
          const dataMemberStatus = await this.member_status.create({
            data: {
              idcard: idCard.toString(),
              prov_cont: prov_cont.toLowerCase(),
              mem_status: mem_status,
              payment_channel: payment_channel,
              mem_type: mem_type,
              user_insert_name: user_insert_name,
              newmem_year: newmem_year,
              chk_st_p2: chk_st_p2,
              g_kkt_id: g_kkt_id,
              signdate: signdate,
            },
          });
          try {
            // let x = await (this.province as any)[
            //   prov_cont.toLowerCase()
            // ].findMany();
            // console.log(x);
            const result = await (this.province as any)[
              prov_cont.toLowerCase()
            ].upsert({
              where: {
                idcard: idCard.toString(),
              },
              create: {
                idcard: idCard.toString(),
                birthday,
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
              update: {
                birthday,
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
            });
          } catch (err) {
            console.log(err);
            const result = await this.miss_province.upsert({
              where: {
                idcard: idCard.toString(),
              },
              create: {
                idcard: idCard.toString(),
                birthday,
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
              update: {
                title,
                f_name,
                l_name,
                prov_cont,
                nationality: nationality ? nationality : "",
                add_no: add_no ? add_no : "",
                moo: moo ? moo : "",
                soi: soi ? soi : "",
                alley: alley ? alley : "",
                road: road ? road : "",
                sub_district,
                district,
                province,
                tel,
                move_out,
                sex: sex ? sex : "",
                email: email ? email : "",
                mid: "",
                zipcode: "",
                village: "",
                tel_home: "",
                id_line: "",
                gen_card: "",
                exp_card: "",
                religion: "",
                add_no_current: "",
                moo_current: "",
                soi_current: "",
                alley_current: "",
                road_current: "",
                village_current: "",
                sub_district_current: "",
                district_current: "",
                province_current: "",
                zipcode_current: "",
              },
            });
          }
          const dataPathImageRegist = await this.t_pic_member_regist_new.create(
            {
              data: {
                member_status_id: dataMemberStatus.id,
                idcard: idCard.toString(),
                pic_data,
                sc_idcard,
                sc_reg,
                registrar_id,
              },
            }
          );
          return res
            .status(200)
            .json({ status: 200, message: "success create member_status" });
        }
      } catch (error) {
        return res.status(200).json({ status: 500, message: error });
      }
    });
    this.router.get(`${this.path}/t-renew-saka-id`, async (req, res) => {
      try {
        const idCard = req.query.idCard ? req.query.idCard.toString() : "";
        let data = await this.t_renew_saka.findMany({
          where: {
            idcard: idCard.toString(),
          },
          select: {
            id: true,
            idcard: true,
            renew_year: true,
          },
        });
        console.log("req.query", data);
        return res.status(200).json({ status: 200, data: data });
      } catch (error) {
        return res.status(200).json({ status: 500, message: error });
      }
    });
    this.router.post(`${this.path}/t-renew-saka`, async (req, res) => {
      console.log(req.body);
      //data use in member_status
      if (!req.body) {
        return;
      }
      console.log(req.body);
      const dataRenew = req.body;

      try {
        for (let index = 0; index < dataRenew.length; index++) {
          const element = dataRenew[index];
          const dataRenewSaka = await this.t_renew_saka.create({
            data: {
              idcard: element.idCard.toString(),
              user_update_name: element.user_update_name,
              renew_date: element.renew_date,
              renew_year: element.renew_year,
              renew_ref: element.renew_ref,
              chk_renew_st: element.chk_renew_st,
              mem_type: element.mem_type,
              g_kkt_id: element.g_kkt_id,
            },
          });

          const dataPathImageRegist = await this.t_pic_member_renew_new.create({
            data: {
              t_renew_saka_id: dataRenewSaka.id,
              idcard: element.idCard.toString(),
              pic_data: element.pic_data,
              sc_idcard: element.sc_idcard,
              sc_reg: element.sc_reg,
              registrar_id: element.registrar_id,
            },
          });
        }
        return res.status(200).json({ status: 200, message: "success" });
      } catch (error) {
        return res.status(200).json({ status: 500, message: error });
      }

      // const {
      //   idCard,
      //   user_update_name,
      //   renew_date,
      //   renew_year,
      //   renew_ref,
      //   chk_renew_st,
      //   mem_type,
      //   g_kkt_id,
      // } = req.body;

      // //data use in ชื่อจังหวัด(t_pic_member_regist_new)
      // const { pic_data, sc_idcard, sc_reg, registrar_id } = req.body;
    });

    this.router.get(`/data/get-member-status`, async (req, res) => {
      try {
        // console.log(req.query);
        let data: any = null;
        const idCard = req.query.idCard ? req.query.idCard.toString() : "";
        data = await this.member_status.findFirst({
          where: {
            idcard: idCard.toString(),
          },
          select: {
            id: true,
            number_ref: true,
            chk_st_p2: true,
            prov_cont: true,
          },
        });

        let dataProvince = null;
        if (data) {
          if (!data?.prov_cont) {
            return null;
          } else {
            dataProvince = await (this.province as any)[
              data.prov_cont.toLowerCase()
            ].findUnique({
              where: {
                idcard: idCard,
              },
              select: {
                birthday: true,
              },
            });
          }
        }

        if (dataProvince) {
          data.birthday = dataProvince.birthday;
        }
        // console.log("req.query", data);
        return res.status(200).json({ status: 200, data: data });
      } catch (error) {
        return res
          .status(200)
          .json({ status: 500, message: error, data: null });
      }
    });
  }
}
