import "reflect-metadata";
import express, { Request, Response } from "express";
import cors from "cors";
import morganMiddleware from "@/shared/middleware/logHttpMiddleware";
import fileUpload from "express-fileupload";
import logger from "@/shared/http/logger";

import { Routes } from "@/interfaces/route/routes.intereface";
import { ErrorMiddleware } from "@/shared/middleware/errorMiddleware";

export class App {
  public app: express.Application;
  public port: number;
  public env: string;

  constructor(routes: Routes[]) {
    this.app = express();
    this.env = process.env.NODE_ENV || "development";

    this.initializeMiddlewares();
    this.initializeRoutes(routes);
    this.initializeErrorHandling();
  }

  public listen(port: number) {
    this.app.listen(port, () => {
      logger.info(`=================================`);
      logger.info(`======= ENV: ${this.env} =======`);
      logger.info(`🚀 App listening on the port ${port}`);
      logger.info(`=================================`);
    });
  }

  public getServer() {
    return this.app;
  }

  public initializeMiddlewares() {
    this.app.use(
      fileUpload({
        useTempFiles: true,
        tempFileDir: "/tmp/",
      })
    );

    this.app.use(cors({ origin: "*" }));
    this.app.use(morganMiddleware);
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
  }

  private initializeRoutes(routes: Routes[]) {
    this.app.use("/api/health", (req: Request, res: Response) => {
      return res.status(200).send({ message: "server is up and running" });
    });

    // this.app.use(AuthMiddleware);
    routes.forEach((route) => {
      this.app.use("/api", route.router);
    });
  }

  private initializeErrorHandling() {
    this.app.use(ErrorMiddleware);
  }
}
