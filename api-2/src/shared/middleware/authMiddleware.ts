import { NextFunction, Request, Response } from "express";
import { JWT } from "../utils/jwt";
import { JwtPayload } from "jsonwebtoken";
import logger from "../http/logger";

declare global {
  namespace Express {
    interface Request {
      user?: JwtPayload | { username: string; userId: number };
    }
  }
}

export function AuthMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const token = req.headers.authorization;

  if (!token) {
    return res.status(401).json({
      message: "Unauthorized",
    });
  }
  try {
    const decoded = JWT.verifyToken(token);
    if (!decoded) {
      throw new Error("Invalid token");
    }
    req.user = decoded as any;
    next();
  } catch (error) {
    logger.error("Verify token failure: ", error);
    return res.status(401).json({
      message: "Unauthorized",
    });
  }
}