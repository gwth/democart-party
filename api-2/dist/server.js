"use strict";
exports.__esModule = true;
var app_1 = require("./app");
var PORT = Number(process.env.PORT) || 8080;
app_1.app.listen(PORT, function () { return console.log("server running at port: ".concat(PORT)); });
//# sourceMappingURL=server.js.map