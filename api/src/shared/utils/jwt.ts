import jwt from "jsonwebtoken";
import { jwtConfig } from "@/shared/utils/config";

export class JWT {
  static jwtSecretKey: string = jwtConfig.secretKey;
  static jwtExpiresIn: string = jwtConfig.expiresIn;

  public static generateToken(payload: any, rememberMe: boolean) {
    console.log("🚀 ~ JWT ~ generateToken ~ rememberMe:", rememberMe);

    return jwt.sign(payload, this.jwtSecretKey, {
      expiresIn: rememberMe ? "30d" : this.jwtExpiresIn,
    });
  }

  public static verifyToken(token: string) {
    token = token.split(" ")[1];
    return jwt.verify(token, this.jwtSecretKey);
  }
}
