import { NextFunction, Request, Response } from "express";
import { JWT } from "../utils/jwt";
import { JwtPayload } from "jsonwebtoken";
import logger from "../http/logger";

declare global {
  namespace Express {
    interface Request {
      user?: JwtPayload | { username: string; userId: number };
    }
  }
}

const validApiKeys = [
  "gFXHQD9fOUOo2QY5oSERu9qlblMomXvtbOPNGy0ynkqT3u9nKITe2Tk8xLrXxPK9ndAFaat6eaFP9o6hSQds4XTI99GPG8CvPwaLSgaCYolhawmuezmRNgZHac56g3HE",
];

export function AuthMiddleware(req: Request, res: Response, next: NextFunction) {
  const token = req.headers.authorization;
  const apiKey = req.headers["x-api-key"];

  if (apiKey || validApiKeys.includes(apiKey as string)) {
    req.user = undefined;
    next();

    return;
  }

  if (!token) {
    return res.status(401).json({
      message: "Unauthorized",
    });
  }
  try {
    const decoded = JWT.verifyToken(token);
    if (!decoded) {
      throw new Error("Invalid token");
    }
    req.user = decoded as any;
    next();
  } catch (error) {
    logger.error("Verify token failure: ", error);
    return res.status(401).json({
      message: "Unauthorized",
    });
  }
}
