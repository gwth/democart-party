import { App } from "./applications/expressApp";
import { UserRoute } from "./routes/user";
import { AuthRoute } from "./routes/auth";

import { port } from "@/shared/utils/config";
import prisma from "./shared/utils/prisma";
import logger from "./shared/http/logger";

import { RegisterRoute } from "./routes/register";
import { ApprovalRoute } from "./routes/approval";
import { RegisterByAgentRoute } from "./routes/registerByAgent";
import { CheckStatusRoute } from "./routes/checkstatus";
const app = new App([
  new UserRoute(),
  new AuthRoute(),
  new RegisterRoute(),
  new ApprovalRoute(),
  new RegisterByAgentRoute(),
  new CheckStatusRoute(),
]);

app.listen(Number(port));

prisma
  .$connect()
  .then(() => logger.info("Connected to database"))
  .catch((e: Error) => console.log(e));
