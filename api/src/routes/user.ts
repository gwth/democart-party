import { UserController } from "./../controllers/user";
import { Router } from "express";

export class UserRoute {
  public path: string = "/user";
  public router = Router();
  public userController = new UserController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/me`, this.userController.findUserMe);
  }
}
