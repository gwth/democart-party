import { RegisteByAgentrController } from "@/controllers/registerByAgent";
import { Router } from "express";

export class RegisterByAgentRoute {
  public path: string = "/registerByAgent";
  public router = Router();
  public registerByAgentController = new RegisteByAgentrController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    
    this.router.post(`${this.path}/list-register`,this.registerByAgentController.getRegisterAll);
    this.router.get(`${this.path}/register-finish`,this.registerByAgentController.getRegisterLog);

  }
}
