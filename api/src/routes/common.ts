import { CommonController } from "@/controllers/common";
import { Router } from "express";

export class CommonRoute {
  public path: string = "/common";
  public router = Router();
  public commonController = new CommonController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}/test`, this.commonController.test);
    this.router.get(`${this.path}/prefix`, this.commonController.getPrefix);
    this.router.get(`${this.path}/prefix-name`, this.commonController.getPrefixByName);

    this.router.get(`${this.path}/province`, this.commonController.getProvince);
    this.router.get(`${this.path}/province-name`, this.commonController.getProvinceByName);

    this.router.get(`${this.path}/aumper/:id`, this.commonController.getAumperByProvinceId);
    this.router.get(`${this.path}/aumper-name`, this.commonController.getAumperByName);
    
    this.router.get(`${this.path}/tumbon/:id`, this.commonController.getTumbonByAumperId);
    this.router.get(`${this.path}/tumbon-name`, this.commonController.getTumbonByName);

    this.router.get(`${this.path}/type-register`, this.commonController.getTypeRegister);
    this.router.get(`${this.path}/type-comment`, this.commonController.getTypeComment);
  }
}
