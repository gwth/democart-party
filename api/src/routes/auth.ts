import { AuthController } from "@/controllers/auth/login";
import { Router } from "express";

export class AuthRoute {
  public path: string = "/auth";
  public router = Router();
  public authController = new AuthController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/login`, this.authController.login);
  }
}
