import { RegisterController } from "@/controllers/register";
import { Router } from "express";

export class RegisterRoute {
  public path: string = "/register";
  public router = Router();
  public registerController = new RegisterController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}/logs`,
      this.registerController.getRegisterLogs
    );
    this.router.get(
      `${this.path}/logs/:id`,
      this.registerController.getRegisterById
    );
    this.router.get(
      `${this.path}/logs/status/:status`,
      this.registerController.getRegisterByStatus
    );
    this.router.get(
      `${this.path}/logs-list`,
      this.registerController.getRegisterLogList
    );
    this.router.get(
      `${this.path}/check-logs/:id`,
      this.registerController.checkRegisterLogByIdCard
    );

    this.router.get(
      `${this.path}/check-member/:id`,
      this.registerController.checkMemberByIdCard
    );
    this.router.post(
      `${this.path}/logs`,
      this.registerController.createRegisterLog
    );
  }
}
