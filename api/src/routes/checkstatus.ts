import { CheckStatusController } from "@/controllers/checkstatus";

import { Router } from "express";

export class CheckStatusRoute {
  public path: string = "/checkstatus";
  public router = Router();
  public checkStatusController = new CheckStatusController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(
      `${this.path}/get-status-user-register`,
      this.checkStatusController.getStatusUserRegist
    );
    this.router.get(
      `${this.path}/get-check-member-detail`,
      this.checkStatusController.getCheckMemberDetail
    );
    this.router.get(
      `${this.path}/get-check-renew`,
      this.checkStatusController.getCheckRenew
    );
  }
}
