import { ApprovalController } from "@/controllers/approval";

import { Router } from "express";

export class ApprovalRoute {
  public path: string = "/approval";
  public router = Router();
  public approvalController = new ApprovalController();
  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(
      `${this.path}/set-status`,
      this.approvalController.setStatusApprove
    );
  }
}
