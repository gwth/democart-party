import { IRegister } from "@/dtos/register/register";
import { PrismaClient, register_log } from "@prisma/client";
import { promises } from "dns";

export class ApprovalService {
  type_prefix_th = new PrismaClient().type_prefix_th;
  province = new PrismaClient().province;
  aumper = new PrismaClient().aumper;
  tumbon = new PrismaClient().tumbon;
  typeRegist = new PrismaClient().type_register;
  typeComment = new PrismaClient().type_comment;
  register_log = new PrismaClient().register_log;
  register = new PrismaClient().register;

  public async updateStatus(
    id: number,
    status: number,
    comment_id: number | null,
    comment: string | null,
    idUserModify: number,
    acceptDate: Date,
    show: number
  ): Promise<register_log> {
    return await this.register_log.update({
      where: {
        id: id,
      },
      data: {
        status: status,
        comment_id: comment_id,
        comment: comment,
        modify_by: idUserModify,
        accept_date: acceptDate,
        show: show,
      },
    });
  }

  public async upsertRegisterByIdCard(dataRegister: IRegister) {
    return await this.register.upsert({
      where: {
        id_card: dataRegister.id_card,
      },
      update: {
        name_th: dataRegister.name_th,
        surname_th: dataRegister.surname_th,
        name_en: dataRegister.name_en,
        surname_en: dataRegister.surname_en,
        birth_date: dataRegister.birth_date,
        address_number: dataRegister.address_number,
        village_no: dataRegister.village_no,
        alley: dataRegister.alley,
        soi: dataRegister.soi,
        road: dataRegister.road,
        tumbon_id: dataRegister.tumbon_id,
        aumper_id: dataRegister.aumper_id,
        province_id: dataRegister.province_id,

        type_register_id: dataRegister.type_register_id,
        phone: dataRegister.phone,
        status: dataRegister.status,
        accept_type_id: dataRegister.accept_type_id,
        accept_date: dataRegister.accept_date,
        prefix_other_th: dataRegister.prefix_other_th,
        prefix_other_en: dataRegister.prefix_other_en,
        prefix_th_id: dataRegister.prefix_th_id,
        prefix_en_id: dataRegister.prefix_en_id,
      },
      create: {
        name_th: dataRegister.name_th,
        surname_th: dataRegister.surname_th,
        name_en: dataRegister.name_en,
        surname_en: dataRegister.surname_en,
        birth_date: dataRegister.birth_date,
        address_number: dataRegister.address_number,
        village_no: dataRegister.village_no,
        alley: dataRegister.alley,
        soi: dataRegister.soi,
        road: dataRegister.road,
        tumbon_id: dataRegister.tumbon_id,
        aumper_id: dataRegister.aumper_id,
        province_id: dataRegister.province_id,
        id_card: dataRegister.id_card,
        type_register_id: dataRegister.type_register_id,
        phone: dataRegister.phone,
        status: dataRegister.status,
        accept_type_id: dataRegister.accept_type_id,
        accept_date: dataRegister.accept_date,
        prefix_other_th: dataRegister.prefix_other_th,
        prefix_other_en: dataRegister.prefix_other_en,
        prefix_th_id: dataRegister.prefix_th_id,
        prefix_en_id: dataRegister.prefix_en_id,
      },
    });
  }

  public async getRegistLogById(id: number) {
    return await this.register_log.findUnique({
      where: {
        id,
      },
      select: {
        tumbon: {
          select: {
            name: true,
          },
        },
        aumper: {
          select: {
            name: true,
          },
        },
        province: {
          select: {
            name: true,
            abbreviation: true,
          },
        },
        users_register_log_agent_idTousers: {
          select: {
            username: true,
            first_name: true,
          },
        },
        type_prefix_th: {
          select: {
            data: true,
          },
        },
        type_accept: {
          select: {
            cost: true,
            data: true,
          },
        },
      },
    });
  }
}
