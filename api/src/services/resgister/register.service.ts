import { create } from "domain";
import { Register } from "./../../dtos/register/register";
import { RegisterLogCreate } from "@/dtos/register/register";
import { PrismaClient } from "@prisma/client";

export class RegisterService {
  register = new PrismaClient().register;
  register_log = new PrismaClient().register_log;
  public async listRegisterLogs() {
    return await this.register_log.findMany({
      include: {
        users_register_log_agent_idTousers: {
          select: {
            first_name: true,
            last_name: true,
          },
        },
      },
    });
  }
  public async findRegisterLogById(id: number) {
    return await this.register_log.findUnique({
      where: {
        id: id,
      },
      include: {
        users_register_log_agent_idTousers: {
          select: {
            first_name: true,
            last_name: true,
          },
        },
        type_prefix_th: {
          select: {
            data: true,
          },
        },
        tumbon: {
          select: {
            name: true,
          },
        },
        aumper: {
          select: {
            name: true,
          },
        },
        province: {
          select: {
            name: true,
          },
        },
      },
    });
  }
  public async findRegisterLogBystatus(status: number) {
    return this.register_log.findMany({
      where: {
        status: status,
      },
    });
  }
  public async findRegisterList(
    skip: number,
    pageSize: number,
    textSearch: string = "",
    status: number | null = null,
    ids?: number[]
  ) {
    const totalCount = await this.register_log.count({
      where: {
        ...((status || status == 0) && { type_register_id: status }),
        show: 1,
        OR: [
          {
            id_card: {
              contains: textSearch,
            },
          },
          {
            name_th: {
              contains: textSearch,
            },
          },
          {
            surname_th: {
              contains: textSearch,
            },
          },
          {
            agent_id: {
              in: ids,
            },
          },
        ],
      },
    });

    const userList = await this.register_log.findMany({
      skip: skip,
      take: pageSize,

      where: {
        ...((status || status == 0) && { type_register_id: status }),
        show: 1,
        OR: [
          {
            id_card: {
              contains: textSearch,
            },
          },
          {
            name_th: {
              contains: textSearch,
            },
          },
          {
            surname_th: {
              contains: textSearch,
            },
          },
          {
            agent_id: {
              in: ids,
            },
          },
        ],
      },
      orderBy: {
        created_at: "desc",
      },
      include: {
        users_register_log_agent_idTousers: {
          select: {
            first_name: true,
            last_name: true,
          },
        },
      },
    });
    return { dataUser: userList, totalCount: totalCount };
  }

  async saveRegisterLog(regis: RegisterLogCreate) {
    return await this.register_log.create({
      data: {
        name_th: regis.firstName,
        surname_th: regis.lastName,
        birth_date: regis.birthDate,
        address_number: regis.houseNumber,
        village_no: regis.moo,
        soi: regis.soi,
        road: regis.road,
        tumbon_id: regis.tombon,
        aumper_id: regis.amphoe,
        province_id: regis.province,
        id_card: regis.idCard,
        payment: regis.typePayment,
        accept_type_id: regis.acceptTypId,
        prefix_th_id: regis.prefix,
        prefix_other_th: regis.prefixOther,
        agent_id: regis.agent,
        type_register_id: regis.typeRegister,
        phone: regis.telephone,
        status: regis.status,
        renew_tag: regis.renewTag,
        name_en: regis.firstNameEn,
        surname_en: regis.lastNameEn,
        prefix_en_id: regis.prefixEn,
        email: regis.email,
      },
    });
  }

  async updatePathRegisterLog({
    logId,
    pathPhotoBank,
    pathPhotoIdCard,
    pathPhotoMe,
  }: {
    logId: number;
    pathPhotoBank?: string | null;
    pathPhotoIdCard?: string | null;
    pathPhotoMe?: string | null;
  }) {
    await this.register_log.update({
      where: {
        id: logId,
      },
      data: {
        ...(pathPhotoBank && { photo_bank: pathPhotoBank }),
        ...(pathPhotoIdCard && { photo_id_card: pathPhotoIdCard }),
        ...(pathPhotoMe && { photo_me: pathPhotoMe }),
      },
    });
  }
  async findRegisterLogList(idCard: string) {
    let data = await this.register_log.findFirst({
      where: {
        id_card: idCard,
        show: 1,
        status: 0,
      },
      orderBy: {
        created_at: "desc", // Sort by createdAt field in descending order
      },
      select: {
        created_at: true,
        status: true,
        id_card: true,
        comment: true,
        birth_date: true,
      },
    });
    return data;
  }
  async findCheckRegisterLog(idCard: string) {
    let data = await this.register_log.findMany({
      where: {
        id_card: idCard,
        type_register_id: {
          in: [3, 4],
        },
        status: 0,
      },
    });
    return data;
  }

  async findRegisterLogByIdCard(idCard: string) {
    let data = await this.register_log.findFirst({
      where: {
        id_card: idCard,
        status: 0,
      },
    });
    return data;
  }
}
