import { HttpException } from "@/shared/http/httpException";
import { apiUrl } from "@/shared/utils/config";
import axios from "axios";

export class RegisterExternalService {
  async findMemberByIdCard(idCard: string): Promise<{ isCanRegister: boolean; message: string; status: number } | HttpException> {
    const { data } = await axios.get<{
      isCanRegister: boolean;
      message: string;
      status: number;
    }>(`${apiUrl.apiUrl3}/api/data/member-status/${idCard}?status=[0,7,15]`);

    if (data.status !== 200) {
      return new HttpException(data.status, data.message);
    }

    return {
      isCanRegister: data.isCanRegister,
      message: data.message,
      status: data.status,
    };
  }

  async findTMemberDpByIdCard(idCard: string): Promise<{ isCanRegister: boolean; message: string; status: number } | HttpException> {
    const { data } = await axios.get<{
      isCanRegister: boolean;
      data: any;
      message: string;
      status: number;
    }>(`${apiUrl.apiUrl3}/api/data/t-member-status?idCard=${idCard}`);

    if (data.status !== 200) {
      return new HttpException(data.status, data.message);
    }
    const state = 1;

    if (!data.data.mem_status) {
      return {
        isCanRegister: true,
        message: data.message,
        status: data.status,
      };
    } else {
      return {
        isCanRegister: false,
        message: data.message,
        status: data.status,
      };
    }
  }

  async findMemberStatusSkByIdCard(idCard: string): Promise<{ isCanRegister: boolean } | HttpException> {
    const { data } = await axios.get<{
      isCanRegister: any;
      data: any;
      message: string;
      status: number;
    }>(`${apiUrl.apiUrl2}/api/data/get-member-status?idCard=${idCard}`);
    if (data.status !== 200) {
      return new HttpException(data.status, data.message);
    }
    if (data.data) {
      // return {
      //   isCanRegister: true,
      // };
      if (data.data.number_ref && data.data.chk_st_p2 == "2") {
        return {
          isCanRegister: true,
          message: data.message,
          status: data.status,
        };
      } else {
        return {
          isCanRegister: false,
          message: data.message,
          status: data.status,
        };
      }
    } else {
      return {
        isCanRegister: true,
        message: data.message,
        status: data.status,
      };
    }
  }
}
