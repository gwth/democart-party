import { PrismaClient } from "@prisma/client";

export class CommonService {
  type_prefix_th = new PrismaClient().type_prefix_th;
  province = new PrismaClient().province;
  aumper = new PrismaClient().aumper;
  tumbon = new PrismaClient().tumbon;
  typeRegist = new PrismaClient().type_register;
  typeComment = new PrismaClient().type_comment;
  register = new PrismaClient().register;
  public async findTypePrefixTh() {
    return await this.type_prefix_th.findMany({
      select: {
        id: true,
        data: true,
        sort: true,
      },
    });
  }
  public async findTypePrefixThByName(prefix_name:string) {
   return await this.type_prefix_th.findFirst({
     where:{
       data:prefix_name
     },
     select:{
       id:true,
       data:true
     }
   })
  }
  public async findProvince() {
    return await this.province.findMany({
      select: {
        id: true,
        name: true,
      },
    });
  }

  public async findProvinceByName(province_name: string) {
    return await this.province.findFirst({
      where: {
        name:province_name
      },
      select:{
        id:true,
        name:true
      }
    })
  }
  public async findAumperByProvinceId(province_id: number) {
    return await this.aumper.findMany({
      where: {
        province_id,
      },
      select: {
        id: true,
        name: true,
      },
    });
  }

  public async findAumperByName(aumper_name: string) {
    return await this.aumper.findMany({
      where: {
        name:aumper_name
      },
      select:{
        id:true,
        name:true
      }
    })
  }
  public async findTumbonByAumperId(aumper_id: number) {
    return await this.tumbon.findMany({
      where: {
        aumper_id,
      },
      select: {
        id: true,
        name: true,
      },
    });
  }

  public async findTumbonByName(tumbon_name: string) {
    return await this.tumbon.findMany({
      where: {
        name:tumbon_name
      },
      select: {
        id: true,
        name: true,
      },
    })
  }
  public async findTypeRegister() {
    return await this.typeRegist.findMany({
      select: {
        id: true,
        data: true,
      },
    });
  }
  public async findTypeComment() {
    return await this.typeComment.findMany({
      select: {
        id: true,
        data: true,
        sort: true,
      },
    });
  }
  public async testCreateRegist(date: Date) {
    return await this.register.create({
      data: {
        id_card: "xx",
        birth_date: date,
      },
    });
  }

  public async testGetLastRegist() {
    return await this.register.findMany({
      select: {
        id: true,
        birth_date: true,
        created_at: true,
      },
    });
  }
}
