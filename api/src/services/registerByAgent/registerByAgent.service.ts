import { PrismaClient } from "@prisma/client";

export class RegisterByAgentrService {
  register = new PrismaClient().register;
  register_log = new PrismaClient().register_log;

  public async findUserInRegister(id: string) {
    // const findRegister = await this.register.findMany({
    //   where: {
    //     id_card: id,
    //     status: 1,
    //   },
    //   select: {
    //     id_card: true,
    //   },
    // });

    const findRegisterLog = await this.register_log.findMany({
      where: {
        id_card: id,
        status: {
          in: [0, 1],
        },
      },
      select: {
        id_card: true,
      },
    });
    return { /*register: findRegister,*/ registerlog: findRegisterLog };
  }

  public async RegisterLog(agent_id: number) {
    return await this.register_log.findMany({
      where: {
        agent_id: agent_id,
        status: 1,
      },
      select: {
        id_card: true,
        name_th: true,
        surname_th: true,
        created_at: true,
      },
      // include: {
      //   users_register_log_agent_idTousers: {
      //     select: {
      //       first_name: true,
      //       last_name: true,
      //     },
      //   },
      // },
    });
  }
}
