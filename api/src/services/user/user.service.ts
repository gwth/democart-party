import { PrismaClient, users } from "@prisma/client";
import { User } from "@/dtos/user/user";
import { isNumber } from "class-validator";

export class UserService {
  user = new PrismaClient().users;

  public async findUserByUsername(username: string) {
    return await this.user.findFirst({
      where: {
        username: username,
      },
    });
  }

  public async findUserById(idUser: number): Promise<users | null> {
    return await this.user.findUnique({
      where: {
        id: idUser,
      },
    });
  }
  public async findUserByText(    textSearch: string ='',) {
    const userList =  await this.user.findMany({
      where: {
        role: 2,
        OR:[
          {
            last_name:{
              contains: textSearch
            }
          },
          {
            first_name:{
              contains: textSearch
            }
          }
        ]
      }
    })
    return { dataUser: userList};
  }

  public async saveUser(user: User) {
    return await this.user.upsert({
      where: {
        nb_id: user.nb_id,
      },
      update: {
        username: user.username,
        password: user.password,
        birth_date: user.birthDate,
        first_name: user.firstName,
        last_name: user.lastName,
        role: user.role,
      },
      create: {
        username: user.username,
        password: user.password,
        birth_date: user.birthDate,
        first_name: user.firstName,
        last_name: user.lastName,
        role: user.role,
        nb_id: user.nb_id,
      },
    });
  }
}
