import { UserExternal } from "@/dtos/user/user";
import { HttpException } from "@/shared/http/httpException";
import { HttpResponse } from "@/shared/http/response";
import { apiUrl } from "@/shared/utils/config";
import axios from "axios";

export class UserExternalService {
  async findUserByUsername(username: string, password: string): Promise<{ user: UserExternal; message: string; status: number } | HttpException> {
    const { data } = await axios.post<{ user: UserExternal; message: string; status: number }>(`${apiUrl.apiUrl3}/api/data/user`, {
      username,
      password,
    });

    if (data.status !== 200) {
      return new HttpException(data.status, data.message);
    }

    return { user: data.user, message: data.message, status: data.status };
  }
}
