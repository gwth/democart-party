import { JWT } from "@/shared/utils/jwt";
import { Hasher } from "@/shared/utils/hasher";
import { IsNotEmpty, IsString, IsOptional } from "class-validator";

export class Auth {
  @IsString()
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsOptional()
  hashedPassword?: string | null;

  rememberMe: boolean = false;

  constructor(username: string, password: string, rememberMe: boolean) {
    this.username = username;
    this.password = password;
    this.rememberMe = rememberMe;
  }

  public comparePassword(hashPassword: string): boolean {
    return Hasher.comparePassword(this.password, hashPassword);
  }

  public hashPassword() {
    this.hashedPassword = Hasher.hashPassword(this.password);
  }

  public generateToken(userId: number): string {
    return JWT.generateToken({ username: this.username, userId }, this.rememberMe);
  }
}
