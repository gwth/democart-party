import { Hasher } from "@/shared/utils/hasher";
import { ClassProperties } from "@/shared/utils/utilities-type";

export class User {
  id?: number | null;
  username: string;
  password: string;
  birthDate: Date | null;
  role: number;
  firstName: string;
  lastName: string;
  nb_id: number;

  constructor(user: ClassProperties<User>) {
    this.username = user.username;
    this.password = user.password;
    this.birthDate = user.birthDate;
    this.role = user.role;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.nb_id = user.nb_id;
  }

  // public checkEmail(): boolean {
  //   const re = /\S+@\S+\.\S+/;
  //   return re.test(this.email);
  // }

  // public checkIdCard(): boolean {
  //   const re = /^[0-9]{13}$/;
  //   return re.test(this.idCard);
  // }

  publicUser(): TUserResponse {
    const userRes = new User({} as User);
    userRes.username = this.username;
    userRes.role = this.role;
    userRes.firstName = this.firstName;
    userRes.lastName = this.lastName;
    userRes.birthDate = this.birthDate;
    return userRes;
  }

  hasPassword() {
    this.password = Hasher.hashPassword(this.password);
  }
}

export type TUserResponse = Pick<User, "username" | "role" | "firstName" | "lastName" | "birthDate">;

export class UserExternal {
  nb_id: number;
  u_name: string;
  p_word: string;
  f_name: string;
  l_name: string;
  s_type: string;
  st_online: string;
  lg_date: string;
  area_point: string;
  position_name: string;
  title: string;
  book_Temporary_receipt: string;
  full_combo: string;
  tel: string;
  e_main: string;

  constructor(user: UserExternal) {
    this.nb_id = user.nb_id;
    this.area_point = user.area_point;
    this.u_name = user.u_name;
    this.p_word = user.p_word;
    this.book_Temporary_receipt = user.position_name;
    this.e_main = user.e_main;
    this.f_name = user.f_name;
    this.full_combo = user.full_combo;
    this.l_name = user.l_name;
    this.lg_date = user.lg_date;
    this.st_online = user.st_online;
    this.title = user.title;
    this.tel = user.tel;
    this.position_name = user.position_name;
  }
}
