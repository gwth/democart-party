import { THAIIDCHECK } from "@/shared/utils/thaiIdCard";
import { ClassProperties } from "@/shared/utils/utilities-type";
import { register_log } from "@prisma/client";
export class Register {
  dataRegister: IRegister;
  x: string;

  //   constructor(data: register_log) {
  //     if (data.id_card != null) {
  //       this.dataRegister = {
  //         id: 0,
  //         id_card: data.id_card,
  //         name_th: data.name_th,
  //         surname_th: data.surname_th,
  //         name_en: data.name_en,
  //         surname_en: data.surname_en,
  //         birth_date: data.birth_date,
  //         address_number: data.address_number,
  //         village_no: data.village_no,
  //         alley: data.alley,
  //         soi: data.soi,
  //         road: data.road,
  //         tumbon_id: data.tumbon_id,
  //         aumper_id: data.aumper_id,
  //         province_id: data.province_id,
  //         type_register_id: data.type_register_id,
  //         payment: data.payment,
  //         phone: data.phone,
  //         status: data.status,
  //         accept_type_id: data.accept_type_id,
  //         accept_date: data.accept_date,
  //         prefix_other_th: data.prefix_other_th,
  //         prefix_other_en: data.prefix_other_en,
  //         prefix_th_id: data.prefix_th_id,
  //         prefix_en_id: data.prefix_en_id,
  //       };
  //     }
  //   }
  //   public getDataRegist(): IRegister {
  //     return this.dataRegister;
  //   }
  public convertLogToIRegister(data: register_log): IRegister {
    if (data.id_card == null) {
      return this.dataRegister;
    }
    this.dataRegister = {
      id: 0,
      id_card: data.id_card,
      name_th: data.name_th,
      surname_th: data.surname_th,
      name_en: data.name_en,
      surname_en: data.surname_en,
      birth_date: data.birth_date,
      address_number: data.address_number,
      village_no: data.village_no,
      alley: data.alley,
      soi: data.soi,
      road: data.road,
      tumbon_id: data.tumbon_id,
      aumper_id: data.aumper_id,
      province_id: data.province_id,
      type_register_id: data.type_register_id,
      phone: data.phone,
      status: 1,
      accept_type_id: data.accept_type_id,
      accept_date: data.accept_date,
      prefix_other_th: data.prefix_other_th,
      prefix_other_en: data.prefix_other_en,
      prefix_th_id: data.prefix_th_id,
      prefix_en_id: data.prefix_en_id,
    };

    return this.dataRegister;
  }
}
export interface IRegister {
  id: number;
  name_th: string | null;
  surname_th: string | null;
  name_en: string | null;
  surname_en: string | null;
  birth_date: Date | null;
  address_number: string | null;
  village_no: string | null;
  alley: string | null;
  soi: string | null;
  road: string | null;
  tumbon_id: number | null;
  aumper_id: number | null;
  province_id: number | null;
  id_card: string;
  type_register_id: number | null;
  phone: string | null;
  status: number | null;
  accept_type_id: number | null;
  accept_date: Date | null;
  prefix_other_th: string | null;
  prefix_other_en: string | null;
  prefix_th_id: number | null;
  prefix_en_id: number | null;
}

export class RegisterLogCreateRequest {
  typeRegister: string;
  prefix: string;
  prefixOther: string;
  firstName: string;
  lastName: string;
  idCard: string;
  birthDate: Date;
  houseNumber: string;
  building: string | null;
  moo: string;
  soi: string | null;
  road: string | null;
  tombon: string;
  amphoe: string;
  province: string;
  telephone: string;
  typePayment: string;
  acceptTypId: string;
  fileImageApprove: File | null;
  fileImageCardIdentification: File | null;
  fileImagePayment: File | null;
  agent: string | null;
  status: string;
  renewTag: string;
  firstNameEn: string | null;
  lastNameEn: string | null;
  prefixEn: string | null;
  email?: string | null;
}

export class RegisterLogCreate {
  typeRegister: number;
  prefix: number;
  prefixOther: string | null;
  firstName: string;
  lastName: string;
  idCard: string;
  birthDate: Date;
  houseNumber: string;
  building: string | null;
  moo: string;
  soi: string | null;
  road: string | null;
  tombon: number;
  amphoe: number;
  province: number;
  telephone: string;
  typePayment: number;
  pathApprove: string;
  pathProfileCard: string;
  pathPayment: string;
  acceptTypId: number;
  agent: number | null;
  status: number;
  renewTag: string;
  firstNameEn: string | null;
  lastNameEn: string | null;
  prefixEn?: number | null;
  email?: string | null;

  constructor({
    typeRegister,
    prefix,
    prefixOther,
    firstName,
    lastName,
    idCard,
    birthDate,
    houseNumber,
    building,
    moo,
    soi,
    road,
    tombon,
    amphoe,
    province,
    telephone,
    typePayment,
    pathProfileCard,
    pathApprove,
    pathPayment,
    acceptTypId,
    agent,
    status,
    renewTag,
    firstNameEn,
    lastNameEn,
    prefixEn,
    email,
  }: ClassProperties<RegisterLogCreate>) {
    this.typeRegister = typeRegister;
    this.prefix = prefix;
    this.prefixOther = prefixOther;
    this.firstName = firstName;
    this.lastName = lastName;
    this.idCard = idCard;
    this.birthDate = birthDate;
    this.houseNumber = houseNumber;
    this.building = building;
    this.moo = moo;
    this.soi = soi;
    this.road = road;
    this.tombon = tombon;
    this.amphoe = amphoe;
    this.province = province;
    this.telephone = telephone;
    this.typePayment = typePayment;
    this.pathProfileCard = pathProfileCard;
    this.pathApprove = pathApprove;
    this.pathPayment = pathPayment;
    this.acceptTypId = acceptTypId;
    this.agent = agent;
    this.status = status;
    this.renewTag = renewTag;
    this.firstNameEn = firstNameEn;
    this.lastNameEn = lastNameEn;
    this.prefixEn = prefixEn;
    this.email = email;
  }

  checkIdCard() {
    return THAIIDCHECK(this.idCard);
  }
}
