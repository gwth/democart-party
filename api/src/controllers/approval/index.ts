import { ApprovalService } from "@/services/approval/approval.service";
import dayjs, { locale } from "dayjs";
import { NextFunction, Request, Response } from "express";
import { register_log } from "@prisma/client";
import { Register, IRegister } from "@/dtos/register/register";
import dayjsCustom from "@/shared/utils/dayjsCustom";
import { getUrlObject } from "@/shared/utils/minio";
import axios, { AxiosResponse, AxiosError } from "axios";
import { apiUrl } from "@/shared/utils/config";

var utc = require("dayjs/plugin/utc");
var timezone = require("dayjs/plugin/timezone");
dayjs.extend(utc);
dayjs.extend(timezone);
const apiUrlNewRegist = `${apiUrl.apiUrl2}/api/create/member-status`;
const apiUrlRenew = `${apiUrl.apiUrl2}/api/create/t-renew-saka`;
export class ApprovalController {
  public approvalService = new ApprovalService();
  setStatusApprove = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const { ids } = req.body;
    if (!req.user) {
      return res.status(400).json({ message: "not have userId" });
    }
    if (!ids) {
      return res.status(400).json({ message: "not have ids" });
    }
    const userId = req.user.userId;
    console.log("start");
    for (let index = 0; index < ids.length; index++) {
      const id = ids[index];
      console.log("start R ", index + 1);
      try {
        const { status, comment_id, comment } = req.body;
        console.log(req.user);

        let dataUpdate: register_log = await this.approvalService.updateStatus(
          id,
          status,
          comment_id ? comment_id : null,
          comment ? comment : null,
          userId,
          new Date(),
          0
        );

        if (dataUpdate && dataUpdate.status == 1) {
          let t = new Register().convertLogToIRegister(dataUpdate);
          t.status = 1;
          await this.approvalService.upsertRegisterByIdCard(t);
          const dataLogMore = await this.approvalService.getRegistLogById(
            dataUpdate.id
          );
          let dataToAPI2: any = {};
          let dataToAPI2Arr: any[] = [];
          if (
            (dataUpdate.type_register_id == 1 ||
              dataUpdate.type_register_id == 2) &&
            dataUpdate.status == 1
          ) {
            dataToAPI2.idCard = dataUpdate.id_card?.toString();
            dataToAPI2.prov_cont = dataLogMore?.province?.abbreviation
              ? dataLogMore?.province?.abbreviation.toLowerCase()
              : "miss";
            dataToAPI2.mem_status = "5";
            dataToAPI2.mem_type = dataLogMore?.type_accept?.cost?.toString();
            dataToAPI2.payment_channel =
              dataUpdate.payment == 1 ? "เงินสด" : "โอน";
            dataToAPI2.user_insert_name =
              dataUpdate.type_register_id == 1
                ? "dmp001"
                : dataLogMore?.users_register_log_agent_idTousers?.username;
            dataToAPI2.newmem_year = dayjsCustom.DateYearBB(
              dataUpdate.created_at
            );
            dataToAPI2.birthday = dayjsCustom.DateShortLongYearTH(
              dataUpdate.birth_date ? dataUpdate.birth_date : new Date()
            );
            dataToAPI2.title = dataLogMore?.type_prefix_th?.data?.includes(
              "อื่นๆ"
            )
              ? dataUpdate.prefix_other_th
              : dataLogMore?.type_prefix_th?.data;
            dataToAPI2.f_name = dataUpdate.name_th;
            dataToAPI2.l_name = dataUpdate.surname_th;
            dataToAPI2.nationality = "";
            dataToAPI2.add_no = dataUpdate.address_number;
            dataToAPI2.moo = dataUpdate.village_no;
            dataToAPI2.soi = dataUpdate.soi;
            dataToAPI2.alley = dataUpdate.alley;
            dataToAPI2.road = dataUpdate.road;
            dataToAPI2.sub_district = dataLogMore?.tumbon?.name;
            dataToAPI2.district = dataLogMore?.aumper?.name;
            dataToAPI2.province = dataLogMore?.province?.name;
            dataToAPI2.tel = dataUpdate.phone;
            dataToAPI2.move_out = "0";
            dataToAPI2.sex = "";
            // console.log(dataUpdate.email);
            dataToAPI2.email = dataUpdate.email;
            dataToAPI2.pic_data = dataUpdate.photo_me
              ? await getUrlObject(dataUpdate.photo_me)
              : "";
            dataToAPI2.sc_idcard = dataUpdate.photo_id_card
              ? await getUrlObject(dataUpdate.photo_id_card)
              : "";
            dataToAPI2.sc_reg = dataUpdate.photo_bank
              ? await getUrlObject(dataUpdate.photo_bank)
              : "";
            dataToAPI2.registrar_id = "2";
            dataToAPI2.chk_st_p2 = "0";
            dataToAPI2.g_kkt_id = "0";
            dataToAPI2.signdate = dayjsCustom.DateShortLongYearTH(
              dataUpdate.created_at ? dataUpdate.created_at : new Date()
            );
            let r = await this.callAPI2(apiUrlNewRegist, dataToAPI2);
            console.log(r);
            if (r.status != 200) {
              await this.setLogToWaitApprove(id, userId);
            }
            // return res.status(r.status).json({ message: r.message });
            // return res.status(200).json({ message: "success", data: dataToAPI2 });
          } else if (
            (dataUpdate.type_register_id == 4 ||
              dataUpdate.type_register_id == 3) &&
            dataUpdate.status == 1
          ) {
            console.log("ต่ออายุ");
            if (!dataUpdate.renew_tag) {
              return;
            }
            let renewTag = JSON.parse(dataUpdate.renew_tag);
            console.log(renewTag);
            console.log(dataUpdate.agent_id);

            for (let index = 0; index < renewTag.length; index++) {
              let temp: any = {};
              temp.idCard = dataUpdate.id_card?.toString();
              temp.user_update_name = dataUpdate.agent_id
                ? dataLogMore?.users_register_log_agent_idTousers?.username
                : "dmp001";
              temp.renew_date = dayjsCustom.DateShortLongYearTH(
                dataUpdate.created_at ? dataUpdate.created_at : new Date()
              );
              // temp.renew_year = dayjsCustom.DateYearBB(dataUpdate.created_at);
              temp.renew_year = renewTag[index].year.toString();
              temp.renew_ref = "0";
              temp.chk_renew_st = "0";
              temp.mem_type = renewTag[index].cost.toString();
              // temp.mem_type = dataLogMore?.type_accept?.cost?.toString();
              temp.g_kkt_id = "";
              temp.pic_data = dataUpdate.photo_me
                ? await getUrlObject(dataUpdate.photo_me)
                : "";
              temp.sc_idcard = dataUpdate.photo_id_card
                ? await getUrlObject(dataUpdate.photo_id_card)
                : "";
              temp.sc_reg = dataUpdate.photo_bank
                ? await getUrlObject(dataUpdate.photo_bank)
                : "";
              dataToAPI2Arr.push(temp);
            }
            let r = await this.callAPI2(apiUrlRenew, dataToAPI2Arr);
            console.log(r);
            if (r.status != 200) {
              await this.setLogToWaitApprove(id, userId);
            }
            // return res.status(r.status).json({ message: r.message });

            // return res
            //   .status(200)
            //   .json({ message: "success", data: dataToAPI2Arr });
          }
        } else {
          // return res.status(200).json({ message: "update status success" });
        }
      } catch (error) {
        await this.setLogToWaitApprove(id, userId);
        next(error);
      }
    }
    return res.status(200).json({ message: "update status success" });
  };

  async setLogToWaitApprove(id: number, userId: number) {
    let dataUpdate: register_log = await this.approvalService.updateStatus(
      id,
      0,
      null,
      null,
      userId,
      new Date(),
      1
    );
  }
  async callAPI2(apiURL: string, data: any | any[]) {
    let d = await axios.post<{ status: number; message: string }>(apiURL, data);
    return d.data;
  }
}
