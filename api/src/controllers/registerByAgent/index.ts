import { RegisterByAgentrService } from "@/services/registerByAgent/registerByAgent.service";
import { NextFunction, Request } from "express";

export class RegisteByAgentrController {
  public registerByAgentrService = new RegisterByAgentrService();

  getRegisterAll = async (req: any, res: any, next: NextFunction) => {
    const idCardList: string[] = req.body.idCard;
    try {
      const results = await Promise.all(
        idCardList.map(async (idCard: string) => {
          return await this.registerByAgentrService.findUserInRegister(idCard);
        })
      );
      // const idRegister = results.map(m => m.register[0]?.id_card|| null).filter(Boolean)
      const idRegisterLog = results
        .map((m) => m.registerlog[0]?.id_card || null)
        .filter(Boolean);
      const concatenatedIds = Array.from(
        new Set([/*...idRegister,*/ ...idRegisterLog])
      );
      return res
        .status(200)
        .json({ data: concatenatedIds, message: "success" });
    } catch (error) {
      next(error);
    }
  };

  getRegisterLog = async (req: Request, res: any) => {
    const agentId = req.user?.userId;
    const data = await this.registerByAgentrService.RegisterLog(agentId);
    return res.status(200).json({ data, message: "success" });
  };
}
