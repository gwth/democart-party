import { User } from "@/dtos/user/user";

import { UserExternalService } from "./../../services/user/userexternal.service";
import { UserService } from "@/services/user/user.service";
import { NextFunction, Request, Response } from "express";
import { isNumber } from "class-validator";
import { HttpException } from "@/shared/http/httpException";

export class UserController {
  public userService = new UserService();
  public userExternalService = new UserExternalService();

  findUserMe = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const foundUser = await this.userService.findUserByUsername(req.user?.username);

      if (!foundUser) {
        throw new HttpException(404, "User not found");
      }

      const userRes = new User({} as User);
      userRes.username = foundUser.username;
      userRes.role = foundUser.role;
      userRes.firstName = foundUser.first_name;
      userRes.lastName = foundUser.last_name;
      userRes.birthDate = foundUser.birth_date;

      return res.status(200).json({
        message: "success",
        data: userRes.publicUser(),
      });
    } catch (error) {
      next(error);
    }
  };

  findUserById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let idUser = Number(req.params.id);
      if (!req.params.id || !isNumber(idUser)) {
        throw new Error("require userId");
      }
      const foundUser = await this.userService.findUserById(idUser);

      if (!foundUser) {
        throw new Error("User not found");
      }

      const userRes = new User({} as User);
      userRes.username = foundUser.username;

      userRes.birthDate = foundUser.birth_date;
      return res.status(200).json({
        message: "success",
        data: userRes.publicUser(),
      });
    } catch (error) {
      next(error);
    }
  };

  //name key fileExcel
  /*   createUserExcel = async (req: Request, res: Response, next: NextFunction) => {
    try {
      if (!req.files) {
        return res.status(400).json({ error: "No file uploaded" });
      }
      let f = req.files.fileExcel as UploadedFile;
      console.log(f.name);
      // Convert buffer to workbook
      const workbook = xlsx.readFile(f.tempFilePath);
      console.log(workbook.SheetNames);

      // Assuming there is only one sheet in the Excel file
      const sheetName = workbook.SheetNames[0];
      const sheet = workbook.Sheets[sheetName];
      // Convert sheet to JSON
      const jsonData: any[] = xlsx.utils.sheet_to_json(sheet);

      let tempDataCreate = [];
      let tempDataUpdate = [];
      let tempDataError = [];
      for (let index = 0; index < jsonData.length; index++) {
        console.log(jsonData[index].id);
        if (!jsonData[index].id) {
          const foundUser = await this.userService.findUserByUsername(
            jsonData[index].idCard.toString()
          );

          if (foundUser) {
            tempDataError.push("idcard is in db " + foundUser.idCard);
            continue;
          }
        }

        // console.log(this.convertUser(jsonData[index]));
        const dataUser = await this.userService.CreateUser(
          this.convertUser(jsonData[index])
        );
        if (!jsonData[index].id) {
          tempDataCreate.push(dataUser);
        } else {
          tempDataUpdate.push(dataUser);
        }
      }
      // Send the JSON data as the response
      res.json({
        create: tempDataCreate,
        update: tempDataUpdate,
        error: tempDataError,
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Internal server error" });
    }
  }; */
}
