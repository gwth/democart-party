import { TypedRequestQuery } from "@/interfaces/express/request";
import { CommonService } from "@/services/common/common.service";
import time from "@/shared/utils/time";
import dayjs, { locale } from "dayjs";
import { NextFunction, Request, Response } from "express";
var utc = require("dayjs/plugin/utc");
var timezone = require("dayjs/plugin/timezone");
dayjs.extend(utc);
dayjs.extend(timezone);
export class CommonController {
  public commonService = new CommonService();
  getPrefix = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let prefixs = await this.commonService.findTypePrefixTh();
      res.status(201).json({ message: "success", data: prefixs });
    } catch (error) {
      next(error);
    }
  };

  getPrefixByName = async (
    req: TypedRequestQuery<{
      prefix_name: string;
    }>,
    res: Response,
    next: NextFunction
  ) =>{
   try {
    let prefixs = await this.commonService.findTypePrefixThByName(req.query.prefix_name);
    res.status(201).json({ message: "success", data: prefixs });
   } catch (error) {
     next(error);
   }
  }

  getProvince = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let data = await this.commonService.findProvince();
      res.status(201).json({ message: "success", data: data });
    } catch (error) {
      next(error);
    }
  };

  getProvinceByName = async (
    req: TypedRequestQuery<{
      province_name: string;
    }>,
    res: Response,
    next: NextFunction
  ) =>{
   try {
    let province = await this.commonService.findProvinceByName(req.query.province_name);
    res.status(201).json({ message: "success", data: province });
   } catch (error) {
     next(error);
   }
  }

  getAumperByProvinceId = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const id: number = Number(req.params.id);
      console.log(id);
      if (!id) {
        return res
          .status(400)
          .json({ message: "require provinceId", data: [] });
      }
      let data = await this.commonService.findAumperByProvinceId(id);
      return res.status(201).json({ message: "success", data: data });
    } catch (error) {
      next(error);
    }
  };

  getAumperByName = async (
    req: TypedRequestQuery<{
    aumper_name: string;
   }>,
   res: Response,
    next: NextFunction
  )=>{
    try {
      let aumper = await this.commonService.findAumperByName(req.query.aumper_name);
      res.status(201).json({ message: "success", data:aumper });
  } catch (error) {
      next(error);
  }
}

  getTumbonByAumperId = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      const id: number = Number(req.params.id);
      console.log(id);
      if (!id) {
        return res.status(400).json({ message: "require AumperId", data: [] });
      }
      let data = await this.commonService.findTumbonByAumperId(id);
      return res.status(201).json({ message: "success", data: data });
    } catch (error) {
      next(error);
    }
  };

  getTumbonByName = async (
    req: TypedRequestQuery<{
      tumbon_name: string;
    }>,
    res: Response,
    next: NextFunction
  ) =>{
  try {
    let tumbon = await this.commonService.findTumbonByName(req.query.tumbon_name);
   res.status(201).json({ message: "success", data:tumbon });
  } catch (error) {
   next(error);
  }
};

  getTypeRegister = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let data = await this.commonService.findTypeRegister();
      res.status(201).json({ message: "success", data: data });
    } catch (error) {
      next(error);
    }
  };
  getTypeComment = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let data = await this.commonService.findTypeComment();
      res.status(201).json({ message: "success", data: data });
    } catch (error) {
      next(error);
    }
  };

  test = async (req: Request, res: Response, next: NextFunction) => {
    try {
      // await this.commonService.testCreateRegist(
      //   time.DateToDateLocal(new Date())
      // );

      let data = await this.commonService.testGetLastRegist();
      let str = data[data.length - 1].birth_date?.toString();
      console.log(str);
      console.log(dayjs(str));
      if (str) console.log("xx ", new Date(str));

      /*  const gg = data.map((d) => {
        console.log(
          dayjs(d.birth_date).locale("Asia/Bangkok").format("DD-MM-YYYY HH:mm")
        );

        return d;
      }); */
      console.log(
        data[data.length - 1].created_at,
        "ggggg",
        dayjs("2024-01-16T10:02:02.000Z").format("DD-MM-YYYY HH:mm")
      );
      res.status(201).json({ message: "success", data: data });
    } catch (error) {
      next(error);
    }
  };
}
