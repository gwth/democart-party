import { RegisterService } from "@/services/resgister/register.service";
import { apiUrl as apiUrlConfig } from "@/shared/utils/config";
import dayjsCustom from "@/shared/utils/dayjsCustom";
import axios from "axios";
import dayjs from "dayjs";
import { NextFunction, Request, Response } from "express";
import * as _ from "lodash";
const apiUrl = `${apiUrlConfig.apiUrl3}/api/data/t-member-status`;
const apiUrlGetData = `${apiUrlConfig.apiUrl3}/api/data/member-status-renew`;
const apiUrlGetMemStatusSKData = `${apiUrlConfig.apiUrl2}/api/data/get-member-status`;

export class CheckStatusController {
  public registerService = new RegisterService();
  getStatusUserRegist = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const idCard = req.query.idCard?.toString();

    // if (birthDate) {
    //   console.log(new Date(birthDate.toString()));
    // }
    // let str = "18 ธ.ค. 2541";
    // console.log(dayjs(str, "DD MMM YYYY", "th").toDate());
    if (!idCard) {
      return res.status(404).json({ message: "need id_card" });
    }
    if (!req.query.birthDate) {
      return res.status(404).json({ message: "need birthDate" });
    }
    // console.log(req.query.birthDate);
    const birthDate = dayjsCustom.DateShortLongYearTH(
      new Date(req.query.birthDate.toString())
    );
    console.log(
      "xx",
      dayjsCustom.DateShortLongYearTH(new Date(req.query.birthDate.toString()))
    );
    let dataRegistLog = await this.registerService.findRegisterLogList(idCard);
    let dataTmemberStatus = await this.callAPI3(apiUrl, idCard);
    let dataMemberStatus = await this.callAPI3MemStatus(apiUrlGetData, idCard); //data DP
    let dataMemberStatusSK = await this.callAPI3MemStatus(
      apiUrlGetMemStatusSKData,
      idCard
    ); //data SK
    // console.log(dataMemberStatusSK?.data);
    // console.log(dataMemberStatus?.data);
    // 0 = wait ,1 = approve ,2 = reject
    let d: {
      text: string;
      status: number;
      dataUser: any;
      renewYear: string;
      tstatus: string;
      newmemYear: string;
    } = {
      text: "ไม่เป็นสมาชิก",
      status: 2,
      dataUser: null,
      renewYear: "",
      tstatus: "",
      newmemYear: "",
    };
    if (dataMemberStatus?.data) {
      // console.log(dataMemberStatus.data.birthday);

      // console.log(birthDate == dataMemberStatus.data.birthday);
      console.log(dataTmemberStatus);
      console.log("dataMemberStatus " + dataMemberStatus);
      if (dataTmemberStatus && birthDate == dataMemberStatus.data.birthday) {
        let yearExp =
          Number(dataTmemberStatus.data.renew_year) > 0
            ? Number(dataTmemberStatus.data.renew_year)
            : Number(dataTmemberStatus.data.newmem_year);

        d.renewYear = dataTmemberStatus.data.renew_year;
        d.newmemYear = dataTmemberStatus.data.newmem_year;
        d.tstatus = dataTmemberStatus.data.mem_status;
        switch (dataTmemberStatus.data.mem_status) {
          case "0":
            d.text = "ไม่เป็นสมาชิก";
            break;
          case "1":
            d.text = "เป็นสมาชิก";
            d.status = 1;
            d.dataUser = dataMemberStatus.data;
            d.dataUser.expire =
              dataMemberStatus.data.deprived_date == "ตลอดชีพ"
                ? "ตลอดชีพ"
                : // : "รายปี";
                  "31 ธ.ค. " + (yearExp + 1);
            break;
          case "5":
            d.text = "รอการอนุมัติ";
            d.status = 0;
            break;
          case "7":
            d.text = "ไม่ต่ออายุสมาชิก";
            break;
          case "15":
            d.text = "ซ้ำกับพรรคอื่นในฐาน กกต.";
            break;
          case "17":
            d.text = "ข้อมูลซ้ำซ้อนกับพรรคการเมืองอื่น";
            break;
          default:
            d.text = "กำลังตรวจสอบ";
            d.status = 0;
            break;
        }
      }
    } else if (
      dataMemberStatusSK?.data &&
      dataMemberStatusSK?.data.birthday &&
      dataMemberStatusSK?.data.birthday == birthDate
    ) {
      let t = dataMemberStatusSK?.data;
      d.text =
        t.number_ref != "" && t.chk_st_p2 == 2
          ? "ไม่ผ่านการอนุมัติ"
          : "รอการอนุมัติ";
      d.status = t.number_ref != "" && t.chk_st_p2 == 2 ? 2 : 0;
    } else if (
      dataRegistLog &&
      dayjs(dataRegistLog.birth_date).isSame(
        new Date(req.query.birthDate.toString()),
        "day"
      )
    ) {
      if (dataRegistLog.status == 0 /*|| dataRegistLog.status == 1*/) {
        d.text = "รอการอนุมัติ";
        d.status = 0;
        // d.status = true;
      } else if (dataRegistLog.status == 1) {
        d.text = dataRegistLog.comment
          ? dataRegistLog.comment
          : "ไม่ผ่านการอนุมัติ";
        d.status = 2;

        // d.status = true;
      }
    } else {
      d.text = "ไม่พบข้อมูลสมาชิก";
      d.status = 2;
    }
    // console.log("xxx ", d);
    res.status(200).json({ message: "success", data: d });
  };
  async callAPI3(apiURL: string, idCard: string) {
    try {
      let d = await axios.get<{
        data: {
          mem_status: string;
          renew_year: string;
          newmem_year: string;
          mem_type: string;
        };
      }>(apiURL + `?idCard=${idCard}`);
      return d.data;
    } catch (err) {
      return null;
    }
  }

  async callAPI3MemStatus(apiURL: string, idCard: string) {
    try {
      let d = await axios.get<{
        data: any;
      }>(apiURL + `?idCard=${idCard}`);
      return d.data;
    } catch (err) {
      return null;
    }
  }

  getCheckMemberDetail = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    const apiUrlGetData = `${apiUrlConfig.apiUrl3}/api/data/member-status-renew?idCard=${req.query.idCard}`;
    try {
      let d = await axios.get<{
        data: any;
      }>(apiUrlGetData);
      console.log("d.data.data", d.data.data);
      return res.status(200).json({ message: "success", data: d.data.data });
    } catch (err) {
      return null;
    }
  };

  // getCheckRenew = async (req: Request, res: Response, next: NextFunction) => {
  //   const apiUrlGetData = `${apiUrlConfig.apiUrl2}/api/create/t-renew-saka-id?idCard=${req.query.idCard}`;
  //   try {
  //     let d = await axios.get<{
  //       data: any;
  //     }>(apiUrlGetData);
  //     const data = d.data.data;
  //     console.log("data", data);
  //     let sorted_obj = _.sortBy(data, [
  //       function (o) {
  //         return o.renew_year;
  //       },
  //     ]);
  //     const check = sorted_obj[sorted_obj.length - 1];

  //     return res.status(200).json({ message: "success", data: check });
  //   } catch (err) {
  //     return null;
  //   }
  // };
  getCheckRenew = async (req: Request, res: Response, next: NextFunction) => {
    const apiUrlGetData = `${apiUrlConfig.apiUrl2}/api/create/t-renew-saka-id?idCard=${req.query.idCard}`;
    try {
      const response = await axios.get<{ data: any }>(apiUrlGetData);
      const data = response.data.data;

      if (data.length === 0) {
        return res.status(200).json({ message: "Data not found", data: data });
      }

      const sorted_obj = _.sortBy(data, [(o) => o.renew_year]);
      const check = sorted_obj[sorted_obj.length - 1];

      return res.status(200).json({ message: "success", data: check });
    } catch (err) {
      console.error("Error fetching data:", err);
      return res.status(500).json({ message: "Internal server error" });
    }
  };
}
