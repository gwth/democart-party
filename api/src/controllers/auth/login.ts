import { Auth } from "@/dtos/login/login";
import { User } from "@/dtos/user/user";
import { TypedRequestBody } from "@/interfaces/express/request";
import { UserService } from "@/services/user/user.service";
import { UserExternalService } from "@/services/user/userexternal.service";
import { HttpException } from "@/shared/http/httpException";
import logger from "@/shared/http/logger";
import { NextFunction, Response } from "express";

export class AuthController {
  userService = new UserService();
  userExternalService = new UserExternalService();

  public login = async (req: TypedRequestBody<Pick<Auth, "username" | "password" | "rememberMe">>, res: Response, next: NextFunction) => {
    try {
      const { username, password, rememberMe } = req.body;

      if (!username || !password) {
        return res.status(400).json({
          message: "username or password is required",
        });
      }

      const foundUserExternal = await this.userExternalService.findUserByUsername(username, password);

      if (foundUserExternal instanceof HttpException) {
        throw new HttpException(foundUserExternal.status, foundUserExternal.message);
      }

      if (!foundUserExternal.user) {
        return res.json(foundUserExternal.status).json({ message: foundUserExternal.message });
      }

      const auth = new Auth(foundUserExternal.user.u_name, foundUserExternal.user.p_word, rememberMe);

      const user = new User({
        username: foundUserExternal.user.u_name,
        lastName: foundUserExternal.user.l_name,
        nb_id: foundUserExternal.user.nb_id,
        firstName: foundUserExternal.user.f_name,
        role: Number(foundUserExternal.user.s_type),
        password: foundUserExternal.user.p_word,
        birthDate: null,
      });
      user.hasPassword();

      const userSaved = await this.userService.saveUser(user);

      const jwt = auth.generateToken(userSaved.id);

      return res.status(200).json({
        message: "login success.",
        token: jwt,
      });
    } catch (error) {
      logger.error("login failure: ", error);
      next(error);
    }
  };
}
