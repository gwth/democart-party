import { RegisterExternalService } from "./../../services/resgister/registerexaternal.service";
import { RegisterLogCreateRequest, RegisterLogCreate } from "@/dtos/register/register";
import { TypedRequestBody, TypedRequestParams } from "@/interfaces/express/request";
import { RegisterService } from "@/services/resgister/register.service";
import { UserService } from "@/services/user/user.service";
import { HttpException } from "@/shared/http/httpException";
import logger from "@/shared/http/logger";
import { ItemBucketMetadata, getUrlObject, uploadFile, uploadFileSteam } from "@/shared/utils/minio";
import { THAIIDCHECK } from "@/shared/utils/thaiIdCard";
import { isNumber } from "class-validator";
import { NextFunction, Request, Response } from "express";
import { text } from "pdfkit";

export class RegisterController {
  public userService = new UserService();
  public registerService = new RegisterService();
  public registerExternalService = new RegisterExternalService();

  getRegisterLogs = async (req: any, res: any) => {
    const data = await this.registerService.listRegisterLogs();
    return res.status(200).json({ data, message: "success" });
  };
  getRegisterById = async (req: Request<{ id: string }>, res: Response) => {
    let data = await this.registerService.findRegisterLogById(Number(req.params.id));
    if (!data) return res.status(404).json({ message: "data not found" });

    let dataRes = JSON.parse(JSON.stringify(data));
    if (dataRes?.photo_bank) {
      dataRes!.photo_bank = await getUrlObject(dataRes?.photo_bank);
    }
    if (dataRes!.photo_id_card) {
      dataRes!.photo_id_card = await getUrlObject(dataRes?.photo_id_card);
    }
    if (dataRes!.photo_me) {
      dataRes!.photo_me = await getUrlObject(dataRes?.photo_me);
    }

    return res.status(200).json({ data: dataRes, message: "success" });
  };
  getRegisterByStatus = async (req: Request<{ status: string }>, res: Response) => {
    const data = await this.registerService.findRegisterLogBystatus(Number(req.params.status));
    return res.status(200).json({ data, message: "success" });
  };

  getRegisterLogList = async (req: Request, res: Response, next: NextFunction) => {
    try {
      let data: any;
      let dataRegister = [];
      let skip = Number(req.query.skip);
      let pageSize = Number(req.query.pagesize);
      let textSearch = req.query.textSearch ? req.query.textSearch.toString() : "";
      let status = req.query.status ? (Number(req.query.status) == -1 ? null : Number(req.query.status)) : null;
      let ids: number[] = [];
      let totalCount = 0;
      let data1: any;
      data1 = await this.userService.findUserByText(textSearch);
      ids = data1.dataUser.map((data: any) => data.id);

      if (isNumber(skip) && isNumber(pageSize)) {
        data = await this.registerService.findRegisterList(skip, pageSize, textSearch, status, ids);
        dataRegister = await data.dataUser;

        totalCount = await data.totalCount;
      } else {
        data = await this.registerService.findRegisterList(0, 1000);
        dataRegister = await data.dataUser;

        totalCount = await data.totalCount;
      }
      return res.status(200).json({
        message: "success",
        data: dataRegister,
        totalCount,
      });
    } catch (error) {
      next(error);
    }
  };

  createRegisterLog = async (
    req: TypedRequestBody<{
      registerData: string;
      fileImageApprove: string;
      fileImageCardIdentification: string;
      fileImagePayment: string;
    }>,
    res: Response,
    next: NextFunction
  ) => {
    try {
      interface FileCustom {
        name: string;
        type: ItemBucketMetadata;
        size: number;
        data: string;
      }
      const { registerData: registerDataString, fileImageApprove, fileImageCardIdentification, fileImagePayment } = req.body;

      const getAgentID = (agentId: number | null, typeRegister: number) => {
        if (!agentId) return [2, 4].includes(typeRegister) ? req.user?.userId : null;

        return agentId;
      };

      const registerRequest: RegisterLogCreateRequest = JSON.parse(registerDataString);

      const registerData = new RegisterLogCreate({
        typeRegister: Number(registerRequest.typeRegister),
        prefix: Number(registerRequest.prefix),
        prefixOther: registerRequest.prefixOther,
        firstName: registerRequest.firstName,
        lastName: registerRequest.lastName,
        idCard: registerRequest.idCard,
        birthDate: registerRequest.birthDate,
        houseNumber: registerRequest.houseNumber,
        building: registerRequest.building,
        moo: registerRequest.moo,
        soi: registerRequest.soi,
        road: registerRequest.road,
        tombon: Number(registerRequest.tombon),
        amphoe: Number(registerRequest.amphoe),
        province: Number(registerRequest.province),
        telephone: registerRequest.telephone,
        typePayment: Number(registerRequest.typePayment),
        acceptTypId: Number(registerRequest.acceptTypId),
        agent: getAgentID(Number(registerRequest.agent), Number(registerRequest.typeRegister)),
        status: Number(registerRequest.status),
        renewTag: registerRequest.renewTag,
        pathPayment: "",
        pathProfileCard: "",
        pathApprove: "",
        firstNameEn: registerRequest.firstNameEn,
        lastNameEn: registerRequest.lastNameEn,
        prefixEn: Number(registerRequest.prefixEn),
        email: registerRequest.email,
      });

      const err = registerData.checkIdCard();

      if (err.hasErr) {
        return res.status(400).json({ message: err.message });
      }

      /*  const fileImageApproveFile: FileCustom = JSON.parse(fileImageApprove);
      const fileImageCardIdentificationFile: FileCustom = JSON.parse(fileImageCardIdentification);
      const fileImagePaymentFile: FileCustom = JSON.parse(fileImagePayment) || undefined; */

      const fileImageApproveFile = req.files?.fileImageApprove;
      const fileImageCardIdentificationFile = req.files?.fileImageCardIdentification;
      const fileImagePaymentFile = req.files?.fileImagePayment;

      /**
       *
       * Check can register
       *
       */

      /*  const result = await this.registerExternalService.findMemberByIdCard(registerData.idCard);

      if (result instanceof HttpException) {
        throw new HttpException(result.status, result.message);
      }

      if (!result.isCanRegister) {
        return res.status(200).json({ message: "มีรายชื่อเป็นสมาชิกอยู่แล้ว" });
      } */

      /**
       * Path
       *
       * "/user-id/type-register-date/payment.jpg"
       *
       */

      const registerLogCreated = await this.registerService.saveRegisterLog(registerData);
      const regisLogId = registerLogCreated.id;

      if (fileImageApproveFile !== undefined && !Array.isArray(fileImageApproveFile)) {
        const idCard: string = registerData.idCard;
        const typeRegister: number = registerData.typeRegister;
        const typeFile: string = fileImageApproveFile.name.split(".")[fileImageApproveFile.name.split(".").length - 1];

        const path: string = `/register/${idCard}/${typeRegister}_${regisLogId}/Approve.${typeFile}`;

        await uploadFile(fileImageApproveFile.tempFilePath, path);
        // await uploadFileSteam(path, Buffer.from(fileImageApproveFile.data, "base64")!, fileImageApproveFile.size, fileImageApproveFile.type);
        await this.registerService.updatePathRegisterLog({
          logId: regisLogId,
          pathPhotoMe: path,
        });
      }

      if (fileImagePaymentFile !== undefined && !Array.isArray(fileImagePaymentFile)) {
        const idCard: string = registerData.idCard;
        const typeRegister: number = registerData.typeRegister;
        const typeFile: string = fileImagePaymentFile.name.split(".")[fileImagePaymentFile.name.split(".").length - 1];

        const path: string = `/register/${idCard}/${typeRegister}_${regisLogId}/Payment.${typeFile}`;

        await uploadFile(fileImagePaymentFile.tempFilePath, path);
        // await uploadFileSteam(path, Buffer.from(fileImagePaymentFile.data, "base64")!, fileImagePaymentFile.size, fileImagePaymentFile.type);
        await this.registerService.updatePathRegisterLog({
          logId: regisLogId,
          pathPhotoBank: path,
        });
      }

      if (fileImageCardIdentificationFile !== undefined && !Array.isArray(fileImageCardIdentificationFile)) {
        const idCard: string = registerData.idCard;
        const typeRegister: number = registerData.typeRegister;
        const typeFile: string = fileImageCardIdentificationFile.name.split(".")[fileImageCardIdentificationFile.name.split(".").length - 1];

        const path: string = `/register/${idCard}/${typeRegister}_${regisLogId}/ProfileCard.${typeFile}`;

        await uploadFile(fileImageCardIdentificationFile.tempFilePath, path);
        /* await uploadFileSteam(
          path,
          Buffer.from(fileImageCardIdentificationFile.data, "base64")!,
          fileImageCardIdentificationFile.size,
          fileImageCardIdentificationFile.type
        ); */
        await this.registerService.updatePathRegisterLog({
          logId: regisLogId,
          pathPhotoIdCard: path,
        });
      }

      return res.status(201).json({ message: "success", statusCode: 201 });
    } catch (error) {
      logger.error(error);
      next(error);
    }
  };

  checkMemberByIdCard = async (req: TypedRequestParams<{ id: string }>, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;

      if (!id || id.length == 0) {
        throw new HttpException(400, "IdCard is required.");
      }

      const err = THAIIDCHECK(id);
      if (err.hasErr) {
        throw new HttpException(400, err.message);
      }

      const result = await this.registerExternalService.findMemberByIdCard(id);

      if (result instanceof HttpException) {
        throw new HttpException(result.status, result.message);
      }

      /*  // DP
      const resultDP = await this.registerExternalService.findTMemberDpByIdCard(id);
      if (resultDP instanceof HttpException) {
        throw new HttpException(result.status, result.message);
      } */

      // SK
      const resultSK = await this.registerExternalService.findMemberStatusSkByIdCard(id);
      if (resultSK instanceof HttpException) {
        throw new HttpException(resultSK.status, result.message);
      }

      //  register log
      const resultRegisterLog = await this.registerService.findRegisterLogByIdCard(id);
      if (resultRegisterLog instanceof HttpException) {
        throw new HttpException(result.status, result.message);
      }

      return res.status(200).json({
        message:
          !result.isCanRegister || !resultSK.isCanRegister || resultRegisterLog?.status == 0
            ? "หมายเลขบัตรประชาชนนี้ได้ลงทะเบียนสมัครสมาชิกแล้ว"
            : "ไม่เป็นสมาชิก",
        isMember: !result.isCanRegister || !resultSK.isCanRegister || resultRegisterLog?.status == 0,
      });
    } catch (error) {
      logger.error("Check member already failure: ", error);
      next(error);
    }
  };

  checkRegisterLogByIdCard = async (req: TypedRequestParams<{ id: string }>, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params;

      const result = await this.registerService.findCheckRegisterLog(id);

      if (result.length > 0) {
        return res.status(200).json({
          message: "ท่านได้ทำการสมัครแล้ว โปรดรอการตรวจสอบ",
          isRegister: true,
        });
      } else {
        return res.status(200).json({
          message: "ท่านยังไม่ได้ทำการสมัคร",
          isRegister: false,
        });
      }
    } catch (error) {
      logger.error("Check member already failure: ", error);
      next(error);
    }
  };
}
